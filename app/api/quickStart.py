
from flask_restful import Resource, request
from app.base.models import SQLSchedulerSetting, SQLErrorSetting
from app.api.utils import import_json, latest_file, get_current_time_string, import_excel
from app import db as sql_db
import os

class QuickStart(Resource):
    def get(self,mode):
        from app.datagent import run

        if mode == "custom":
           run.switcher(mode='custom')
        else: 
            run.switcher(mode='quick_start')
        from app.agents.scheduleAgent import delete_db, copy_to_db, delete_p_db
        delete_db()
        copy_to_db()
        delete_p_db() 

        return {
            'message': 'Init Success!'
        }, 200
