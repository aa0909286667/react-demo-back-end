
from flask_restful import Resource, request
from flask import jsonify, current_app, abort
from sqlalchemy import desc
from sqlalchemy import or_, and_
from sqlalchemy.orm.attributes import flag_modified
from app.base.models import SQLProjectModel, SQLProductModel, SQLJobModel, SQLProcessModel, SQLSchedulerSetting, SQLMaterialModel, SQLJobLib

from app.base.models import SQLWorkingDays, SQLWorkingHours, SQLResourceModel, SQLScheduleDB, SQLNotificationModel, SQLBottleneckSetting

from app.api.gantt_assignment import FormattingGantt_db, FormattingGantt
from app.api.utils import import_json, save_json_to_file, serialize_db, get_current_time_string, AlchemyEncoder

from app.api.features import update_db
from app import db as sql_db

class Schedule(Resource):
    @staticmethod
    def _clear_selected_schedule():
        SQLScheduleDB.query.update({SQLScheduleDB.selected: False}) 
        sql_db.session.commit()

    @staticmethod
    def get(sid):
        sql_s = SQLScheduleDB.query.filter(SQLScheduleDB.index == sid).first()
        return sql_s

    @staticmethod
    def post(info=None):
        if not info: info = request.json
        Schedule._clear_selected_schedule()        
        sql_s = SQLScheduleDB(**info)
        sql_db.session.add(sql_s)
        sql_db.session.commit()
        return sql_s
    
    @staticmethod
    def put(sid,info=None):
        if not info: info = request.json
        if "selected" in info:
            Schedule._clear_selected_schedule()
        sql_s = SQLScheduleDB.query.filter(SQLScheduleDB.id == sid).first()
        for key, value in info.items():
            setattr(sql_s, key, value)
        sql_db.session.commit()
        return sql_s

def serialize_config_db(db,db_name):
    config = import_json(filename="scheduler_ver_config.json")
    json_db = {}
    for item in db:
        vals =  list(vars(item))[1:]
        if "index" in vals:
            index = item.index
        else:
            index = item.id 
        json_db[index]={}
        for val in vals:
            if val not in config["V3"][db_name]:
                continue
            if isinstance(getattr(item, val),set):
                json_db[index][val]=list(getattr(item, val))
            else:
                json_db[index][val] = getattr(item, val)
    return json_db

def filter_parameter(db, db_name):
    config = import_json(filename="scheduler_ver_config.json")
    parameter = set(config["V3"][db_name])
    for jid in db:
        vals = set(db[jid].keys())
        unwanted_key = vals - parameter
        for key in unwanted_key:
            db[jid].pop(key,None)
    return db
    
class Planner(Resource):
    @staticmethod
    def get_latest_setting():
        res = SQLSchedulerSetting.query.order_by(desc(SQLSchedulerSetting.created_time)).first()
        return res
    
    #排除已完成工作和暫停工作
    @staticmethod
    def get_pending_job():

        res = sql_db.session.query(SQLJobModel, SQLJobLib).join(SQLJobLib).filter(
            and_(
                SQLJobModel.real_end_time == None,
                SQLJobModel.status != "pause",
                SQLJobModel.lock != True,
                SQLJobModel.status != "remove"
            )
            ).all()

        job_db = AlchemyEncoder().default(res)

        return job_db

    #處理已完成或進行中工作之料號工作明細
    @staticmethod
    def update_product():
        product_db = serialize_config_db(SQLProductModel.query.all(),"product_db")

        SQLJobEntities = SQLJobModel.query.with_entities(
            SQLJobModel.index, SQLJobModel.product_id, \
            SQLJobModel.status, SQLJobModel.job_sequence
        ).filter(
            or_(
                SQLJobModel.status == "finished",
                SQLJobModel.status == "pause"
            )
        ).all()

        for sql_j in SQLJobEntities:
            job_sequence = sql_j.job_sequence
            prdid = sql_j.product_id
            jid = sql_j.index

            product_db[prdid]["_jobID_list"][str(job_sequence)].remove(jid)
            if len(product_db[prdid]["_jobID_list"][str(job_sequence)]) == 0:
                del product_db[prdid]["_jobID_list"][str(job_sequence)]
        
        return product_db


    #處理setting改變之resource
    @staticmethod
    def update_resource(dispatching_method):
        
        resource_db = serialize_config_db(SQLResourceModel.query.all(),"resource_db")
        if dispatching_method: 
            return resource_db
        else:
            for rid in resource_db:
                resource_db[rid]["in_between"] = False

        return resource_db
    
    #處理只忽略一次瓶頸
    @staticmethod
    def update_bottleneck():
        SQLBottleneckEntities = SQLBottleneckSetting.query.filter(SQLBottleneckSetting.ignore=="ignoreOnce").all()
        for sql_b in SQLBottleneckEntities:
            sql_b.ignore = "never"
            sql_db.session.commit()

    def check_pending_schedule(self):
        
        sql_s = SQLScheduleDB.query.filter(SQLScheduleDB.status=="pending").\
            order_by(desc(SQLScheduleDB.time)).first()
        return sql_s
    def get_data(self,dispatching_method):
        data = {
            "project_db":serialize_config_db(SQLProjectModel.query.all(),"project_db"),
            "process_db":serialize_config_db(SQLProcessModel.query.all(),"process_db"),
            "job_db": self.get_pending_job(),
            "resource_db": self.update_resource(dispatching_method),
            "product_db": self.update_product(),
            "workingdays_db":serialize_config_db(SQLWorkingDays.query.all(),"workingdays_db"),
            "workinghours_db":serialize_config_db(SQLWorkingHours.query.all(),"workinghours_db"),
            "job_lib": serialize_db(SQLJobLib.query.all())
        }
        return data

    def check_success(self,input_data,schedule):
        full_jid = list(input_data["job_db"].keys())
        
        result_job = []
        
        for rid in schedule:
            for job_tuple in schedule[rid]:
                JID = job_tuple[0]
                result_job.append(JID)
        
        diff = list(set(full_jid) - set(result_job))
        if len(diff) > 0:
            print("JID that failed to be assigned")
            diff.sort()
            print(diff)
    
    @staticmethod
    def check_lock_time_resource():
        event_db = {}
        
        
    @staticmethod
    def check_day_off():
        event_db = {}
        SQLResourceEntities = SQLResourceModel.query.with_entities(
            SQLResourceModel.index, SQLResourceModel.day_off
        ).all()
        
        for sql_r in SQLResourceEntities:
            if len(sql_r.day_off) == 0:
                continue
            rid = sql_r.index
            for index in sql_r.day_off:
                event_db[rid] = {
                    "is_job": 0,
                    "is_resource":1,
                    "job_id": None,
                    "resource_id": rid,
                    "time_switch": 0,
                    "start_time": sql_r.day_off[index]["event_time"][0],
                    "end_time": sql_r.day_off[index]["event_time"][1] 
                }
        return event_db
    
    @staticmethod
    def check_day_on():
        event_db = {}
        SQLResourceEntities = SQLResourceModel.query.with_entities(
                SQLResourceModel.index, SQLResourceModel.day_on
            ).all()
            
        for sql_r in SQLResourceEntities:
            if len(sql_r.day_off) == 0:
                continue
            rid = sql_r.index
            for date in sql_r.day_off:
                event_db[rid] = {
                    "is_job": 0, 
                    "is_resource":1,
                    "job_id": None,
                    "resource_id": rid,
                    "time_switch": 1,
                    "start_time": sql_r.day_on[index][0],
                    "end_time": sql_r.day_on[index][1] 
                }
        return event_db
        

    @staticmethod
    def check_resource_report():
        # resource event
        
        SQLNotificationEntities = SQLNotificationModel.query.filter(and_(\
            SQLNotificationModel.status != "resume_sent",
            SQLNotificationModel.status != "resume_received",
            SQLNotificationModel.jid == None)).all()
        
        event_db = {}
        for res in SQLNotificationEntities:
            
            event_db[res.index]={
                "is_job"      : 0 if res.jid == None else 1,
                "is_resource" : 1,
                "job_id"      : res.jid,
                "resource_id" : res.rid,
                "time_switch" : 0,
                "start_time"  : res.start_time,
                "end_time"    : res.end_time
            }
            res.status = "report_received"
            res.report_time = get_current_time_string()
        sql_db.session.commit()
        
        return event_db

    @staticmethod
    def check_job_report():
        SQLNotificationEntities = SQLNotificationModel.query.filter(and_(\
            SQLNotificationModel.status != "resume_sent",
            SQLNotificationModel.status != "resume_received",
            SQLNotificationModel.jid != None)).all()
        
        for res in SQLNotificationEntities:
            res.status = "report_received"
            res.report_time = get_current_time_string()
        sql_db.session.commit()

    @staticmethod
    def check_ongoing_job():
        event_db = {}
        
        SQLJobEntities = SQLJobModel.query.filter(\
            and_(SQLJobModel.real_start_time != None,
                 SQLJobModel.real_end_time == None
            )).all()

        for sql_j in SQLJobEntities:
            rid_list = sql_j.resource

            for num, rid in enumerate(rid_list):
                reindex = sql_j.index + "_" + str(num)
                event_db[reindex]={
                    "is_job"      : 1,
                    "is_resource" : 1,
                    "job_id"      : sql_j.index,
                    "resource_id" : rid,
                    "time_switch" : 0,
                    "start_time"  : None,
                    "end_time"    : sql_j.plan_end_time
                }
        return event_db
    
    @staticmethod
    def check_resource_resume():
        # check 異常排除之資源
        
        SQLNotificationEntities = SQLNotificationModel.query.filter(and_(\
            SQLNotificationModel.status == "resume_sent",
            SQLNotificationModel.jid == None)).all()
        
        for res in SQLNotificationEntities:
            res.status = "resume_received"
            res.end_received_time = get_current_time_string()

        sql_db.session.commit()

    @staticmethod
    def check_job_resume():
        SQLNotificationEntities = SQLNotificationModel.query.filter(and_(\
            SQLNotificationModel.status =="resume_sent",
            SQLNotificationModel.jid != None)).all()
        
        for res in SQLNotificationEntities:
            res.status = "resume_received"
            res.end_received_time = get_current_time_string()

        sql_db.session.commit()


    def createEvent(self):
        event_db = {}
        event_db.update(self.check_resource_report())
        event_db.update(self.check_ongoing_job())
        self.check_job_report()
        self.check_resource_resume()
        self.check_job_resume()

        return event_db
    
    def post(self):
        import subprocess
        import os

        setting = self.get_latest_setting()
        db_schedule = self.check_pending_schedule()
        if db_schedule != None and db_schedule.schedule_status=="sync":
            # 拆批之後
            data = self.check_pending_schedule().schedule_result
            data["job_db"] = filter_parameter(data["job_db"], "job_db")
            if "JID147_002" in data["job_db"]:
                print(data["job_db"]['JID147_002'])
        else:
            data = self.get_data(setting.dispatching_method)
        
        event_db = self.createEvent()
            
        input_path = os.path.join("app","compute","input")
        data_path = os.path.join(input_path,"UNASSIGNED_DB.json")
        event_path = os.path.join(input_path,"EVENT_DB.json")
        save_json_to_file(data=data, path=data_path, filename="")
        save_json_to_file(data=event_db, path=event_path,filename="")
        gs = get_current_time_string()
        command = 'python /app/compute/sara-planner/main.py {} {}'.format(setting.sequencing_method, gs)
        
        print(command)
        subprocess.call(command, shell = True)
        
        output_path = os.path.join("app","compute","output","planner_result.json")
        schedule_result = import_json(path=output_path,filename="")

        self.check_success(data,schedule_result["schedule"])

        update_schedule = update_db(db=data,schedule=schedule_result["schedule"],name=gs)

        fg = FormattingGantt(gs)
        update_gantt = fg.assign(update_schedule)
        if db_schedule is not None:
            db_schedule.status = "scheduled"
            sql_db.session.commit()
        
        if SQLScheduleDB.query.filter(SQLScheduleDB.status=="scheduled").first():
            count = SQLScheduleDB.query.filter(
                or_(SQLScheduleDB.status=="scheduled",
                    SQLScheduleDB.status=="pending"
                    )).count()
            index = "tmp" + str(count + 1)
        else:
            index = "tmp0"
        
        db_schedule = SQLScheduleDB(index=index, time=gs, schedule_result= update_schedule, schedule_status="autogenerated", 
                    gantt_result=update_gantt, gantt_status="autogenerated", status="scheduled")
        
        SQLProjectEntities = SQLProjectModel.query.filter(SQLProjectModel.status=="New").all()
        
        # for sql_p in SQLProjectEntities:
        #     sql_p.status = "scheduled"
        
        setting.scheduler_version.append(gs)
        
        sql_db.session.add(db_schedule)
        sql_db.session.commit()
        
        self.update_bottleneck()
        
        return {
            'message': 'Schedule Success!'
        }, 200        



