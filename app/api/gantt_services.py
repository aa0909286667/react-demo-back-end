from flask_restful import Resource, request
from app.api.utils import import_json, save_json_to_file, latest_file, latest_gantt_file, import_csv, get_current_time_string
from app.api.gantt_assignment import FormattingGantt_db, FormattingGantt
from app.base.models import SQLProjectModel, SQLProductModel, SQLJobModel, SQLWorkingDays, SQLWorkingHours, SQLResourceModel, SQLScheduleDB
from app import db as sql_db
from sqlalchemy import desc
import itertools
REVISION_ITER = itertools.count()

class LoadGantt(Resource):  #TODO:json
    
    def get(self):
        print("=======DEBUG: LOAD API TRIGGER =======")
        print(request.args)
        
        import ast
        
        request_data = ast.literal_eval(request.args.get("data"))
        print(request_data)

        schedule = SQLScheduleDB.query.filter(
            SQLScheduleDB.status != "pending"
        ).order_by(desc(SQLScheduleDB.time)).first()
        if not schedule:
            return
        
        db = schedule.gantt_result
        db["requestId"] = request_data["requestId"]
        db["type"] = "load"
        db["revision"] = next(REVISION_ITER)
        
        return db

class SyncGantt(Resource): 
    
    def get(self):
        return {
                'message': 'hello from save gantt db'
            }, 200
        
    def post(self):
        print("=======DEBUG: SYNC API TRIGGER =======")
        if not request.json:
            return {
                'message': 'data missing'
            }, 400
        
        data = request.json
        print(data)
        return self.sync_gantt(data)

    def sync_gantt(self,request_data): #TODO:json
        import pandas as pd
        gantt_data = SQLScheduleDB.query.filter(
            SQLScheduleDB.status != "pending"
        ).order_by(desc(SQLScheduleDB.time)).first().gantt_result
        
        response = {
            "requestId": request_data["requestId"],
            "success":True,
            "revision": next(REVISION_ITER), 
            "type": "sync"  
        }
        # time change
        update_job  = None
        if "tasks" in request_data: 
            new_tasks = request_data["tasks"]["updated"]
            new_df_task = pd.DataFrame(new_tasks)
            new_jobs = new_df_task[new_df_task.id >= 10000000]
            update_job = list(new_df_task[new_df_task.id >= 10000000].id)
        
            projects = gantt_data["tasks"]["rows"][0]["children"]
            for project in projects:
                for product in project["children"]:
                    for job in product["children"]:
                        if job["id"] in update_job:
                            #print(job)
                            endDate = list(new_jobs[new_jobs["id"]==job["id"]].endDate)[0]
                            startDate = list(new_jobs[new_jobs["id"]==job["id"]].startDate)[0]
                            if not pd.isna(endDate):
                                job["endDate"] = endDate
                            if not pd.isna(startDate):
                                job["startDate"] = startDate
                            #print(job)

        # resource change
        assign_true = True if "assignments" in request_data else False

        if assign_true:
            assignment = gantt_data["assignments"]["rows"]
            df_assignment = pd.DataFrame(assignment)
            resource_assignment = request_data["assignments"]
            for case in resource_assignment:
                if case == "added":
                    for i in resource_assignment["added"]:
                        _id = df_assignment["id"].iloc[-1] + 1
                        assignment.append(
                            {
                                "id": _id, 
                                "event": i["event"],
                                "resource": i["resource"]
                            }
                        )
                elif case == "updated":
                    for i in resource_assignment["updated"]:
                        #event = list(df_assignment[df_assignment["id"]==i["id"]].event)[0]
                        for assign in assignment:
                            if assign["id"] == i["id"]:
                                #print(assign)
                                assign["resource"] = i["resource"]
                                #print(assign)

        response["tasks"] = gantt_data["tasks"]
        response["dependencies"] = gantt_data["dependencies"]
        response["resources"] = gantt_data["resources"]
        response["assignments"] = gantt_data["assignments"]

        base = get_current_time_string()
        gantt_filename = "gantt_" + base + ".json"
        save_json_to_file(gantt_data, filename=gantt_filename)

        #response["response"]=sol
        print(self.gantt_to_schedule(gantt_data=gantt_data,base=base, update_job =update_job , assignment=assign_true))
        print(response)
        return response
    
    @staticmethod
    def gantt_to_schedule(gantt_data,base, assignment=False, update_job =None):  #TODO:json

        #db = import_json(path=latest_file(),filename="")
        db = SQLScheduleDB.query.order_by(desc(SQLScheduleDB.time)).first().schedule_result

        job_db = db["job_db"]
        
        if update_job : 
            projects = gantt_data["tasks"]["rows"][0]["children"]
            # task time change
            for project in projects:
                for product in project["children"]:
                    for job in product["children"]:
                        if job["id"] in update_job :
                            jid = "JID"+str(job["id"] - 10000000)
                            print("++++++{}++++++".format(job_db[jid]))
                            job_db[jid]["plan_start_time"] = job["startDate"].replace("T"," ")[0:16] #.split('+')[0]
                            job_db[jid]["plan_end_time"] = job["endDate"].replace("T"," ")[0:16] #.split('+')[0]
                            print("++++++{}++++++".format(job_db[jid]))
        # resource change
        if assignment:
            resource_assignment = gantt_data["assignments"]["rows"]
            assign = {}
            for r in resource_assignment:
                
                rid = "RID" + str(r["resource"])
                jid = "JID" + str(r["event"]- 10000000)
                if jid not in assign:
                    assign[jid] = [rid]
                else:
                    assign[jid].append(rid)
            for jid in assign:
                #print(job_db[jid])
                job_db[jid]["resource"] = assign[jid]
        
        base = get_current_time_string()
        schedule_filename = "schedule_" + base + ".json"
        save_json_to_file(db, filename=schedule_filename)

        count = SQLScheduleDB.query.filter(SQLScheduleDB.status=="scheduled").count()
        index = "tmp" + str(count + 1)
        
        db_schedule = SQLScheduleDB(index=index, time=base, schedule_result= db, schedule_status="sync", 
                      gantt_result=gantt_data, gantt_status="sync",status="scheduled")
        sql_db.session.add(db_schedule)
        sql_db.session.commit()
    
        return "success gantt to schedule"
 
class GanttFormat(Resource):  #TODO:json
    def get(self):
        import os
        path = latest_file()
        base = os.path.basename(path)
        base = os.path.splitext(base)[0].split('schedule_')[1]

        db = import_json(path=path,filename="")
        
        fg = FormattingGantt(base)
        wip_df = import_csv('wip_df12291041.csv')
        
        sol = fg.assign(db,wip_df)
        gantt_filename = "gantt_" + base + ".json"         
        save_json_to_file(sol, filename=gantt_filename)
        return sol
