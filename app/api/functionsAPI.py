import os
from flask_restful import Resource, request
from flask import jsonify, current_app, abort
from sqlalchemy import desc
from sqlalchemy import or_, and_
import pandas as pd

from app.api.utils import import_json, latest_file, import_excel, save_json_to_file, latest_MES_file
from app.api.utils import get_current_time_string, import_excel, convert_str_to_datetime, convert_datetime_to_str

from app.base.models import SQLProjectModel, SQLProductModel, SQLJobModel, SQLProcessModel, SQLSchedulerSetting, SQLMaterialModel, SQLJobLib
from app.base.models import SQLWorkingDays, SQLWorkingHours, SQLResourceModel, SQLScheduleDB, SQLNotificationModel, SQLErrorSetting
from sqlalchemy.orm.attributes import flag_modified    
from app.local_config import CUSTOMER_NAME, CUSTOMER_MES_API

from app import db as sql_db
import logging

from config import Config
logging.getLogger().setLevel(logging.INFO)
import time

class ExcelMapper(Resource):
    def get():
        info = request.json
        pass
        

class MESUpdate(Resource):
    
    def put(self,sid):
        from app.tasks import celery_reselect_schedule_result, expand_details, group_calculate_bottleneck, group_calculate_working_hours 
        celery_reselect_schedule_result(sid).delay()
        expand_details.delay() #展開工作備註
        group_calculate_bottleneck.delay() #計算瓶頸製程
        group_calculate_working_hours.delay() #計算預計工時
    def get(self):
        logging.info("=======DEBUG:ENTER MES RELEASE=======")
        from app.tasks import celery_get_schedule_result, expand_details, group_calculate_bottleneck, group_calculate_working_hours, celery_get_schedule_summary
        from app.api.schedulerAPI import Schedule
        sid = celery_get_schedule_result.delay().get()
        
        Schedule.put(sid=sid,info={"status":"已發行，展開工作備註中"})
        expand_details.delay()
        Schedule.put(sid=sid,info={"status":"已發行，計算瓶頸製程中"})
        group_calculate_bottleneck.delay()
        Schedule.put(sid=sid,info={"status":"已發行，計算預計工時中"}) 
        group_calculate_working_hours.delay()
        summary = celery_get_schedule_summary.delay().get()
        Schedule.put(sid=sid,info={"status":"已發行","schedule_summary":summary}) 
        
        
    def post(self):
        from app.tasks import celery_get_mes_update, group_check_abnormal
        celery_get_mes_update()
        group_check_abnormal.delay()

class CreateSchedulerSetting(Resource):
    def get(self):
        pass
    def post(self):
        info = request.json
        if info["dispatching_method"] == None:
            dispatching_method = True
        else:
            dispatching_method = True if info["dispatching_method"] == "true" else False
        
        setting = SQLSchedulerSetting(
            created_time = get_current_time_string(),
            ml_model = "BPNN" if info["ml_model"] == None else info["ml_model"] ,
            sequencing_method = "order_due" if info["sequencing_method"] == None else info["sequencing_method"], 
            dispatching_method = dispatching_method 
        )
        
        if info["reschedule_timer"] != None:
            setting.reschedule_timer = info["reschedule_timer"]
        sql_db.session.add(setting)
        sql_db.session.commit()
        

class UpdateDB(Resource):
    
    @staticmethod
    def get_latest_schedule():
        res = SQLScheduleDB.query.filter(SQLScheduleDB.status=="scheduled").order_by(desc(SQLScheduleDB.time)).first()
        return res

    @staticmethod
    def updateEventJob():
        
        SQLJobEntities = SQLJobModel.query.filter(SQLJobModel.status=="pause").all()
        
        for sql_j in SQLJobEntities:
            sql_j.plan_start_time = None
            sql_j.plan_end_time = None

        sql_db.session.commit()

    def get(self):
        latest_schedule = self.get_latest_schedule()
        tmp_data = latest_schedule.schedule_result
        from app.agents import dataAgent
        
        # print("-- create product --")
        # create_product(tmp_data["product_db"])
        print("-- update jobs--")
        dataAgent.distribute_jobs(tmp_data["job_db"],tmp_data["product_db"])
        
        print("-- update resources --")
        dataAgent.distribute_resources(tmp_data["schedule"]["res"])
        print("-- update products --")
        dataAgent.update_product(tmp_data["product_db"])
        print("-- update projects -- ")
        dataAgent.update_project(tmp_data["project_db"])

        self.updateEventJob()
        
        latest_schedule.status = "released"
        sql_db.session.commit()
        from app.tasks import group_calculate_bottleneck, group_calculate_working_hours
        group_calculate_bottleneck.delay()
        group_calculate_working_hours.delay()

        return {
                'message': 'update db success'
            }, 200
     

class GetScheduleJSON(Resource):
    def get(self):
        print(latest_file())
        db = import_json(path=latest_file(),filename="")
        return db


