
from app.pages import Dashboard
from app.api.utils import AlchemyEncoder
from app import db as sql_db
from app.base.models import SQLJobModel, SQLJobLib
def get_schedule_detail_dashboard():
    project_list = Dashboard.project_query_form()
    process_list = Dashboard.process_query_form()
    return project_list, process_list
def get_schedule_table():
    tupleEntities = sql_db.session.\
        query(SQLJobModel, SQLJobLib).\
        join(SQLJobLib).\
        order_by(SQLJobModel.project_id, SQLJobModel.plan_start_time).all()
    
    table = AlchemyEncoder().default(tupleEntities,mode="time")
    # time_spend_info = get_time_spent(tupleEntities) 
    # for jid in time_spend_info:
    #     table[jid]["time_spend"] = time_spend_info[jid]
    return table
    
def get_time_spent(tupleEntities):
    from app.agents.jobAgent import JobAgent
    res = {}
    for tup in tupleEntities:
        sql_j = tup[0]
        ja = JobAgent(jid=sql_j.index, sql_j=sql_j)
        res[sql_j.index] = ja.calculate_time_spent_total()
    return res

def get_schedule_info():
    project_list, process_list = get_schedule_detail_dashboard()
    table = get_schedule_table()
    return [ project_list, process_list, table]
