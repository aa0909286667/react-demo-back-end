# -*- encoding: utf-8 -*-
"""
License: Commercial
Copyright (c) 2019 - present AppSeed.us
"""

# Author: Joy Jen
# Updated On: 1/21

from app.home import blueprint
from flask import render_template, redirect, url_for, request, Response
from flask_login import login_required, current_user
# from flask_user import roles_required

from jinja2 import TemplateNotFound
from app.api.utils import get_current_time_string, get_current_day_string, get_current_time, convert_datetime_to_str
from datetime import datetime, timedelta
import requests
import os
import json
import logging

from config import Config
logging.getLogger().setLevel(logging.INFO)

@blueprint.route('/index')
# @roles_required('admin')
@login_required
def index():
    DASHBOARD = json.loads(requests.get(Config.SARA_PORT + '/api/get_dashboard_info').text)
    PROJECT = json.loads(requests.get( Config.SARA_PORT + '/api/get_project').text)
    return render_template('index-dn.html',PROJECT=PROJECT, DASHBOARD=DASHBOARD)


@blueprint.route('/get_started', methods=['GET', 'POST'])
@login_required
def get_started():
    
    if request.method == "POST":
        uploaded_files = request.files.getlist("file")
        uploaded_file = uploaded_files[0]

        #uploaded_file = request.files.get("file")
        if 'quick_start' in request.form:
            mode = "quick_start"
            response = requests.get(Config.SARA_PORT + '/api/quick_start/{}'.format(mode))
            return redirect(url_for('home_blueprint.index')) 
            
        if ".xlsx" in uploaded_file.filename:
            DATA_PATH = os.path.join("app","data")
            fp = os.path.join(DATA_PATH,"input.xlsx")
            mapping_fp = os.path.join(DATA_PATH,"input_key_mapper.json")

            uploaded_file.save(fp)
            uploaded_files[1].save(mapping_fp)
            response = requests.get(Config.SARA_PORT + '/api/quick_start/custom')
    
            return redirect(url_for('home_blueprint.index'))
   
    return render_template('get-started.html')

@blueprint.route('/import_data', methods=['GET', 'POST'])
@login_required
def import_data():

    return render_template('import-data.html')

@blueprint.route('/product_detail/<product_id>')
@login_required
def product_detail(product_id):

    response = requests.get(Config.SARA_PORT + '/api/get_product/{}'.format(product_id))
    PRODUCT = json.loads(response.text)
    PROCESS_LIST = json.loads(requests.post(Config.SARA_PORT + '/api/get_dashboard_info/process_query_form').text)
    JLB_LIST = json.loads(requests.post(Config.SARA_PORT + '/api/get_dashboard_info/jlb_name_query_form').text)
    
    return render_template("product-detail-dn.html", PRODUCT=PRODUCT, PROCESS_LIST=PROCESS_LIST, JLB_LIST=JLB_LIST)

@blueprint.route('/project_schedule/<pid>')
@login_required
def project_schedule(pid):
    response = requests.get(Config.SARA_PORT + '/api/get_project/{}'.format(pid))
    PROJECT = json.loads(response.text)

    return render_template("project-schedule-dn.html", PROJECT=PROJECT,PROJECT_ID=pid)

@blueprint.route('/project_schedule_add/<pid>')
@login_required
def project_schedule_add(pid):
    response = requests.get(Config.SARA_PORT + '/api/get_project/{}'.format(pid))
    PROJECT = json.loads(response.text)
    PROCESS_LIST = json.loads(requests.post(Config.SARA_PORT + '/api/get_dashboard_info/process_query_form').text)
    JLB_LIST = json.loads(requests.post(Config.SARA_PORT + '/api/get_dashboard_info/jlb_name_query_form').text)

    return render_template("project-schedule-add-dn.html", PROJECT=PROJECT, PROJECT_ID=pid, PROCESS_LIST=PROCESS_LIST, JLB_LIST=JLB_LIST)

@blueprint.route('/process_list', methods=['GET'])
@login_required
def process_list(): #製程列表？
    response = requests.get(Config.SARA_PORT + '/api/get_job_list')
    JOB_LIST = json.loads(response.text)
    return render_template("process-list-dn.html",JOB_LIST=JOB_LIST)
    
@blueprint.route('/tbl_material',methods=['GET'])
@login_required
def tbl_material():
    response = requests.get(Config.SARA_PORT + '/api/get_material')
    MATERIAL = json.loads(response.text)
    return render_template("tbl-material-dn.html",MATERIAL =MATERIAL )

@blueprint.route('/job_detail/<jid>', methods=['GET', 'POST'])
@login_required
def job_detail(jid):
    response = requests.get(Config.SARA_PORT + '/api/get_job/{}'.format(jid))
    data = json.loads(response.text)
    JOB = data[0]
    ERROR_TABLE = data[1]
    
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        if 'split' in request.form:
            num = int(request.form['num'])
            
            urlstring = Config.SARA_PORT + '/api/split/{}:{}'.format(jid,num) 
            response = requests.post(urlstring)
            #data_db = json.loads(response.text)
            #job_id = job_id + "_001"
        
        elif 'start' in request.form:
            urlstring = Config.SARA_PORT + '/api/update_job/{}:start'.format(jid)
            print(urlstring)

            response = requests.put(urlstring)
        elif 'pause' in request.form:
            info = {
                "good_qty": int(request.form.get('good-qty')),
                "bad_qty": int(request.form.get('bad-qty'))
            }

            urlstring = Config.SARA_PORT + '/api/update_job/{}:pause'.format(jid) 
            response = requests.put(urlstring,json=info)

        elif 'end' in request.form:
            info = {
                "good_qty": int(request.form.get('good-qty')),
                "bad_qty": int(request.form.get('bad-qty'))
            }
            
            urlstring = Config.SARA_PORT + '/api/update_job/{}:end'.format(jid)
            response = requests.put(urlstring,json=info)
        if 'errorReport' in request.form:
            select = request.form.get('custom-select')
            logging.info("{}".format(select))
            info = {
                "jid": jid,
                "rid": None,
                "event_type": select,
                "start_time": None,
                "end_time": None
            }
            urlstring = Config.SARA_PORT + "/api/update_job/{}:report".format(jid)
            response = requests.put(urlstring,json=info)
        elif 'errorResume' in request.form:
            select = []
            length = len(JOB['ongoing_error'])
            for i in range(1,length+1):
                id_name = 'error_check_box' + str(i) 
                
                if id_name in request.form:
                    select.append(request.form.get(id_name))
            info = {
                "jid": jid,
                "event_type": select
            }
            
            urlstring = Config.SARA_PORT + "/api/update_job/{}:resume".format(jid)
            response = requests.put(urlstring,json=info)
            
            
            

        response = requests.get(Config.SARA_PORT + '/api/get_job/{}'.format(jid))
        data = json.loads(response.text)
        JOB  = data[0]
        ERROR_TABLE = data[1]
        return render_template('job-detail-dn.html', JOB=JOB, ERROR_TABLE=ERROR_TABLE)

    else:

        return render_template('job-detail-dn.html', JOB=JOB, ERROR_TABLE=ERROR_TABLE)

@blueprint.route('job_performance_list',methods=['GET','POST'])
@login_required
def job_performance_list():
    import ast
    def filter_method(JOB,key,value):
        if value == "all":
            return

        remove = []
        for jid in JOB:
            if type(value) == list:
                if JOB[jid][key] not in value:
                    remove.append(jid)
            else:        
                if value != JOB[jid][key]:
                    remove.append(jid)
        for r in remove:
            JOB.pop(r,None)
    def filter_time(JOB, n):
        
        t = convert_datetime_to_str(get_current_time() + timedelta(hours=n))
        remove = []
        for jid in JOB:
            if JOB[jid]["plan_start_time"] is not None and job_info[jid]["plan_start_time"] > t:
                remove.append(jid)
            if JOB[jid]["real_end_time"] is not None and job_info[jid]["real_end_time"] > t: 
                remove.append(jid)
        for jid in remove:
            JOB.pop(jid,None)


    response = requests.get(Config.SARA_PORT + '/api/get_job_performance')
    
    sol = json.loads(response.text)
    

    table = {}
    full_table = {}
    DASHBOARD = {
        "operation_delay":0,
        "time_abnormal": 0,
        "sequence_abnormal": 0,
        "abnormal_state":0
    }
    if sol != None:

        # value1: operation_delay
        operation_delay = sol["operation_delay"]
        time_abnormal = sol["working_hours_mismatch"]
        sequence_abnormal = sol["abnormal_sequence"]
        abnormal_state = sol["abnormal_report"]

        for JOB in [operation_delay,time_abnormal,sequence_abnormal,abnormal_state]:
            filter_method(JOB, "status", ["running","ready","scheduled","pause"])
        if request.method == "POST":
            if 'query_options' in request.form:
                tmp = request.form.get('query_options')
                QUERY = ast.literal_eval(tmp)
                print(QUERY)
                if "time_option" in QUERY:
                    for JOB in [operation_delay,time_abnormal,sequence_abnormal,abnormal_state]:
                        filter_time(JOB, int(QUERY['time_option']))
                if 'process_option' in QUERY:
                    for JOB in [operation_delay,time_abnormal,sequence_abnormal,abnormal_state]:
                        filter_method(operation_delay, "process_name", QUERY['process_option'])
                if 'jlb_option' in QUERY:
                    for JOB in [operation_delay,time_abnormal,sequence_abnormal,abnormal_state]:
                        filter_method(operation_delay, 'jlb_name', QUERY['jlb_option'])


        DASHBOARD["operation_delay"] = len(operation_delay)
        for jid in operation_delay:
            full_table["operation_delay{}".format(jid)] = sol["operation_delay"][jid]
            full_table["operation_delay{}".format(jid)]["abnormal"] = "工作延遲開始"
            full_table["operation_delay{}".format(jid)]["jid"] = jid
        
        # value2: time_abnormal
        
        DASHBOARD["time_abnormal"] = len(time_abnormal)
        for jid in time_abnormal:
            full_table["working_hours_mismatch{}".format(jid)] = sol["working_hours_mismatch"][jid]
            full_table["working_hours_mismatch{}".format(jid)]["abnormal"] = "工時異常"
            full_table["working_hours_mismatch{}".format(jid)]["jid"] = jid

        # value3: sequence_abnormal
        
        DASHBOARD["sequence_abnormal"] = len(sequence_abnormal)
        for jid in sequence_abnormal:
            full_table["abnormal_sequence{}".format(jid)] = sol["abnormal_sequence"][jid]
            full_table["abnormal_sequence{}".format(jid)]["abnormal"] = "未按次序"
            full_table["abnormal_sequence{}".format(jid)]["jid"] = jid
                
        # value4: abnormal_state
        
        DASHBOARD["abnormal_state"] = len(abnormal_state)
        for jid in abnormal_state:
            full_table["abnormal_report{}".format(jid)] = sol["abnormal_report"][jid]
            full_table["abnormal_report{}".format(jid)]["abnormal"] = "異常回報"
            full_table["abnormal_report{}".format(jid)]["jid"] = jid
        
        

        return render_template('job_performance-dn.html', JOB=full_table,DASHBOARD=DASHBOARD)
                                



@blueprint.route('/inprogress_jobs', methods=['GET', 'POST'])
@login_required
def inprogress_jobs():
    
    def filter_method(JOB,key,value):
        if value == "all":
            return

        remove = []
        for jid in JOB:
            if value != JOB[jid][key]:
                remove.append(jid)
        for r in remove:
            JOB.pop(r,None)
    def filter_time(JOB, n):
        
        t = convert_datetime_to_str(get_current_time() + timedelta(hours=n))
        remove = []
        for jid in JOB:
            if JOB[jid]["plan_start_time"] is not None and job_info[jid]["plan_start_time"] > t:
                remove.append(jid)
            if JOB[jid]["real_end_time"] is not None and job_info[jid]["real_end_time"] > t: 
                remove.append(jid)
        for jid in remove:
            JOB.pop(jid,None)
    
    response = requests.get(Config.SARA_PORT + '/api/get_inprogress_jobs')
    JOB = json.loads(response.text)
    
    import ast
    
    if request.method == "POST":
        
        if 'query_options' in request.form:
            tmp = request.form.get('query_options')
            QUERY = ast.literal_eval(tmp)
            print(QUERY)
            if "time_option" in QUERY:
                filter_time(JOB, int(QUERY['time_option']))
            if 'process_option' in QUERY:
                filter_method(JOB, "process_name", QUERY['process_option'])
            if 'jlb_option' in QUERY:
                filter_method(JOB, 'jlb_name', QUERY['jlb_option'])
            
    
    return render_template("inprogress_job-dn.html", JOB=JOB)



@blueprint.route('/resource_schedule', methods=['GET', 'POST'])
@login_required
def resource_schedule():
    
    def filter_time(job_info, n):
        t = convert_datetime_to_str(get_current_time() + timedelta(hours=n))
        remove = []
        for jid in job_info:
            if job_info[jid]["plan_start_time"] is not None and job_info[jid]["plan_start_time"] > t:
                remove.append(jid)
            if job_info[jid]["real_end_time"] is not None and job_info[jid]["real_end_time"] > t: 
                remove.append(jid)
        print(remove)
        for jid in remove:
            job_info.pop(jid,None)
        
    rid = request.args.get('rid',None)
    
    response = requests.get(Config.SARA_PORT + '/api/get_resource/{}'.format(rid))
    data = json.loads(response.text)
    RESOURCE = data[0]
    
    if 'time' in request.args:
        time = int(request.args.get('time',None))
        filter_time(RESOURCE["jid_list"], time)
    
    ERROR_TABLE = data[1]
    
    if request.method == "POST":
        logging.info("==========Resource Schedule DEBUG:{}===========".format(request.form))
    
        if 'errorReport' in request.form:
            select = request.form.get('custom-select')
            logging.info("{}".format(select))
            info = {
                "jid": None,
                "rid": rid,
                "event_type": select,
                "start_time": None,
                "end_time": None
            }
            urlstring = Config.SARA_PORT + "/api/get_resource/{}:report".format(rid)
            response = requests.put(urlstring,json=info)

        elif 'errorResume' in request.form:
            select = []
            length = len(RESOURCE['ongoing_error'])
            for i in range(1,length+1):
                id_name = 'error_check_box' + str(i) 
                print(id_name)
                if id_name in request.form:
                    select.append(request.form.get(id_name))
            
            info = {
                "rid": rid,
                "event_type": select
            }
            print(info)

            urlstring = Config.SARA_PORT + "/api/get_resource/{}:resume".format(rid)
            response = requests.put(urlstring,json=info)

        response = requests.get(Config.SARA_PORT + '/api/get_resource/{}'.format(rid))
        data = json.loads(response.text)
            
        RESOURCE = data[0]
        ERROR_TABLE = data[1]

    return render_template("resource-schedule-dn.html", RESOURCE=RESOURCE, ERROR_TABLE=ERROR_TABLE)


@blueprint.route('/work_center',methods=['GET', 'POST'])
@login_required
def work_center(process=None,jlb=None,time=None):
    def resources_info(schedule):
        info = dict(
            available_resource = 0,
            idle_resource_count = 0,
            resource_abnormal = 0)

        for rid in SCHEDULE:
            if SCHEDULE[rid]["resource_color"] in [3,4]:
                info["available_resource"] += 1
            if SCHEDULE[rid]["resource_color"] == 4:
                info["idle_resource_count"] +=1
            if SCHEDULE[rid]["resource_color"] in [1,2]:
                info["resource_abnormal"] +=1
        return info

    def jobs_info(schedule):
        info = dict(
            job_abnormal = 0,
            remain_goals = 0,
            inprogress_count = 0
        )
        abnormal_jid = set()
        for rid in SCHEDULE:
            job_info = SCHEDULE[rid]["job_list"]
            for i in job_info:
                if i["status"] == "running":
                    info["inprogress_count"] +=1
                if i["status"] == "scheduled":
                    info["remain_goals"] +=1
                if i["abnormal"] == True:
                    abnormal_jid.add(i["job_id"])
        info["job_abnormal"] = len(abnormal_jid)
        return info
        
    def get_dashboard(schedule):
        info = resources_info(schedule)
        info.update(jobs_info(schedule))
        return info
        
    def filter_method(key, value, SCHEDULE):
        if value =="all":
            return SCHEDULE

        tmp = {}
        for rid in SCHEDULE:
            job_info = SCHEDULE[rid]["job_list"]
            for i in job_info:
                if value == i[key] and rid not in tmp:
                    tmp[rid] = SCHEDULE[rid]
                    tmp[rid]["job_list"] = [i]
                elif value == i[key] and rid in tmp:
                    tmp[rid]["job_list"].append(i) 
        return tmp
        
    def filter_time(SCHEDULE, n):
        
        t = convert_datetime_to_str(get_current_time() + timedelta(hours=n))
        tmp = {}
        for rid in SCHEDULE:
            job_info = SCHEDULE[rid]["job_list"]
            for i in job_info:
                if i["plan_start_time"] is None and i["real_start_time"] is None:
                    continue
                if i["plan_start_time"] is None and i["real_start_time"] <= t:
                    if rid not in tmp:
                        tmp[rid] = SCHEDULE[rid]
                        tmp[rid]["job_list"] = [i]
                    else:
                        tmp[rid]["job_list"].append(i) 
                elif i["plan_start_time"] <= t and rid not in tmp:
                    tmp[rid] = SCHEDULE[rid]
                    tmp[rid]["job_list"] = [i]
                elif i["plan_start_time"] <= t and rid in tmp:
                    tmp[rid]["job_list"].append(i) 
        return tmp

    def filter_virtual_resource(SCHEDULE):
        remove_resource = []
        for rid in SCHEDULE:
            if SCHEDULE[rid]["resource_type"] == "virtual":
                remove_resource.append(rid)
        for rid in remove_resource:
            SCHEDULE.pop(rid,None)
        return SCHEDULE

    def filter_jlb_list(JLB_LIST,process_name):
        remove = []
        for jlb_name in JLB_LIST:
            if JLB_LIST[jlb_name]["process_name"] != process_name:
                remove.append(jlb_name)
        for r in remove:
            JLB_LIST.pop(r,None)
        return JLB_LIST

    response = requests.get(Config.SARA_PORT + '/api/get_work_center_info')
    doc = json.loads(response.text)
    if len(doc[2]) == 0:
        return render_template("work-center-dn_v2.html")
    
    PROCESS_LIST, JLB_LIST, SCHEDULE, SCHEDULE_VERSION, MES_VERSION = doc[0], doc[1], doc[2], doc[3], doc[4]
    QUERY = {}
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        if "jlb_option" in request.form and request.form.get('jlb_option') != 'all':
            SCHEDULE  = filter_method('jlb_name', request.form.get('jlb_option'), SCHEDULE)
            QUERY["jlb_option"] = request.form.get('jlb_option')
            QUERY["process_option"] = JLB_LIST[request.form.get('jlb_option')]["process_name"]
        if "process_option" in request.form:
            SCHEDULE  = filter_method('process_name', request.form.get('process_option') , SCHEDULE)
            QUERY["process_option"] = request.form.get('process_option') 
            JLB_LIST = filter_jlb_list(JLB_LIST, QUERY["process_option"] )
        if "time_option" in request.form:
            SCHEDULE = filter_time(SCHEDULE, int(request.form.get('time_option')))
            QUERY["time_option"] = request.form.get('time_option')  
        if "schedule_version" in request.form:
            sid = str(request.form.get('schedule_version') )
            response = requests.put(Config.SARA_PORT + '/api/mes_update/' + sid)
            return redirect(url_for('home_blueprint.work_center'))
            
            


    DASHBOARD = get_dashboard(SCHEDULE)
    SCHEDULE = filter_virtual_resource(SCHEDULE)
    
    return render_template("work-center-dn_v2.html", JLB_LIST=JLB_LIST, PROCESS_LIST=PROCESS_LIST, \
                                                     SCHEDULE=SCHEDULE, DASHBOARD=DASHBOARD, \
                                                     QUERY=QUERY, SCHEDULE_VERSION=SCHEDULE_VERSION,
                                                     MES_VERSION = MES_VERSION)


@blueprint.route('/resource_list', methods=['GET', 'POST'])
@login_required
def resource_list():

    response = requests.get(Config.SARA_PORT + '/api/get_calendar')
    WORKINGDAYS_TABLE = json.loads(response.text) 
    response = requests.get(Config.SARA_PORT + '/api/get_shift')
    WORKINGHOURS_TABLE = json.loads(response.text) 
    RESOURCE = json.loads(requests.get(Config.SARA_PORT + '/api/manage_resources').text)
    if request.method == "POST": 
        logging.info("==========DEBUG:{}===========".format(request.form))
        rid = request.form.get('rid')
        if "day_off" in request.form:
            
            index = len(RESOURCE[rid]["day_off"])
            info = {
                "rid": rid,
                "add_day_off_list"[index] : {
                    "event_time": request.form.getlist('day_off'),
                    "event_type": request.form.get('event_type') 
                }
            }
            
            requests.post(Config.SARA_PORT + '/api/manage_resources/add_day_off',json=info)

        if "lock_time" in request.form:
            info = {
                "rid": rid,
                "lock_time": request.form.get('lock_time')  
            }
            
            requests.post(Config.SARA_PORT + '/api/manage_resources/edit_lock_time',json=info)


    return render_template("resource-list-dn.html", RESOURCE=RESOURCE, WORKINGHOURS_TABLE=WORKINGHOURS_TABLE, WORKINGDAYS_TABLE=WORKINGDAYS_TABLE)

@blueprint.route('/tbl_datatable_outsourcing',methods=['GET','POST'])
@login_required
def tbl_datatable_outsourcing():
    
    TABLE_ARRAY = json.loads(requests.get(Config.SARA_PORT + '/api/get_outsourcing').text)
    JOB_TABLE = TABLE_ARRAY[0]
    RESOURCE_TABLE = TABLE_ARRAY[1]
    return render_template("tbl_datatable_outsourcing-dn.html", JOB_TABLE  = JOB_TABLE, RESOURCE_TABLE = RESOURCE_TABLE )

@blueprint.route('/schedule_detail', methods=['GET', 'POST'])
@login_required
def schedule_detail():

    sol = json.loads(requests.get(Config.SARA_PORT + '/api/get_schedule_detail').text)
    PROJECT_LIST, PROCESS_LIST, TABLE = sol[0], sol[1], sol[2]

    return render_template('schedule-detail-dn.html',PROJECT_LIST=PROJECT_LIST,PROCESS_LIST=PROCESS_LIST, TABLE=TABLE)

@blueprint.route('/schedule_comparisons',methods=['GET','POST'])
# @roles_required('admin')
@login_required
def schedule_comparisons():
    TIMEFORM= json.loads(requests.get(Config.SARA_PORT + '/api/get_schedule_comparision').text)
    first = TIMEFORM["scheduled"][0]
    if first in  TIMEFORM["released"]:
        TIMEFORM["released"].remove(first)

    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        if "scheduled_time_option" in request.form and "released_time_option" not in request.form:
            
            time = request.form.get("scheduled_time_option")
            if time in TIMEFORM["released"]:
                 TIMEFORM["released"].remove(time)
            
            index_pair = "{},{}".format(time,"")

            dashboard = json.loads(requests.post(Config.SARA_PORT + '/api/get_schedule_comparision/{}'.format(index_pair)).text)
            SCHEDULED_RESULT1 = dashboard[time]
            

            return render_template('schedule_comparisons-dn.html', TIMEFORM=TIMEFORM,OPTION1=time,SCHEDULED_RESULT1=SCHEDULED_RESULT1)

        elif "released_time_option" in request.form:
            time1 = request.form.get("time1")
            time2 = request.form.get("released_time_option")
            index_pair = "{},{}".format(time1,time2)
            print(time1)
            print(time2)
            
            dashboard = json.loads(requests.post(Config.SARA_PORT + '/api/get_schedule_comparision/{}'.format(index_pair)).text)
            SCHEDULED_RESULT1 = dashboard[time1]
            SCHEDULED_RESULT2 = dashboard[time2]
            print(dashboard)
            return render_template('schedule_comparisons-dn.html', TIMEFORM=TIMEFORM,SCHEDULED_RESULT1=SCHEDULED_RESULT1,SCHEDULED_RESULT2=SCHEDULED_RESULT2, OPTION1=time1, OPTION2=time2)
        
    return render_template('schedule_comparisons-dn.html', TIMEFORM=TIMEFORM)

@blueprint.route('/bottleneck_detail/<jlb_name>',methods=['GET','POST'])
# @roles_required('admin')
@login_required
def bottleneck_detail(jlb_name):
    response = requests.get(Config.SARA_PORT + '/api/get_bottleneck/' + jlb_name)
    BOTTLENECK = json.loads(response.text)
    info = {}
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        if "threshold" in request.form:
            info["job_name"] = request.form.get('eventId1') 
            info["warning"] = request.form.get('warning')
            info["urgent"] = request.form.get('urgent')
        elif "ignore" in request.form:
            info["job_name"] = request.form.get('eventId2')
            info["ignore"] = request.form.get('ignore')

        requests.post(Config.SARA_PORT + '/api/get_bottleneck_setting',json=info)
        response = requests.get(Config.SARA_PORT + '/api/get_bottleneck')
        BOTTLENECK = json.loads(response.text)
            
    return render_template('bottleneck-detail-dn.html',BOTTLENECK=BOTTLENECK)


@blueprint.route('/bottleneck_analytics',methods=['GET','POST'])
# @roles_required('admin')
@login_required
def bottleneck_analytics():
    response = requests.get(Config.SARA_PORT + '/api/get_bottleneck')
    BOTTLENECK = json.loads(response.text)
    info = {}
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        if "threshold" in request.form:
            info["job_name"] = request.form.get('eventId1') 
            info["warning"] = request.form.get('warning')
            info["urgent"] = request.form.get('urgent')
        elif "ignore" in request.form:
            info["job_name"] = request.form.get('eventId2')
            info["ignore"] = request.form.get('ignore')
        
        print(info)

        requests.put(Config.SARA_PORT + '/api/get_bottleneck_setting/' + info["job_name"],json=info)
        response = requests.get(Config.SARA_PORT + '/api/get_bottleneck')
        BOTTLENECK = json.loads(response.text)
            
    return render_template('bottleneck-analytics-dn.html',BOTTLENECK=BOTTLENECK)


@blueprint.route('/performance',methods=['GET','POST'])
# @roles_required('admin')
@login_required
def performance():

    def filter_time(df, n=7):
        current_time = get_current_time_string()
        timeflag = convert_datetime_to_str(get_current_time() + timedelta(days=n),mode="day")
        print(df)
        return df[(df["Time"]>=current_time) & (df["Time"]<=timeflag)]

    def categorized_loading(phase1):
        days =  phase1['Time'].unique()
        days.sort()
        dataProvider = []
        for day in days:
            total_resource = len(phase1[phase1['Time']==day])
            
            below = len(phase1[(phase1['Time']==day) & (phase1['Loading'] < 0.4)])
            average = len(phase1[(phase1['Time']==day) & (phase1['Loading']<0.8) & (phase1['Loading']>0.4)])
            above = len(phase1[(phase1['Time']==day) & (phase1['Loading']>0.8)])
            
            res = {
                "day": day,
                "value1":total_resource,
                "value2":average, 
                "value3":above,
                "value4":below
            }
            dataProvider.append(res)
        tmp = phase1.to_json(orient="index")
        sol = json.loads(tmp)
        return dataProvider, sol

    json_resource = json.loads(requests.get(Config.SARA_PORT + '/api/get_resource_performance').text)
    import pandas as pd
    resource_df = pd.DataFrame(data=json_resource)
    if len(resource_df) > 0:
        resource_df.columns =['Time', 'Resource Name', 'Resource Type','Resource Jobs','Cost', 'Loading'] 
    phase1 = None
    if request.method == 'POST':
        
        if "day_option" in request.form:
            daytype = int(request.form.get('day_option'))
            phase1 = filter_time(resource_df, daytype)
            dataProvider, sol = categorized_loading(phase1)
            
            return render_template('performance-dn.html', PERFORMANCE_TABLE=dataProvider, TABLE=sol, OPTION=daytype)
        
        elif "job_option" in request.form:
            response = requests.get(Config.SARA_PORT + '/api/get_job_performance')
            if response.status_code == 400:
                return render_template('performance-dn.html')
            else:
                sol = json.loads(response.text)
            

            table = {}
            full_table = {}
            if sol != None:
            
            
                daytype = request.form.get('job_option')
                current_time = get_current_time()
                dataProvider = {}
                for i in reversed(range(int(daytype))): 
                    tmp = {
                        "day": (current_time -  timedelta(days=i)).strftime('%Y-%m-%d'),
                        "value1":0,
                        "value2":0,
                        "value3":0,
                        "value4":0
                    }
                    dataProvider[tmp["day"]] = tmp

                # value1: operation_delay
                operation_delay = sol["operation_delay"]
                table["operation_delay"] = {}
                for jid in operation_delay:
                    time = operation_delay[jid]["plan_start_time"].split(' ')[0]
                    if time in dataProvider:
                        dataProvider[time]["value1"] +=1
                         
                        full_table["operation_delay{}".format(jid)] = sol["operation_delay"][jid]
                        full_table["operation_delay{}".format(jid)]["abnormal"] = "工作延遲開始"
                        full_table["operation_delay{}".format(jid)]["jid"] = jid
                        
                        table["operation_delay"][jid] = sol["operation_delay"][jid]
                 
                    
                
                # value2: time_abnormal
                time_abnormal = sol["working_hours_mismatch"]
                table["working_hours_mismatch"] = {}
                for jid in time_abnormal:
                    time = time_abnormal[jid]["real_start_time"].split(' ')[0]
                    if time in dataProvider:
                        dataProvider[time]["value2"] +=1
                         
                        full_table["working_hours_mismatch{}".format(jid)] = sol["working_hours_mismatch"][jid]
                        full_table["working_hours_mismatch{}".format(jid)]["abnormal"] = "工時異常"
                        full_table["working_hours_mismatch{}".format(jid)]["jid"] = jid
                        
                        table["working_hours_mismatch"][jid] = sol["working_hours_mismatch"][jid]
                        

                # value3: sequence_abnormal
                sequence_abnormal = sol["abnormal_sequence"]
                table["abnormal_sequence"] = {}
                for jid in sequence_abnormal:
                    time = sequence_abnormal[jid]["real_start_time"].split(' ')[0]
                    if time in dataProvider:
                        dataProvider[time]["value3"] +=1 
                        full_table["abnormal_sequence{}".format(jid)] = sol["abnormal_sequence"][jid]
                        full_table["abnormal_sequence{}".format(jid)]["abnormal"] = "未按次序"
                        full_table["abnormal_sequence{}".format(jid)]["jid"] = jid

                        table["abnormal_sequence"][jid] = sol["abnormal_sequence"][jid]
                        
                # value4: abnormal_state
                abnormal_state = sol["abnormal_report"]
                table["abnormal_report"] = {}
                for jid in abnormal_state:
                    time = abnormal_state[jid]["event_start_time"].split(' ')[0]
                    if time in dataProvider:
                        dataProvider[time]["value4"] +=1
                        full_table["abnormal_report{}".format(jid)] = sol["abnormal_report"][jid]
                        full_table["abnormal_report{}".format(jid)]["abnormal"] = "異常回報"
                        full_table["abnormal_report{}".format(jid)]["jid"] = jid
                        table["abnormal_report"][jid] = sol["abnormal_report"][jid]
                        
                
                dataProvider_list = [dataProvider[day]for day in dataProvider]

                #phase1 = filter_time(resource_df)
                #dataProvider, sol = categorized_loading(phase1)

                return render_template('performance-dn.html',
                                        PERFORMANCE_TABLE_2=dataProvider_list, TABLE2=full_table, OPTION2=daytype, id="job_performance")
    if phase1 is None:
        if len(resource_df) == 0:
            return render_template('performance-dn.html')
        else:
           phase1 = filter_time(resource_df)
           dataProvider, sol = categorized_loading(phase1)

    return render_template('performance-dn.html',PERFORMANCE_TABLE=dataProvider, TABLE=sol, OPTION="7")
    
@blueprint.route('/delay_orders',methods=['GET','POST'])
@login_required
def delay_order():
    response = requests.get(Config.SARA_PORT + '/api/get_delay_order')
    DELAY = json.loads(response.text)
    return render_template('delay-orders-dn.html',DELAY=DELAY)

@blueprint.route('/delay_detail/<product_id>',methods=['GET','POST'])
@login_required
def delay_detail(product_id):
    response = requests.get(Config.SARA_PORT + '/api/get_delay_order/' + product_id)
    DELAY = json.loads(response.text)
    return render_template('delay-detail-dn.html',DELAY=DELAY)


@blueprint.route('/extend_job/<jid>', methods=['GET', 'POST'])
@login_required
def extend_job(jid):
    
    res = None
    if request.method == "POST":

        if 'phase1' in request.form:
            num = int(request.form['total'])
            logging.info("*** DEBUG: case1: {}".format(num))
            response = requests.get(Config.SARA_PORT + '/api/get_schedule_json')
            data_db = json.loads(response.text)
            JOB = data_db['job_db'][jid]
            return render_template('extend-job-dn.html',JOB_LENGTH=num,JOB=JOB)
        elif 'phase2' in request.form:
            num = int(request.form['phase2'])
            logging.info("*** DEBUG: success:{} ".format(num))
            job_info = []
            for i in range(num):
                j = {
                    "job_name":request.form["job_name"+str(i)],
                    "qty":int(request.form["qty"+str(i)]),
                    "est_time":int(request.form["est_time"+str(i)])
                }
                job_info.append(j)

            
            response = requests.get(Config.SARA_PORT + '/api/get_schedule_json')
            db = json.loads(response.text)
            db["job_info"] = job_info

            urlstring = Config.SARA_PORT + '/api/dpe/{}'.format(jid) 
            response = requests.post(urlstring,json=db)
            res = json.loads(response.text) #return new_db
            product_id = res["job_db"][jid]["product_id"]
            return redirect(url_for('home_blueprint.product_detail',product_id=product_id))
        elif 'add1' in request.form:
            
            info = request.form['text']
            
            info = info.split(',')
            logging.info("{}".format(info))
            

            response = requests.get(Config.SARA_PORT + '/api/get_schedule_json')
            db = json.loads(response.text)
            db["product_list"] = info
            
            urlstring = Config.SARA_PORT + '/api/add/{}'.format(jid) 
            response = requests.post(urlstring,json=db)
            res = json.loads(response.text) #return new_db
            product_id = res["job_db"][jid]["product_id"]
            return redirect(url_for('home_blueprint.product_detail',product_id=product_id))

            #JOB = db['job_db'][jid]
            #num = len(info)
            #return render_template('extend-job-dn.html',PRODUCT_LENGTH=num,JOB=JOB,INFO=info)
            
        #elif 'add2' in request.form:

        #    return render_template('work-center.html')

    else:
        response = requests.get(Config.SARA_PORT + '/api/get_schedule_json')
        data_db = json.loads(response.text)
        JOB = data_db['job_db'][jid]
        return render_template('extend-job-dn.html',JOB=JOB)

@blueprint.route('/add_product/<pid>',methods=['GET','POST'])
@login_required
def add_product(pid):
    PRODUCT_ID = pid
    return render_template('add-product.html',PRODUCT_ID= PRODUCT_ID)

@blueprint.route('/settings',methods=['GET','POST'])
@login_required
def settings():
    response = requests.get(Config.SARA_PORT + '/api/get_error_setting')
    ERROR_TABLE = json.loads(response.text)
    
    response = requests.get(Config.SARA_PORT + '/api/get_shift')
    WORKINGHOURS_TABLE = json.loads(response.text)
    response = requests.get(Config.SARA_PORT + '/api/get_calendar')
    WORKINGDAYS_TABLE = json.loads(response.text) 
    CALENDAR_EVENT = WORKINGDAYS_TABLE['WDID0']["events"]
    SELECTED_CALENDAR = WORKINGDAYS_TABLE['WDID0']['calendar_name'] 
     
    if request.method == 'POST':
        logging.info("==========DEBUG:{}===========".format(request.form))
        
        if 'scheduler_setting' in request.form:
            logging.info("==========DEBUG:{}===========".format(request.form))
            
            config = {
                
                "reschedule_timer": request.form.get('reschedule_timer') if "reschedule_timer_check" in request.form else None ,
                "ml_model": request.form.get('ml_model') if "ml_model_check" in request.form else None ,
                "sequencing_method": request.form.get('sequencing_method') if "sequencing_method_check" in request.form else None ,
                "dispatching_method": request.form.get('dispatching_method') if "dispatching_method_check" in request.form else None
            }
            
            response = requests.post(Config.SARA_PORT + '/api/scheduler_setting',json=config)

        elif 'material_setting' in request.form:
            pass
        elif 'selected_calendar' in request.form:
            
            wdid = request.form.get('selected_calendar')
            if wdid not in WORKINGDAYS_TABLE:
                CALENDAR_EVENT = []
                SELECTED_CALENDAR = "NEW"
            else:
                CALENDAR_EVENT = WORKINGDAYS_TABLE[wdid]["events"]
                SELECTED_CALENDAR = WORKINGDAYS_TABLE[wdid]["calendar_name"]


        elif 'create_shift' in request.form:
            info = {
                "hours_name" : request.form.get('hours_name'),
                "working_start_time" : request.form.get('working_hours_start'),
                "working_end_time":  request.form.get('working_hours_end'),
                "break_start_time": request.form.get('break_time_start'),
                "break_end_time": request.form.get('break_time_end') 
            }
            print(info)
            requests.post(Config.SARA_PORT + '/api/get_shift',json=info)

            response = requests.get(Config.SARA_PORT + '/api/get_shift')
            WORKINGHOURS_TABLE = json.loads(response.text)

        elif 'edit_shift' in request.form:

            whid = request.form.get('edit_shift')
            info = {
                "index": whid,
                "working_start_time": request.form.get("modifyShiftStart_" + whid),
                "working_end_time":  request.form.get("modifyShiftEnd_" + whid), 
                "break_start_time": request.form.get("createBreakStart_" + whid),
                "break_end_time": request.form.get('createBreakEnd_' + whid)
            }
            if "delete_break" in request.form:
                pass 

            print(info)
            requests.put(Config.SARA_PORT + '/api/get_shift/{}'.format(whid),json=info)
            response = requests.get(Config.SARA_PORT + '/api/get_shift')
            WORKINGHOURS_TABLE = json.loads(response.text)
        elif 'delete_shift' in request.form:
            whid = request.form.get('delete_shift')
            
            requests.delete(Config.SARA_PORT + '/api/get_shift/{}'.format(whid))
            response = requests.get(Config.SARA_PORT + '/api/get_shift')
            WORKINGHOURS_TABLE = json.loads(response.text)
        elif 'add_abnormal' in request.form:
            logging.info("==========DEBUG:{}===========".format(request.form))

            config = {    
                "error_type": request.form.get('error-type'),
                "error_group": request.form.get('error-group'),
                "error_code": request.form.get('error-code') 
            }
            print(config)

            response = requests.post(Config.SARA_PORT + '/api/get_error_setting',json=config)
            response = requests.get(Config.SARA_PORT + '/api/get_error_setting')
            ERROR_TABLE = json.loads(response.text)
        elif 'delete_abnormal' in request.form:
            eid = request.form.get('delete_abnormal') 
            response = requests.delete(Config.SARA_PORT + '/api/get_error_setting/' + eid)
            response = requests.get(Config.SARA_PORT + '/api/get_error_setting')
            ERROR_TABLE = json.loads(response.text)


    return render_template('settings-dn.html',ERROR_TABLE=ERROR_TABLE, WORKINGHOURS_TABLE=WORKINGHOURS_TABLE, \
        WORKINGDAYS_TABLE=WORKINGDAYS_TABLE,CALENDAR_EVENT=CALENDAR_EVENT,SELECTED_CALENDAR=SELECTED_CALENDAR)

@blueprint.route('/create_resource',methods=['GET','POST'])
@login_required
def create_resource():
    # TODO: init 需要討論
    if request.method == "POST":
        resource_info = {
            "resource_name": request.form.get('resource_name'),
            "calendar_name": request.form.get('calendar_name'),
            "hours_name": request.form.get('hours_name'),
            "resource_type": request.form.get('resource_type'),
            "process": request.form.get('process')
        } 
        
# 製程管理 
@blueprint.route('/create_job',methods=['GET','POST'])
@login_required
def create_job():

    # TODO: 需要討論
    if request.method == "POST":
        multi_resource = request.form.get('multi_resource')
        job_info = {
            "job_name": request.form.get('job_name'),
            "multi_resource": multi_resource,
            "outsourcing": request.form.get('outsourcing')
        }
        

@blueprint.route('/create_project_job',methods=['GET','POST'])
@login_required
def create_project_job(prdid):
    
    info = {
        "req":["job_name"]
    }
    JOB_NAME = json.loads(requests.post(Config.SARA_PORT + '/api/get_job', json=info))
    # TODO: job_sequence? 

    if request.method == "POST":
        
        job_req = {
            "job_name": request.form.get('job_name'),
            "job_sequence": request.form.get('job_sequence'),
            "est_time": request.form.get('est_time'),
            "qty": request.form.get('qty'),
            "pre_product": request.form.get('pre_product')
        }
@blueprint.route('/create_project_material',methods=['GET','POST'])
@login_required
def create_project_material():
    if request.method == "POST":
        material_req = {
            "material_id": request.form.get('material_id'),
            "material_type": request.form.get('material_type'),
            "req_qty": int(request.form.get('req_qty')),
            "used": int(request.form.get('used_material_count'))
        }


# NOT URGENT
@blueprint.route('/add_process', methods=['GET','POST'])
@login_required
def add_job_to_resource():
    if request.method == "POST":
        job_info = {
            "job_name": request.form.get('job_name'),
            "skill_priority": request.form.get('skill_priority')
        }

@blueprint.route('/create_mfg_order', methods= ['GET','POST'])
@login_required
def create_mfg_order():
    if request.method == 'POST':
        project_info = {
            "project_name": request.form.get('project_name'),
            "lot": request.form.get('lot'),
            "prdid": request.form.get('prdid'),
            "qty": request.form.get('qty'),
            "due": request.form.get('due')
        }
        
@blueprint.route('/create_material', methods=['GET','POST'])
@login_required
def create_material():
    if request.method == 'POST':
        material_info = {
            "material_id": request.form.get('material_id'),
            "material_type": request.form.get('material_type'),
            "inventory": int(request.form.get('inventory')),
            "unit": request.form.get('unit'),
            "lead_time": request.form.get('lead_time')
        }

@blueprint.route('/create_order',methods=['GET','POST'])
@login_required
def _create_order():
    requests.get(Config.SARA_PORT + '/api/create_order')
    return redirect(url_for('home_blueprint.index'))

@blueprint.route('/get_notification', methods= ['GET'])
@login_required
def _get_notification():
    logging.info("ENTER GET NOTIFICATION")
    COLOR = None
    NOTIFICATION = json.loads(requests.get(Config.SARA_PORT + '/api/get_notification').text)
    if len(NOTIFICATION) > 0:
        COLOR = "text-warning" 
    # response = requests.get(Config.SARA_PORT + '/api/get_schedule_progress')
    # PROGRESS = json.loads(response.text)
    # if len(PROGRESS) != 0:
    #     event_id = len(NOTIFICATION)
        
    #     info = {
    #         "name": "排程",
    #         "event_type": PROGRESS["status"], 
    #         "start_time": PROGRESS["time"]
    #     }
        
    #     NOTIFICATION[event_id] = info
    #     if COLOR is None:
    #         if PROGRESS["status"] == "released":
    #             COLOR = "text-success"
    #         else:
    #             COLOR = "text-warning"
        
    
    return render_template("base.html", NOTIFICATION=NOTIFICATION, COLOR=COLOR)


@blueprint.route('/mes_update')
@login_required
def _update_mes():
    
    requests.get(Config.SARA_PORT + '/api/mes_update')
    
    return redirect(url_for('home_blueprint.work_center'))



@blueprint.route('/released')
@login_required
def _release_schedule():
    logging.info("=======DEBUG:ENTER RELEASE=======")
    requests.get(Config.SARA_PORT + '/api/update_db')
    
    return redirect(url_for('home_blueprint.index'))

@blueprint.route('/reschedule')
@login_required
def _reschedule():
    from app.tasks import planner_rescheduler
    logging.info("=======DEBUG:ENTER RESCHEDULE=======")
    
    # planner_rescheduler.delay()
    response = requests.post(Config.SARA_PORT + '/api/update_status_planner',timeout=300)
    info = json.loads(response.text)
    print("======",info,"======")
    
    if info["message"] == "inprogress":
        return redirect(url_for('home_blueprint.index'))
    else:
        return redirect(url_for('base_blueprint.gantt'))
    
@blueprint.route('/export_schedule/<option>', methods=['POST'])
@login_required
def _export_schedule(option):
    
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        data = request.form.get('data')
        import ast
        data = ast.literal_eval(data)
        logging.info("==========DEBUG:{}===========".format(type(data)))
        from io import StringIO
        import csv
        f = StringIO()

        w = csv.writer(f)

        w.writerow(["工單編號","料號","工序流程","製程名稱","排定資源","數量","預估工時(分鐘)","預計開始","預計結束","實際開始","實際結束","實際工時(分鐘)"])
        
        for jid in data:
            
            logging.info("{} {}".format(jid, data[jid]))
            w.writerow(
                [
                    data[jid]["project_name"],
                    data[jid]["product_name"],
                    data[jid]["job_sequence"],
                    data[jid]["job_name"],
                    ",".join(data[jid]["resource"]),
                    data[jid]["qty"],
                    data[jid]["est_time"],
                    data[jid]["plan_start_time"],
                    data[jid]["plan_end_time"],
                    data[jid]["real_start_time"],
                    data[jid]["real_end_time"],
                    data[jid]["time_spent"]
                ])
        csv =f.getvalue().encode("utf-8-sig")
        #filename = data[jid]["project_name"]
        #udata=option.decode("utf-8")
        filename=option.encode("ascii","ignore")
        time = get_current_time_string()      
    #csv = '1,2,3\n4,5,6\n'
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":"attachment; filename={}_{}.csv".format(filename,time)})

@blueprint.route('/export_resource_schedule_csv', methods=['POST'])
@login_required
def _export_resource_schedule_csv():
    
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        data = request.form.get('data')
        import ast
        data = ast.literal_eval(data)
        logging.info("==========DEBUG:{}===========".format(type(data)))
        from io import StringIO
        import csv
        f = StringIO()

        w = csv.writer(f)

        w.writerow(
            ["狀態","現在站點","工作名稱","工單/製令",\
             "製品料號","批號","途程","製程名稱","製程數量","類別",\
             "預計開始","預計結束","實際開始","實際結束"]
        )
        for jid in data:
            
            logging.info("{} {}".format(jid, data[jid]))
            w.writerow(
                [
                    data[jid]["status"],
                    data[jid]["current_loc"],
                    data[jid]["job_name"],
                    data[jid]["project_name"],
                    data[jid]["product_name"],
                    data[jid]["lot_nbr"],
                    data[jid]["route_seq"],
                    data[jid]["jlb_name"],
                    data[jid]["qty"],
                    data[jid]["project_type"],
                    data[jid]["plan_start_time"],
                    data[jid]["plan_end_time"],
                    data[jid]["real_start_time"],
                    data[jid]["real_end_time"]
                ])
        csv =f.getvalue().encode("utf-8-sig")
        #filename = data[jid]["project_name"]
        #udata=option.decode("utf-8")
        #filename=option.encode("ascii","ignore")
        time = get_current_time_string()   
    #csv = '1,2,3\n4,5,6\n'
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":"attachment; filename=resource_{}.csv".format(time)})

@blueprint.route('/export_outsourcing_csv', methods=['POST'])
@login_required
def _export_outsourcing_csv():
    
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        data = request.form.get('data')
        import ast
        data = ast.literal_eval(data)
        logging.info("==========DEBUG:{}===========".format(type(data)))
        from io import StringIO
        import csv
        f = StringIO()

        w = csv.writer(f)

        w.writerow(["工單編號","料號","製程名稱","預計發包","預計回應","實際發包","實際回廠"])
        
        for jid in data:
            
            logging.info("{} {}".format(jid, data[jid]))
            w.writerow(
                [
                    data[jid]["project_name"],
                    data[jid]["product_name"],
                    data[jid]["job_name"],
                    data[jid]["plan_start_time"],
                    data[jid]["plan_end_time"],
                    data[jid]["real_start_time"],
                    data[jid]["real_end_time"]
                ])
        csv =f.getvalue().encode("utf-8-sig")
        #filename = data[jid]["project_name"]
        #udata=option.decode("utf-8")
        #filename=option.encode("ascii","ignore")
        time = get_current_time_string()   
    #csv = '1,2,3\n4,5,6\n'
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":"attachment; filename=outsourcing_{}.csv".format(time)})

@blueprint.route('/export_resource_performance_csv/<option>', methods=['POST'])
@login_required
def _export_resource_performance_csv(option):
    
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        data = request.form.get('data')
        import ast
        data = ast.literal_eval(data)
        logging.info("==========DEBUG:{}===========".format(type(data)))
        from io import StringIO
        import csv
        f = StringIO()

        w = csv.writer(f)

        w.writerow(["機台名稱","類型","工作日期","負荷率","工作技能"])
        
        for count in data:
            
            logging.info("{} {}".format(count, data[count]))
            w.writerow(
                [
                    data[count]["Resource Name"],
                    data[count]["Resource Type"],
                    data[count]["Time"],
                    data[count]["Loading"],
                    data[count]["Resource Jobs"]
                ])
        csv =f.getvalue().encode("utf-8-sig")
        #filename = data[jid]["project_name"]
        #udata=option.decode("utf-8")
        #filename=option.encode("ascii","ignore")
        time = get_current_time_string()   
    #csv = '1,2,3\n4,5,6\n'
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":"attachment; filename=resource_performance_{}days_{}.csv".format(option,time)})
 
@blueprint.route('/export_job_performance_csv/<option>', methods=['POST'])
@login_required
def _export_job_performance_csv(option):
    
    if request.method == "POST":
        logging.info("==========DEBUG:{}===========".format(request.form))
        data = request.form.get('data')
        import ast
        data = ast.literal_eval(data)
        logging.info("==========DEBUG:{}===========".format(type(data)))
        from io import StringIO
        import csv
        f = StringIO()

        w = csv.writer(f)

        w.writerow(["工單","料號","製程名稱","工序流程","資源",\
                    "異常類型", "異常原因","異常發生時間",\
                    "預計開始時間","預計結束時間","實際開始時間","實際結束時間"])


        
        for count in data:
            if 'event_type' in data[count]:
                event_type = data[count]["event_type"]
                event_start_time = data[count]["event_start_time"]
            else:
                event_type =None
                event_start_time = None
                
            
            logging.info("{} {}".format(count, data[count]))
            w.writerow(
                [
                    data[count]["project_name"],
                    data[count]["product_name"],
                    data[count]["job_name"],
                    ", ".join(data[count]["resource"]),
                    data[count]["abnormal"],
                    event_type,
                    event_start_time,
                    data[count]["plan_start_time"],
                    data[count]["plan_end_time"],
                    data[count]["real_start_time"],
                    data[count]["real_end_time"]
                ])

        # ['工時異常','未按次序','異常回報']
        # for problem_type in data:
        #     if problem_type == "time_abnormal":
        #         w.writerow(['工時異常'])
        #         w.writerow(['工單','料號','製程名稱','預估工時','實際時長','實際開始時間'])
        #         for jid in data[problem_type]:
        #             w.writerow(
        #                 [
        #                     data[problem_type][jid]["project_name"],
        #                     data[problem_type][jid]["product_name"],
        #                     data[problem_type][jid]["job_name"],
        #                     data[problem_type][jid]["plan_time"],
        #                     data[problem_type][jid]["real_time"],
        #                     data[problem_type][jid]["start_time"]
        #                 ]
        #             )
        #     elif problem_type == "sequence_abnormal":
        #         w.writerow(['未按次序'])
        #         w.writerow(['工單','料號','製程名稱','流程','前站報工時間','當站報工時間'])
        #         for jid in data[problem_type]:
        #             w.writerow(
        #                 [
        #                     data[problem_type][jid]["project_name"],
        #                     data[problem_type][jid]["product_name"],
        #                     data[problem_type][jid]["job_name"],
        #                     data[problem_type][jid]["job_sequence"],
        #                     data[problem_type][jid]["prev_time"],
        #                     data[problem_type][jid]["start_time"]
        #                 ]
        #             )
        #     elif problem_type == "abnormal_state":
        #         w.writerow(['異常回報'])
        #         w.writerow(['工單','料號','製程名稱','資源','問題','發生時間'])
        #         for jid in data[problem_type]:
        #             w.writerow(
        #                     [
        #                         data[problem_type][jid]["project_name"],
        #                         data[problem_type][jid]["product_name"],
        #                         data[problem_type][jid]["job_name"],
        #                         data[problem_type][jid]["resource_name"],
        #                         data[problem_type][jid]["event_type"],
        #                         data[problem_type][jid]["start_time"]
        #                     ]
        #                 )

        csv =f.getvalue().encode("utf-8-sig")
        #filename = data[jid]["project_name"]
        #udata=option.decode("utf-8")
        #filename=option.encode("ascii","ignore")
        time = get_current_time_string()    
    #csv = '1,2,3\n4,5,6\n'
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":"attachment; filename=job_performance_{}days_{}.csv".format(option,time)})

@blueprint.route('/<template>')
def route_template(template):

    #if not current_user.is_authenticated:
    #    return redirect(url_for('base_blueprint.login'))

    try:

        # Flexible rendering 
        # Sample pattern: '/profile' AND '/profile.html' 
        if not template.endswith( '.html' ):
            template += '.html'

        return render_template(template)

    except TemplateNotFound:
        return render_template('page-404.html'), 404
    
    except:
        return render_template('page-500.html'), 500
