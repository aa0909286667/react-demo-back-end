import LocaleManager from '../../../lib/Core/localization/LocaleManager.js';
//<umd>
import LocaleHelper from '../../../lib/Core/localization/LocaleHelper.js';
import En from '../../../lib/Gantt/localization/En.js';
import SharedEn from './shared.locale.En.js';

const examplesEnLocale = LocaleHelper.mergeLocales(SharedEn, {

    extends : 'En',

    Baselines : {
        baseline           : 'base line',
        Complete           : 'Complete',
        'Delayed start by' : 'Delayed start by',
        Duration           : 'Duration',
        End                : 'End',
        'Overrun by'       : 'Overrun by',
        Start              : 'Start'
    },

    Indicators : {
        Indicators     : 'Indicators',
        lateDates      : 'Late start/end',
        constraintDate : 'Constraint'
    },

    StatusColumn : {
        Status : 'Status'
    },

    TaskTooltip : {
        'Scheduling Mode' : 'Scheduling Mode',
        Calendar          : 'Calendar',
        Critical          : 'Critical'
    }

});

LocaleHelper.publishLocale('En', En);
LocaleHelper.publishLocale('EnExamples', examplesEnLocale);

export default examplesEnLocale;
//</umd>

LocaleManager.extendLocale('En', examplesEnLocale);
