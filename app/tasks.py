from app import create_celery_app
from celery.schedules import crontab
from celery.utils.log import get_task_logger
import requests
import json
from config import Config
celery = create_celery_app()

@celery.task
def get_scheduling_result():
    print('=============== Auto Update Scheduling Result ============')
    # requests.get('http://sara:5555/api/mes_update')
    response = requests.get(Config.SARA_PORT + '/api/mes_update')
    print(json.loads(response.text))
    return None
    
@celery.task
def planner_rescheduler():
    
    # import requests
    # response = requests.post(Config.SARA_PORT + '/api/update_status_planner',timeout=300)
    pass
@celery.task
def celery_reselect_schedule_result(sid):
    from app.agents.pipelineAgent import ReselectScheduleResult
    ReselectScheduleResult(sid)

@celery.task
def celery_get_schedule_result():
    from app.agents.pipelineAgent import GetScheduleResult
    print("===== celery start get schedule ====")
    return GetScheduleResult()

@celery.task
def celery_get_mes_update():
    from app.agents.pipelineAgent import GetMESUpdate
    GetMESUpdate()
    
@celery.task
def mes_update_task():
    response = requests.post(Config.SARA_PORT + '/api/mes_update')
    return None
@celery.task
def celery_get_schedule_summary():
    from app.agents.scheduleAgent import get_schedule_summary
    return get_schedule_summary()
@celery.task
def expand_details():
    from app.agents.dataAgent import expand_job_detail
    expand_job_detail()
    print("=== expand complete ===")
    return None

@celery.task
def group_check_abnormal():
    from app.agents.dataAgent import group_check_job_abnormal
    group_check_job_abnormal()
    print("=== abnormal check complete ====")
    return None

@celery.task
def group_calculate_bottleneck():
    print("=== long_job_calculation ===")
    from app.agents import dataAgent
    dataAgent.DA_calculte_waiting_time()
    dataAgent.calculate_bottlenecks()

@celery.task
def group_calculate_working_hours():
    from app.agents import dataAgent
    dataAgent.calculate_working_hours()
    print("=== long_job_calculation complete ===")

@celery.task
def log(message):
    logger = get_task_logger(__name__)

    """Print some log messages"""
    logger.debug(message)
    logger.info(message)
    logger.warning(message)
    logger.error(message)
    logger.critical(message)

@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Auto update scheduling result every 2 hours
    sender.add_periodic_task(
        # crontab(minute=00, hour='*/1'),
        crontab(minute=00, hour='*/2'),
        #crontab(minute='*/10'),
        get_scheduling_result.s()
    )
    sender.add_periodic_task(
        # crontab(minute=00, hour='*/1'),
        crontab(minute='*/5'),
        mes_update_task.s()
    )
     