from numbers import Number
from collections import OrderedDict
from global_vars import RESULT

import logging
from logging.config import dictConfig
from config import *
from error_handler import PreprocessException

dictConfig(LOG_CONFIG)
logger = logging.getLogger('myLogger')

#============== utilities ==============
def second_level_completeness_check(db:dict, first_level_key:str, second_level_key:str, err_code:int):
    if second_level_key not in db[first_level_key].keys():
        msg = f'{first_level_key} does not have column "{second_level_key}".'
        raise PreprocessException(err_code, msg)


def make_new_col(my_dict, db_name):
    f = lambda x: choice(x) if isinstance(x, tuple) else x
    new_cols = VARS[db_name]['new_cols']
    new_cols_default = VARS[db_name]['new_cols_default']
    if new_cols:
        for i, col in enumerate(new_cols):
            my_dict[col] = f(new_cols_default[i])
    return


#============== function: processing order_db ==============
def pipe_order(db:dict, db_template:dict, project_key:str) -> dict:
    '''Process and store project part in db

    Args:
        db (dict): sourced data with json-like dictionary format.
        db_template (dict): The template of input db.
        project_key (str): The key for accessing project_db in db.

    Returns:
        A dict format of order db.
        e.g. {'PID1':{'order_id': None,
                      'order_date': '2020-10-03 00:00:00',
                      'project_name': 'MO1840002',
                      'project_type': '一般',
                      'due': '2020-10-10 00:00:00',
                      '_product': ('PRDID2',),
                      'customer_id': None,
                      'status': 'New',
                      'updated_on': '2020-12-03 11:47:35'}}
    '''
    from copy import deepcopy
    from utilities import take_first_to_int, str_to_datetime

    try:
        # print('Enter pipe_order')
        mydb = dict()
        source_order_db = db[project_key]
        cols = db_template[project_key]

        for oid in source_order_db.keys():
            mydb[oid] = dict()
            for col in cols:
                if col not in source_order_db[oid].keys():
                    continue

                val = source_order_db[oid][col]
                if isinstance(val, dict):
                    mydb[oid][col] = pipe(
                        val.items(),
                        lambda items: sorted((item for item in items), key=take_first_to_int),
                        lambda pairs: od(pairs)
                    )

                elif isinstance(val, list):
                    mydb[oid][col] = tuple(val)

                elif isinstance(val, str):
                    mydb[oid][col] = val

                else:
                    mydb[oid][col] = deepcopy(val)

        str_to_datetime(mydb, *VARS['order']['date_cols'])
        # print('pipe_order pass')
        return mydb

    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.debug(e)
        RESULT['error'].append(str(e))


def check_order(order_db:dict):
    '''Run through necessary check points of order_db

    Args:
        order_db (dict): The order db to be checked.

    Returns:
        None
    '''
    from numpy import array, count_nonzero
    try:
        # print('Enter check_order')
        for oid in order_db.keys():
            # CASE: order_date
            second_level_completeness_check(order_db, oid, 'order_date', 110)

            # CASE: due < order_date
            second_level_completeness_check(order_db, oid, 'due', 110)

            end = VARS['order']['due_date_col']
            start = VARS['order']['order_date_col']

            length = array(list(map(lambda x: (x[end] - x[start]).total_seconds(), order_db.values())))
            if count_nonzero(length <= 0) > 0:
                msg = f'You got at least one order {oid} that its due date is prior to the created date '
                raise PreprocessException(110, msg)

            # CASE: order_id
            second_level_completeness_check(order_db, oid, 'order_id', 110)

            # CASE: _product
            second_level_completeness_check(order_db, oid, '_product', 110)

            # CASE: customer_id
            second_level_completeness_check(order_db, oid, 'customer_id', 110)

            # CASE: status
            second_level_completeness_check(order_db, oid, 'status', 110)

            # CASE: updated_on
            second_level_completeness_check(order_db, oid, 'updated_on', 110)

        # print('check_order pass')

    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.error(e)
        RESULT['error'].append(str(e))


#============== function: processing product_db ==============
def pipe_product(db:dict, db_template:dict, product_key:str) -> OrderedDict:
    '''Process and store product part in db.

    Args:
        db (dict): sourced data with json-like dictionary format.
        db_template (dict): The template of input db.
        project_key (str): The key for accessing product_db in db.


    Returns:
        A ordered dict format of product db
        e.g. OrderedDict([('PRDID1':
                            {'_jobID_list': OrderedDict([('1', 'JID0'),
                                                         ('2', 'JID1'),
                                                         ('3', 'JID2'),
                                                         ('4', 'JID3'),
                                                         ('5', 'JID4')]),
                             '_process': ('PRCID0',
                                          'PRCID1',
                                          'PRCID1',
                                          'PRCID1',
                                          'PRCID1',
                                          'PRCID2'),
                             'product_name': 'PA00000001',
                             'project_id': 'PID1'})])
    '''
    from toolz import pipe
    from copy import deepcopy
    from utilities import take_first_to_int
    from collections import OrderedDict as od

    try:
        # print('Enter pipe_product')
        mydb = od()
        source_product_db = db[product_key]
        cols = db_template[product_key]

        # TODO: make sure "sorted(source_product_db.keys())" is actually sorted by number order in all cases
        for pid in sorted(source_product_db.keys()):
            mydb[pid] = dict()
            for col in cols:
                if col not in source_product_db[pid].keys():
                    continue

                val = source_product_db[pid][col]
                if isinstance(val, dict):
                    mydb[pid][col] = pipe(
                        val.items(),
                        lambda items: sorted((item for item in items), key=take_first_to_int),
                        lambda pairs: od(pairs)
                    )

                elif isinstance(val, list):
                    mydb[pid][col] = tuple(val)

                elif isinstance(val, str):
                    mydb[pid][col] = val

                elif isinstance(val, Number):
                    mydb[pid][col] = val

                else:
                    mydb[pid][col] = deepcopy(val)

        # print('pipe_product pass')
        return mydb

    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.debug(e)
        RESULT['error'].append(str(e))


def check_product(product_db:dict):
    '''Run through necessary check points of product_db

    Args:
        product_db (dict): The product db to be checked.

    Returns:
        None
    '''
    from utilities import take_first_to_int

    try:
        # print('Enter check_product')
        for pid in product_db.keys():
            # CASE _jobID_list
            # 先skip
            # second_level_completeness_check(product_db, pid, '_jobID_list', 110)

            # s_real = tuple(product_db[pid]['_jobID_list'].keys())
            # s_ideal = tuple(str(i) for i in range(take_first_to_int(s_real), len(s_real)+1))
            # if s_ideal != s_real:
            #     raise PreprocessException(110, f'Product {pid}, job series numbers are not consecutive')

            # CASE _process
            second_level_completeness_check(product_db, pid, '_process', 110)

            # CASE product_name
            second_level_completeness_check(product_db, pid, 'product_name', 110)

            # CASE project_id
            second_level_completeness_check(product_db, pid, 'project_id', 110)

            # CASE mother
            has_mother = True if 'mother' in product_db[pid].keys() else False
            has_splitted = True if '_' in pid else False

            if has_mother:
                mom_id = product_db[pid]['mother']

            # split but not have mother tag
            if has_splitted and not has_mother:
                msg = 'Mother product has been splitted, but one of children products does not have tag "mother".'
                raise PreprocessException(110, msg)

            # split but not have correct mother tag
            if has_splitted and has_mother:
                if pid.split('_')[0] != mom_id:
                    msg = 'Mother product id of one of children products does not match its id.'
                    raise PreprocessException(110, msg)

            if has_mother and not has_splitted:
                if bool(mom_id):
                    msg = 'The product did not splitted but does have mother tag.'
                    raise PreprocessException(110, msg)

        # print('check_product pass')
    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.debug(e)
        RESULT['error'].append(str(e))


#============== function: processing process_db ==============
def pipe_process(db:dict, db_template:dict, process_key:str) -> OrderedDict:
    '''Process and store process part in db.

    Args:
        db (dict): sourced data with json-like dictionary format.
        db_template (dict): The template of input db.
        process_key (str): The key for accessing process_db in db.


    Returns:
        A ordered dict format of process_db
        e.g. OrderedDict([('PRCID0', {'process_name': '裁板', '_job': ('烘烤', '裁板')}),
                          ('PRCID1', {'process_name': '內層', '_job': ('顯影', '曝光', '壓膜', '內層前處理')})])
    '''
    from toolz import pipe
    from copy import deepcopy
    from utilities import take_first_to_int
    from collections import OrderedDict as od

    try:
        mydb = od()
        source_process_db = db[process_key]
        cols = db_template[process_key]

        # TODO: make sure "sorted(source_process_db.keys())" is actually sorted by number order in all cases
        for pid in sorted(source_process_db.keys()):
            mydb[pid] = dict()
            for col in cols:
                if col not in source_process_db[pid].keys():
                    continue

                val = source_process_db[pid][col]
                if isinstance(val, dict):
                    mydb[pid][col] = pipe(
                        val.items(),
                        lambda items: sorted((item for item in items), key=take_first_to_int),
                        lambda pairs: od(pairs)
                    )

                elif isinstance(val, list):
                    mydb[pid][col] = tuple(val)

                elif isinstance(val, str):
                    mydb[pid][col] = val

                elif isinstance(val, Number):
                    mydb[pid][col] = val

                else:
                    mydb[pid][col] = deepcopy(val)
            make_new_col(mydb[pid], 'process')
        return mydb

    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.debug(e)
        RESULT['error'].append(str(e))


def check_process(process_db:dict):
    '''Run through necessary check points of process_db

    Args:
        process_db (dict): The process db to be checked.

    Returns:
        None
    '''
    try:
        # print('Enter check_process')
        for pid in process_db.keys():
            # CASE _job
            second_level_completeness_check(process_db, pid, '_job', 110)
            # print('_job check pass')

            # CASE process_name
            second_level_completeness_check(process_db, pid, 'process_name', 110)
            # print('process_name check pass')

        # print('check_process pass')
    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.error(e)
        RESULT['error'].append(str(e))


#============== function: processing job_db ==============
def pipe_job(db:dict, db_template:dict, job_key:str) -> dict:
    '''Process and store job part in db.

    Args:
        db (dict): sourced data with json-like dictionary format.
        db_template (dict): The template of input db.
        job_key (str): The key for accessing job_db in db.


    Returns:
        A ordered dict format of job_db
        e.g. {'JID0': {
                    'product_id': 'PRDID0_000',
                    'job_name': '裁板',
                    'process_name': '裁板',
                    'qty': 200,
                    'job_sequence': 1,
                    'est_time': 15,
                    'stdHourQty': None,
                    '_material': OrderedDict(),
                    'pre_product': None,
                    'skill': 9,
                    "secondary_resource": {
                        "machine": [
                            "RCID102"
                        ]
                    },
                    "primary_resource": {
                        "operator": [
                            "RCID95",
                            "RCID99"
                        ]
                    },
                    "sourcing": "in-house"},
              'JID13_002': {
                    'product_id': 'PRDID0_002',
                    'job_name': '目檢作業',
                    'process_name': '成檢',
                    'qty': 160.0,
                    'job_sequence': 9,
                    'est_time': 135,
                    'stdHourQty': None,
                    '_material': OrderedDict(),
                    'pre_product': None,
                    'skill': 3,
                    "secondary_resource": {},
                    "primary_resource": {
                        "machine": [
                            "RCID7"
                        ]
                    },
                    "sourcing": "in-house",
                    'mother': 'JID13'}
             }
    '''
    # from toolz import pipe
    from copy import deepcopy
    from numbers import Number
    from collections import OrderedDict as od
    from utilities import take_first_to_int

    try:
        # print('Enter pipe_job')
        mydb = dict()
        source_job_db = db[job_key]
        cols = db_template[job_key]

        for jid in source_job_db.keys():
            mydb[jid] = dict()
            for col in cols:
                if col not in source_job_db[jid].keys():
                    continue

                val = source_job_db[jid][col]
                if isinstance(val, dict):
                    # mydb[jid][col] = pipe(
                    #     val.items(),
                    #     lambda items: sorted((item for item in items), key=take_first_to_int),
                    #     lambda pairs: od(pairs)
                    # )
                    mydb[jid][col] = deepcopy(val)

                elif isinstance(val, list):
                    mydb[jid][col] = tuple(val)

                elif isinstance(val, str):
                    mydb[jid][col] = val

                elif isinstance(val, Number):
                    mydb[jid][col] = val

                else:
                    mydb[jid][col] = deepcopy(val)
            make_new_col(mydb[jid], 'job')
        # print('pipe_job pass')
        return mydb

    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.debug(e)
        RESULT['error'].append(str(e))


def check_job(job_db:dict):
    '''Run through necessary check points of job_db

    Args:
        job_db (dict): The job db to be checked.

    Returns:
        None
    '''
    try:
        # print('Enter check_job')
        for jid in job_db.keys():
            # CASE product_id
            second_level_completeness_check(job_db, jid, 'product_id', 110)

            # CASE process_name
            second_level_completeness_check(job_db, jid, 'process_name', 110)

            # CASE job_name
            second_level_completeness_check(job_db, jid, 'job_name', 110)

            # CASE qty
            second_level_completeness_check(job_db, jid, 'qty', 110)

            # CASE job_sequence
            second_level_completeness_check(job_db, jid, 'job_sequence', 110)

            # CASE sourcing
            second_level_completeness_check(job_db, jid, 'sourcing', 110)

            # CASE est_time
            second_level_completeness_check(job_db, jid, 'est_time', 110)

            # CASE stdHourQty
            second_level_completeness_check(job_db, jid, 'stdHourQty', 110)

            # CASE pre_product
            second_level_completeness_check(job_db, jid, 'pre_product', 110)

            # CASE skill
            second_level_completeness_check(job_db, jid, 'skill', 110)

            # CASE primary_resource
            second_level_completeness_check(job_db, jid, 'primary_resource', 110)

            # CASE secondary_resource
            second_level_completeness_check(job_db, jid, 'secondary_resource', 110)

            # CASE mother
            has_mother = True if 'mother' in job_db[jid].keys() else False
            has_splitted = True if '_' in jid else False

            if has_mother:
                mom_id = job_db[jid]['mother']

            # split but not have mother tag
            if has_splitted and not has_mother:
                msg = 'Mother product has been splitted, but one of children products does not have tag "mother".'
                raise PreprocessException(110, msg)

            # split but not have correct mother tag
            if has_splitted and has_mother:
                if jid.split('_')[0] != mom_id:
                    msg = 'Mother product id of one of children products does not match its id.'
                    raise PreprocessException(110, msg)

            if has_mother and not has_splitted:
                if bool(mom_id):
                    msg = 'The product did not splitted but does have mother tag.'
                    raise PreprocessException(110, msg)

        # print('check_job pass')
    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.error(e)
        RESULT['error'].append(str(e))


#============== function: processing resource_db ==============
def pipe_resource(db:dict, db_template:dict, resource_key:str) -> dict:
    '''Process and store resource part in db.

    Args:
        db (dict): sourced data with json-like dictionary format.
        db_template (dict): The template of input db.
        procresource_keyess_key (str): The key for accessing resource_db in db.


    Returns:
        A dict format of resource_db
        e.g. {'RCID0': {'resource_name': 'M001',
                        'job_name': ('裁板', 烘烤),
                        'resource_type': 'machine',
                        'working_type': 'Discrete',
                        'operator_skill': 9,
                        'working_day': 'WDID0',
                        'working_hours': 'WHID0'}}
    '''
    from copy import deepcopy
    from utilities import take_first_to_int

    try:
        # print('Enter pipe_resource')
        mydb = dict()
        source_resource_db = db[resource_key]
        cols = db_template[resource_key]

        for rid in source_resource_db.keys():
            mydb[rid] = dict()
            for col in cols:
                if col not in source_resource_db[rid].keys():
                    continue
                val = source_resource_db[rid][col]
                if isinstance(val, dict):
                    mydb[rid][col] = pipe(
                        val.items(),
                        lambda items: sorted((item for item in items), key=take_first_to_int),
                        lambda pairs: od(pairs)
                    )
                elif isinstance(val, list):
                    mydb[rid][col] = tuple(val)
                elif isinstance(val, str):
                    mydb[rid][col] = val
                else:
                    mydb[rid][col] = deepcopy(val)
            make_new_col(mydb[rid], 'resource')
        # print('pipe_resource pass')
        return mydb

    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.debug(e)
        RESULT['error'].append(str(e))


# TODO: varify new cols
def check_resource(resource_db:dict):
    '''Run through necessary check points of resource_db

    Args:
        resource_db (dict): The resource db to be checked.

    Returns:
        None
    '''
    try:
        # print('Enter check_resource')
        for rid in resource_db.keys():
            # CASE resource_name
            second_level_completeness_check(resource_db, rid, 'resource_name', 110)

            # CASE job_name
            second_level_completeness_check(resource_db, rid, 'job_name', 110)

            # CASE resource_type
            second_level_completeness_check(resource_db, rid, 'resource_type', 110)

            # CASE virtual_resource
            second_level_completeness_check(resource_db, rid, 'virtual_resource', 110)

            # CASE working_type
            second_level_completeness_check(resource_db, rid, 'working_type', 110)

            # CASE operator_skill
            # if resource_db[rid]['resource_type'] == 'operator':
            #     second_level_completeness_check(resource_db, rid, 'operator_skill', 110)

            # CASE working_day
            second_level_completeness_check(resource_db, rid, 'working_day', 110)

            # CASE working_hours
            second_level_completeness_check(resource_db, rid, 'working_hours', 110)
        # print('check_resource pass')
    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.error(e)
        RESULT['error'].append(str(e))


#============== function: processing workingdays_db ==============
def pipe_workingdays(db:dict, db_template:dict, workingdays_key:str) -> dict:
    '''Process and store workingdays part in db.

    Args:
        db (dict): sourced data with json-like dictionary format.
        db_template (dict): The template of input db.
        workingdays_key (str): The key for accessing workingdays_db in db.

    Returns:
        A dict format of workingdays_db.
        e.g. {"WDID0": {"calendar_name": "CTE_calendar",
                        "working_days": ["2020-01-02 00:00:00",
                                         "2020-01-03 00:00:00",
                                         "2020-01-04 00:00:00",
                                         "2020-01-05 00:00:00",
                                         "2020-01-06 00:00:00"]}}
    '''
    from toolz import pipe
    from copy import deepcopy
    from utilities import take_first_to_int, strTuple_to_datetime

    try:
        mydb = dict()
        source_workingdays_db = db[workingdays_key]
        cols = db_template[workingdays_key]
        for cid in source_workingdays_db.keys():
            mydb[cid] = dict()
            for col in cols:
                if col not in source_workingdays_db[cid].keys():
                    continue

                val = source_workingdays_db[cid][col]
                if isinstance(val, dict):
                    mydb[cid][col] = pipe(
                        val.items(),
                        lambda items: sorted((item for item in items), key=take_first_to_int),
                        lambda pairs: od(pairs)
                    )

                elif isinstance(val, list):
                    mydb[cid][col] = tuple(val)

                elif isinstance(val, str):
                    mydb[cid][col] = val

                else:
                    mydb[cid][col] = deepcopy(val)

        strTuple_to_datetime(mydb, *VARS['workingdays']['date_cols'])
        return mydb

    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.debug(e)
        RESULT['error'].append(str(e))


def check_workingdays(workingdays_db:dict):
    '''Run through necessary check points of workingdays_db

    Args:
        workingdays_db (dict): The workingdays db to be checked.

    Returns:
        None
    '''
    try:
        # print('Enter check_workingdays')
        for cid in workingdays_db.keys():
            # CASE: calender_name
            second_level_completeness_check(workingdays_db, cid, 'calendar_name', 110)

            # CASE: working_days
            second_level_completeness_check(workingdays_db, cid, 'working_days', 110)
        # print('check_workingdays pass')
    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.error(e)
        RESULT['error'].append(str(e))


#============== function: processing workinghours_db ==============
def pipe_workinghours(db:dict, db_template:dict, workinghours_key:str) -> dict:
    '''Process and store workinghours part in df.
    e.g. {'WHID0': {'id': 1,
                    'index': 'WHID0',
                    'hours_name': '早班',
                    'working_hours': ['07:00-16:00'],
                    'break_time': ['12:00-13:00']},
          'WHID1': {'id': 2,
                    'index': 'WHID1',
                    'hours_name': '中班',
                    'working_hours': ['16:00-23:59'],
                    'break_time': ['19:00-19:30', '22:00-22:30']},
          'WHID2': {'id': 3,
                    'index': 'WHID2',
                    'hours_name': '正常班',
                    'working_hours': ['08:00-17:00'],
                    'break_time': ['13:00-14:00']}}

    Args:
        db (dict): sourced data with json-like dictionary format.
        db_template (dict): The template of input db.
        workinghours_key (str): The key for accessing workinghours_db in db.

    Returns:
        A dict format of workinghours_db.
        e.g. {'WHID0': {'id': 1,
                        'index': 'WHID0',
                        'hours_name': '早班',
                        'working_hours': [[(7,0), (16,0)]],
                        'break_time': [[(12,0), (13,0)]]},
              'WHID1': {'id': 2,
                        'index': 'WHID1',
                        'hours_name': '中班',
                        'working_hours': [[(16,0), (23,59)]],
                        'break_time': [[(19,0), (19,30)], [(22,0), (22,30)]]},
              'WHID2': {'id': 3,
                        'index': 'WHID2',
                        'hours_name': '正常班',
                        'working_hours': [[(8,0), (17,0)]],
                        'break_time': [[(13,0), (14,0)]]}}
    '''
    from toolz import pipe
    from copy import deepcopy
    from utilities import take_first_to_int, take_second_to_int

    try:
        # print('Enter pipe_workinghours')
        mydb = dict()
        source_workinghours_db = db[workinghours_key]
        cols = db_template[workinghours_key]

        for sid in source_workinghours_db.keys():
            mydb[sid] = dict()
            for col in cols:
                if col not in source_workinghours_db[sid].keys():
                    continue

                val = source_workinghours_db[sid][col]
                if isinstance(val, dict):
                    mydb[sid][col] = pipe(
                        val.items(),
                        lambda items: sorted((item for item in items), key=take_first_to_int),
                        lambda pairs: od(pairs)
                    )

                elif isinstance(val, list):
                    mydb[sid][col] = tuple(val)

                elif isinstance(val, str):
                    mydb[sid][col] = val

                else:
                    mydb[sid][col] = deepcopy(val)

            for time_col in VARS['workinghours']['time_cols']:
                # '07:00-16:00' -> [(7, 0), (16, 0)]
                intervals = list()
                for ts in mydb[sid][time_col]:
                    intervals.append(
                        pipe(
                            ts.split('-'),
                            lambda x: [ts.split(':') for ts in x],
                            lambda x: [(take_first_to_int(ts), take_second_to_int(ts)) for ts in x]
                    ))
                mydb[sid][time_col] = intervals
        return mydb

    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.debug(e)
        RESULT['error'].append(str(e))


def check_workinghours(workinghours_db:dict):
    '''Run through necessary check points of workinghours_db

    Args:
        workinghours_db (dict): The workinghours db to be checked.

    Returns:
        None
    '''
    try:
        # print('Enter check_workinghours')
        for sid in workinghours_db.keys():
            # CASE: hours_name
            second_level_completeness_check(workinghours_db, sid, 'hours_name', 110)

            # CASE: working_hours    start-time > end-time
            second_level_completeness_check(workinghours_db, sid, 'working_hours', 110)
        # print('check_workinghours pass')
    except PreprocessException as e:
        logger.error(e)
        RESULT['error'].append(str(e))

    except Exception as e:
        logger.error(e)
        RESULT['error'].append(str(e))


#============== function: processing material_db ==============
def pipe_material():
    pass


#============== function: executor function ==============
def run(db:dict, ver:str, project_key:str, process_key:str, job_key:str, resource_key:str, product_key:str, workingdays_key:str, workinghours_key:str) -> dict:
    '''Run a pipeline to execute all preprocess steps

    Args:
        df (dict): data with JSON format.
        ver (str): version of df format.
        project_key (str): key for project db. e.g. 'project_db'
        process_key (str): key for process db. e.g. 'process_db'
        job_key (str): key for job db. e.g. 'job_db'
        resource_key (str): key for resource db. e.g. 'resource_db'
        product_key (str): key for product db. e.g. 'product_db'
        workingdays_key (str): key for workingdays db. e.g. 'workingdays_db'
        workinghours_key (str):  key for workinghours db. e.g. 'workinghours_db'

    Return:
        A dict data same as input df but filled in schedule arrangement for
            resources and assigned resource for jobs
    '''
    import json
    with open('/app/compute/sara-planner/db_ver_config.json') as f:
        template = json.load(f)

    if ver not in template.keys():
        raise ValueError(f'db {ver} is not supported.')

    order_db = pipe_order(db, template[ver], project_key)
    check_order(order_db)

    product_db = pipe_product(db, template[ver], product_key)
    check_product(product_db)

    process_db = pipe_process(db, template[ver], process_key)
    check_process(process_db)

    job_db = pipe_job(db, template[ver], job_key)
    check_job(job_db)

    resource_db = pipe_resource(db, template[ver], resource_key)
    check_resource(resource_db)

    workingdays_db = pipe_workingdays(db, template[ver], workingdays_key)
    check_workingdays(workingdays_db)

    workinghours_db = pipe_workinghours(db, template[ver], workinghours_key)
    check_workinghours(workinghours_db)

    processed_db = {
        'order_db': order_db,
        'product_db': product_db,
        'process_db': process_db,
        'job_db': job_db,
        'resource_db': resource_db,
        'workingdays_db': workingdays_db,
        'workinghours_db': workinghours_db
    }

    return processed_db

