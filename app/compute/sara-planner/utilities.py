from collections.abc import Sequence
from numbers import Number
from datetime import datetime

import logging
from logging.config import dictConfig
from config import *

dictConfig(LOG_CONFIG)
logger = logging.getLogger('myLogger')

def take_first(elem:Sequence):
    return elem[0]

def take_second(elem:Sequence):
    return elem[1]

def take_nth(elem:Sequence, nth:int):
    return elem[nth]

def take_first_to_int(elem:list) -> int:
    return int(elem[0])

def take_second_to_int(elem:list) -> int:
    return int(elem[1])

def take_nth_to_int(elem:list, nth:int) -> int:
    return int(elem[nth])

def take_first_to_str(elem:list) -> str:
    return str(elem[0])

def take_second_to_str(elem:list) -> str:
    return str(elem[1])

def take_nth_to_str(elem:list, nth:int) -> str:
    return str(elem[nth])

def prepend(*elem, lt:'list'=None) -> list:
    lt = lt if lt else []
    for e in elem[::-1]:
        lt.insert(0, e)
    return lt


def is_empty_or_blank(msg:str):
    """ This function checks if given string is empty
     or contain only spaces.

    Args:
        msg (str): The message to be parsed.

    Returns:
        A Bool for replying the message is empty or not.
    """
    import re.search as sr
    if sr("^\s*$", msg) is None:
        return False
    else:
        return True


def str_to_datetime(db, *fields):
    '''Convert value in datetime field with str type in
    JSON format db into actual datetime type.

    Args:
        db (dict): JSON-like db to be parsed internal str-type date fields.
        *fields: A list of date/datetime fields to be parsed.

    Returns:
        None
    '''
    from dateutil.parser import parse

    for key in db.keys():
        for field in fields:
            db[key][field] = parse(db[key][field])


def strList_to_datetime(db:dict, *fields):
    '''Convert a list of values in datetime field with str type in
    JSON format db into actual datetime type.

    Args:
        db (dict): JSON-like db to be parsed internal str-type date fields.
        *fields: A list of date/datetime fields to be parsed.

    Returns:
        None
    '''
    from dateutil.parser import parse

    for key in db.keys():
        for field in fields:
            db[key][field] = [parse(val) for val in db[key][field]]


def strTuple_to_datetime(db:dict, *fields):
    '''Convert a tuple of values in datetime field with str type in
    JSON format db into actual datetime type.

    Args:
        db (dict): JSON-like db to be parsed internal str-type date fields.
        *fields: A list of date/datetime fields to be parsed.

    Returns:
        None
    '''
    from dateutil.parser import parse

    for key in db.keys():
        for field in fields:
            db[key][field] = tuple(parse(val) for val in db[key][field])


def remove_key(target_dict:dict, unwanted:list):
    for unwanted_key in unwanted:
        del target_dict[unwanted_key]
    return


def get_min(job_db:dict, job_id_list:Sequence, attr:str) -> Number:
    '''Select min attr value from a list of attribute value of jobs.

    Args:
        job_db:
        job_id_list: job IDs of the selected jobs
        attr:

    Returns:
        A numerical number which is the minimum attribute value of selected jobs.
    '''

    vals = [job_db[jid][attr] for jid in job_id_list]
    if None in vals:
        raise DispatchingException(310, f'There exits job(s) with None {attr}.')
    else:
        return min(vals)


def get_max(job_db:dict, job_id_list:Sequence, attr:str) -> Number:
    '''Select max attr value from a list of attribute value of jobs.

    Args:
        job_db:
        job_id_list: job IDs of the selected jobs
        attr:

    Returns:
        A numerical number which is the maximum attribute value of selected jobs.
    '''

    vals = [job_db[jid][attr] for jid in job_id_list]
    if None in vals:
        raise DispatchingException(310, f'There exits job(s) with None {attr}.')
    else:
        return max(vals)


def get_root(leaf:str, tree:dict):
    if tree[leaf]:
        return take_first(tree[leaf])
    else:
        return None


def thread_root_series(leaf:str, tree:dict):
    rootList = list()

    try:
        while get_root(leaf, tree):
            root = get_root(leaf, tree)
            rootList.append(root)
            leaf = root

        return rootList

    except Exception as e:
        print(e)

# TODO: change name to root_profiler()
def compose_root_features(root_seris:list, leaf:str, leaf_bank:dict) -> dict:
    '''
    return dict with features: head(start_time) and tail(due_time)
    '''
    from collections import OrderedDict
    root_bank = OrderedDict()
    head = leaf_bank[leaf]['start_time']

    for root in root_seris:
        root_dict = dict()
        root_dict['tail'] = head
        root_dict['head'] = head - leaf_bank[root]['est_time']
        root_bank[root] = root_dict
        head = root_dict['head']
    return root_bank


def calc_time_gap(start, end, granularity:str):
    from datetime import datetime

    try:
        if not isinstance(start, datetime):
            msg = '"start" must datetime type.'
            raise ValueError(msg)

        if not isinstance(end, datetime):
            msg = '"end" must be datetime type.'
            raise ValueError(msg)

        delta = (end - start).total_seconds()

        func = {
            'second': lambda x: x,
            'seconds': lambda x: x,
            'sec': lambda x: x,
            'minute': lambda x: x/60.0,
            'minutes': lambda x: x/60.0,
            'min': lambda x: x/60.0,
            'hour': lambda x: x/60/60,
            'hours': lambda x: x/60/60,
            'hr': lambda x: x/60/60
        }.get(granularity, lambda: 'Invalid')

        delta = round(func(delta))

        if delta == 'Invalid':
            msg = 'Invalid argument for "granularity".'
            raise ValueError(msg)
        else:
            return delta

    except Exception as e:
        logger.error(e)


def calc_job_due(order_db:dict, product_db:dict, job_db:dict, start_time:datetime):
    import itertools
    from toolz import pipe
    # from workflow import time_gap
    # from utility.timekit import time_gap

    for product_id, product in product_db.items():
        order_due = pipe(
            product_db[product_id]['project_id'],
            lambda x: order_db[x]['due']
        )

        order_date = pipe(
            product_db[product_id]['project_id'],
            lambda x: order_db[x]['order_date']
        )

        jobs_of_product = pipe(
            product['_jobID_list'].values(),
            lambda x: itertools.chain.from_iterable(x),
            lambda x: list(x)
        )

        due = calc_time_gap(start_time, order_due, VARS['time_granularity'])

        for job_id in jobs_of_product[::-1]:
            job_db[job_id]['due'] = due # number
            job_db[job_id]['order_date'] = order_date # datetime
            job_db[job_id]['order_due'] = order_due # datetime
            due -= job_db[job_id]['est_time']

            # update: 2021/2/19
            if job_db[job_id]['due'] < 0:
                msgWarn = f'due time of job {job_id} is prior to current time.'
                msgDebug = f'due of job {job_id} < 0.'
                logger.warning(msgWarn)
                logger.debug(msgDebug)
    return

# update: 2021/2/17
def event_deps(job_id:str, job_db:dict, event_db:dict):
    job_sequence = job_db[job_id]['job_sequence']
    product_id = job_db[job_id]['product_id']
    jobs_in_same_product = list()

    for event in event_db.keys():
        if not event_db[event]['is_job']:
            continue

        event_job_id = event_db[event]['job_id']
        event_job_prod = job_db[event_job_id]['product_id']
        event_job_sequence = job_db[event_job_id]['job_sequence']
        if event_job_prod == product_id and event_job_sequence < job_sequence:
            jobs_in_same_product.append((event_job_id, event_job_sequence))

    if not jobs_in_same_product:
        return 0
    else:
        # sort by job sequence from big to small
        jobs_in_same_product = sorted(jobs_in_same_product, key=lambda x: x[1], reverse=False)
        dependency_id = jobs_in_same_product.pop()[0]
        return job_db[dependency_id]['end_time']


# update: 2021/2/19
def find_deps_in_event(job_id:str, job_db:dict, event_db:dict) -> dict:
    job_sequence = job_db[job_id]['job_sequence']
    product_id = job_db[job_id]['product_id']
    jobs_in_same_product = dict()

    for event in event_db.keys():
        if not event_db[event]['is_job']:
            continue

        event_job_id = event_db[event]['job_id']
        event_job_prod = job_db[event_job_id]['product_id']
        event_job_sequence = job_db[event_job_id]['job_sequence']
        if event_job_prod == product_id and event_job_sequence < job_sequence:
            jobs_in_same_product[event_job_id] = event_job_sequence

    return jobs_in_same_product


# update: 2021/2/19
def is_all_positive(job_db:dict, job_id_list:Sequence, attr:str, default_val:'Number'=1) -> bool:
    '''confirm if all of jobs in job_id_list should be with positive attr value (> 0).

    Args:
        job_db:
        job_id_list: job IDs of the selected jobs
        attr:
        default_val: default value if attr value is None.

    Returns:
        A bool value confirming whether or not all of jobs in job_id_list has positive attr value.
    '''
    from numpy import array

    arr = array([job_db[jid][attr] for jid in job_id_list])
    isNone = [i for i, val in enumerate(arr) if val is None]
    arr[isNone] = default_val
    arr = arr > 0
    if False in arr:
        return False
    else:
        return True


def fill_missing_days(workingdays_db:dict, WDID:str) -> list:
    # import numpy as np
    from copy import deepcopy

    wdays = list()
    wdays_length = len(workingdays_db[WDID]['working_days'])
    for i, j in enumerate(range(1, wdays_length)):
        td = workingdays_db[WDID]['working_days'][j] - workingdays_db[WDID]['working_days'][i]
        if td.days == 1:
            new_day = [workingdays_db[WDID]['working_days'][i]]
        elif td.days > 1:
            new_day = [None]*td.days
            new_day[0] = workingdays_db[WDID]['working_days'][i]
        else:
            raise ValueError('there exists two or more same days consecutive in line')

        wdays.extend(new_day)

    wdays.append(workingdays_db[WDID]['working_days'][-1])
    return wdays


def multiple_slicing(x:list, slices:list, stored:list) -> list:
    from itertools import chain

    if not x:
        raise TypeError("'NoneType' object 'x' is not subscriptable")

    start = list(chain(*stored))[-1] + 1 if stored else 0
    if slices:
        end = slices.pop(0)
        if (end - start) == 0:
            stored.append([x[end]])
        else:
            stored.append(x[start:end])
            stored.append([x[end]])

        return multiple_slicing(x=x, slices=slices, stored=stored)

    else:
        if start < len(x):
            stored.append(x[start:])

        return stored
