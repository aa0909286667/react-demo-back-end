from datetime import datetime, timedelta, date
from ordered_set import OrderedSet
from collections import OrderedDict, deque

from global_vars import RESULT, EVALUATION_TABLE
from data_structure import LinkedList
from utilities import remove_key, get_max, find_deps_in_event, is_all_positive
from workflow import init_resource, event_registration
from workflow import dependency_check, register_resource, select_best_resource

import logging
from logging.config import dictConfig
from config import *
from error_handler import DispatchingException

dictConfig(LOG_CONFIG)
logger = logging.getLogger('myLogger')

#============== utilities ==============
# def get_set(target_db:dict, key:str) -> OrderedSet:
#     from ordered_set import OrderedSet
#     mySet = OrderedSet([x[key] for x in target_db.values()])
#     return mySet


# def get_code_map(array:list) -> dict:
#     code = range(len(array))
#     code_map = dict(zip(array, code))
#     return code_map


# TODO: remove printing
def run(time_base:datetime, event_db:dict, job_seq:dict, job_db:dict, resource_db:dict, workingdays_db:dict, workinghours_db:dict):
    from termcolor import colored
    #============== resource initialisation ==============
    print(colored('===== ENTER RESOURCE INITIALISATION =====', 'red'))
    t = datetime.now()
    init_resource(resource_db, workingdays_db, workinghours_db, time_base)
    print(datetime.now()-t)
    print(colored('===== EXIT FROM RESOURCE INITIALISATION =====', 'red'))

    #============== event registration ==============
    print(colored('===== ENTER EVENT REGISTRATION =====', 'red'))
    t = datetime.now()
    event_registration(event_db         = event_db,
                       resource_db      = resource_db,
                       job_db           = job_db,
                       time_base        = t,
                       job_seq          = job_seq,
                       performance_dict = EVALUATION_TABLE,
                       error_handle     = RESULT['error'])
    print(datetime.now()-t)
    print(colored('===== EXIT FROM EVENT REGISTRATION =====', 'red'))

    #============== re-scheduling ==============
    print(colored('===== ENTER SCHEDULING =====', 'red'))
    t = datetime.now()
    prev_job = True
    while prev_job:
        for job_id, dependencies_list in job_seq.items():
            try:
                est_time = job_db[job_id]['est_time']
                primary = {*job_db[job_id]['primary_resource']}.pop()
                secondary = (*job_db[job_id]['secondary_resource'],)
                is_multi = True if job_db[job_id]['secondary_resource'] else False
                due = job_db[job_id]['due'] if job_db[job_id]['due'] > 0 else None
                candidate_resources = {**job_db[job_id]['primary_resource'], **job_db[job_id]['secondary_resource']}

                if job_db[job_id]['is_finished']:
                    continue

                if not dependency_check(job_db, dependencies_list):
                    prev_job = False
                    continue
                if not dependencies_list:
                    deps = find_deps_in_event(job_id, job_db, event_db)
                    waiting = 0 if not deps else job_db[max(deps, key=deps.get)]['end_time']
                else:
                    waiting = get_max(job_db, dependencies_list, 'end_time')
                    due = due if is_all_positive(job_db, dependencies_list, 'due') else None

                updated_resources = select_best_resource(job_id=job_id,
                                                         est_time=est_time,
                                                         waiting=waiting,
                                                         due=due,
                                                         is_multi=is_multi,
                                                         primary=primary,
                                                         secondary=secondary,
                                                         candidate_resources=candidate_resources,
                                                         resource_db=resource_db)

                for update in updated_resources:
                    EVALUATION_TABLE[update['resource_id']][job_id] = update['loading_ratio']
                    remove_key(update, ['selectedNodes_indices', 'loading_ratio'])
                    register_resource(job_id=job_id, job_db=job_db, resource_db=resource_db, **update)

            except DispatchingException as e:
                logger.error(e)
                RESULT['error'].append(str(e))

            except Exception as e:
                logger.error(e)
                RESULT['error'].append(str(e))

        if job_db[job_id]['is_finished']:
            break
    print(datetime.now()-t)
    print(colored('===== EXIT FROM SCHEDULING =====', 'red'))
