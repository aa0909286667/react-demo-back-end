from numbers import Number
from datetime import datetime
from collections.abc import Sequence
from data_structure import LinkedList
from global_vars import RESULT

import logging
from logging.config import dictConfig
from config import *
from error_handler import DispatchingException

dictConfig(LOG_CONFIG)
logger = logging.getLogger('myLogger')

def binding(resource_db:dict, resource_id:str, key:int):
    '''Merge consecutive nodes with identical values by key

    Args:
        resource_db:
        resource_id:
        key: Index of target element in node data list.

    Returns:
        None
    '''
    from numpy import array

    queue = resource_db[resource_id]['queue']
    vals = set([node.get_data()[key] for node in queue])
    for v in vals:
        nodes_index = sorted([node.get_index() for node in queue if node.get_data()[key] == v], reverse=True)
        neighbors = array(nodes_index[:-1]) - array(nodes_index[1:])
        for i, neighbor in enumerate(neighbors):
            if neighbor == 1:
                queue.merge_nodes(start_index=nodes_index[i], forward=False)
    return {resource_id: queue}


class ResourceBinder:
    def __init__(self, resource_db:dict):
        self.db = resource_db

    def __call__(self, resource_id:str, key:str):
        '''Merge consecutive nodes with identical values by key

        Args:
            resource_id:
            key: Index of target element in node data list.

        Returns:
            None
        '''
        from numpy import array

        queue = self.db[resource_id]['queue']
        vals = set([node.get_data()[key] for node in queue])
        for v in vals:
            nodes_index = sorted([node.get_index() for node in queue if node.get_data()[key] == v], reverse=True)
            neighbors = array(nodes_index[:-1]) - array(nodes_index[1:])
            for i, neighbor in enumerate(neighbors):
                if neighbor == 1:
                    queue.merge_nodes(start_index=nodes_index[i], forward=False)
        return {resource_id: queue}


class Binder:
    def __init__(self, resource_db:dict):
        self.db = resource_db

    def resource_binding(self):
        return ResourceBinder(self.db)


def init_resource(resource_db:dict, workingdays_db:dict, workinghours_db:dict, start_time:datetime):
    '''Initialise resource_db with prerequisite conditions or states.'''
    from datetime import datetime
    # from multiprocessing import Pool, cpu_count
    from utility.timekit import init_pattern, shift
    from utilities import take_first, fill_missing_days

    # import sys
    # sys.setrecursionlimit(1000000)

    # if cpu_count() == 1:
    #     core_num = cpu_count()
    # else:
    #     fixed = 0 # CHANGE IT IF NEEDED
    #     core_num = cpu_count() - fixed

    if not isinstance(start_time, datetime):
        raise ValueError('Parameter "start_time" should be passed by a datetime type value')

    day_start = datetime(*[start_time.year, start_time.month, start_time.day])

    for RCID, profile in resource_db.items():
        # print(f'RCID: {RCID}\n profile: {profile}')
        # if profile['resource_type'] == 'tooling':
        #     continue
        calendar = fill_missing_days(workingdays_db, profile['working_day'])
        rotation = workinghours_db[profile['working_hours']]
        workhour = take_first(rotation['working_hours'])
        breaktime = rotation['break_time']

        if day_start not in calendar:
            n = 1
            virtual_start = day_start + timedelta(days=n)
            day_count = 1
            while True:
                if virtual_start in calendar:
                    break
                else:
                    virtual_start = virtual_start + timedelta(days=n)
                    day_count += 1

            wdays = calendar[calendar.index(virtual_start)-day_count:]
        else:
            wdays = calendar[calendar.index(day_start):]

        wdays_length = len(wdays)

        resCycles = init_pattern(time_base=start_time,
                                 working_days=wdays,
                                 working_hours=workhour,
                                 break_time=breaktime,
                                 span_size=wdays_length,
                                 gran=VARS['time_granularity'])

        shift(resCycles, start_time)
        resource_db[RCID]['queue'] = resCycles

    # bind = Binder(resource_db).resource_binding() # by class method

    # with Pool(core_num) as p:
    #     # result = p.starmap(binding, [(resource_db, rid, 0) for rid in resource_db.keys()]) # by defined function 'binding'
    #     result = p.starmap(bind, [(rid, 0) for rid in resource_db.keys()]) # class method
    #     for resource_queue in result:
    #         key = {*resource_queue}.pop()
    #         resource_db[key]['queue'] = resource_queue[key]


def calc_day_in_total(time:datetime, unit:'str'='minute') -> int:
    '''Calculate how many time(minutes or seconds or other units) has passed since the beginning of the target day.

    Args:
        time: The target daytime.
        unit: Which time granularity to be computed.

    Returns:
        A integer number which represents how many time has been passed since the beginning the target day.

    '''
    from datetime import datetime
    # from utility.timekit import time_gap
    from utilities import calc_time_gap

    units = [time.year, time.month, time.day, time.hour, time.minute]

    unit_len = {
        'minute': 4,
        'minutes': 4,
        'min': 4,
        'mins': 4,

        'hour': 3,
        'hours': 3,
        'hr': 3,
        'hrs': 3,

        'day': 2,
        'days': 2,

        'month': 1,
        'months': 1
    }.get('hour', lambda: 'Invalid')

    left = units[0: unit_len]
    start = datetime(*left)

    return calc_time_gap(start, time, granularity=unit)


def is_forward_gap(start:datetime, end:datetime) -> bool:
    '''Check whether the gap between start and end is forward direction.

    Args:
        start:
        end:

    Returns:
        A boolean value indicating the gap is forward or not.
    '''
    return (end - start).total_seconds() > 0


def calc_forward_gap(start:datetime, end:datetime, granularity:'str'='minute', reset_end:'bool'=False, message:'str'='') -> int:
    '''Calculate the range of the gap between start and end.

    Args:
        start:
        end:
        granularity:
        reset_end:
        message:

    Returns:
        An integer number which represents the range of the gap.
    '''
    # from utility.timekit import time_gap
    from utilities import calc_time_gap

    if is_forward_gap(start, end):
        return calc_time_gap(start, end, granularity)
    else:
        if reset_end:
            return 0
        else:
            raise DispatchingException(310, message)


def register_event(resource_db:dict, resource_id:str, workerNodes_idx:list, startNode_idx:int, waitTime_to_start:Number, endNode_idx:int, workTime_to_end:Number, start_time:Number, end_time:Number, is_job:int, time_switch:int):
    '''Arrange event in corresponding resource.

    Args:
        resource_db:
        resource_id:
        workerNodes_idx:
        startNode_idx:
        waitTime_to_start:
        endNode_idx:
        workTime_to_end:
        start_time:
        end_time:
        is_job:
        time_switch:

    Returns:
        None
    '''
    queue = resource_db[resource_id]['queue']

    if startNode_idx == endNode_idx:
        thisNode_idx = startNode_idx
        if queue.get_node(thisNode_idx).get_log() >= 1:
            raise DispatchingException(310, f'resource {resource_id} has conflict events.')

        if waitTime_to_start == 0:
            if workTime_to_end != queue.get_node(thisNode_idx).get_data()[-1]:
                # only 1 segment, partially occupy former part
                queue.divide_node(thisNode_idx, workTime_to_end)

        else:
            if workTime_to_end == queue.get_node(thisNode_idx).get_data()[-1]:
                # only 1 segment, partially occupy latter part
                queue.divide_node(thisNode_idx, waitTime_to_start)
                thisNode_idx += 1

            else:
                # only 1 segment, partition twice and occupy the middle part
                queue.divide_node(thisNode_idx, workTime_to_end)
                queue.divide_node(thisNode_idx, waitTime_to_start)
                thisNode_idx += 1

        queue.modify_node_available_state(thisNode_idx, time_switch)
        queue.modify_node_job_state(thisNode_idx, is_job)
        queue.get_node(thisNode_idx).update()

    else:
        # middle nodes
        for thisNode_idx in workerNodes_idx[1: -1]:
            if queue.get_node(thisNode_idx).get_log() >= 1:
                raise DispatchingException(310, f'resource {resource_id} has conflict events.')

            queue.modify_node_available_state(thisNode_idx, time_switch)
            queue.modify_node_job_state(thisNode_idx, is_job)
            queue.get_node(thisNode_idx).update()

        # ending node
        if queue.get_node(endNode_idx).get_log() >= 1:
            raise DispatchingException(310, f'resource {resource_id} has conflict events.')

        if queue.get_node(endNode_idx).get_data()[-1] != workTime_to_end:
            queue.divide_node(endNode_idx, workTime_to_end)

        queue.modify_node_available_state(endNode_idx, time_switch)
        queue.modify_node_job_state(endNode_idx, is_job)
        queue.get_node(endNode_idx).update()

        # starter worker
        if queue.get_node(startNode_idx).get_log() >= 1:
            raise DispatchingException(310, f'resource {resource_id} has conflict events.')

        if waitTime_to_start != 0:
            queue.divide_node(startNode_idx, waitTime_to_start)
            startNode_idx += 1

        queue.modify_node_available_state(startNode_idx, time_switch)
        queue.modify_node_job_state(startNode_idx, is_job)
        queue.get_node(startNode_idx).update()


# TODO: deal with warning
def event_registration(event_db:dict, resource_db:dict, job_db:dict, time_base:datetime, job_seq:dict, performance_dict:dict, error_handle:list):
    '''Arrange necessary events before dispatching jobs.

    Args:
        event_db:
        resource_db:
        time_base:
        performance_dict:
        error_handle:

    Returns:
        None
    '''
    from dateutil.parser import parse
    from toolz import pipe
    from operator import add
    from itertools import accumulate
    from numpy import array
    import sys

    for event in event_db.values():
        try:
            start_time_str = event['start_time']
            end_time_str = event['end_time']
            queue = resource_db[event['resource_id']]['queue']
            names = ['workerNodes_idx', 'startNode_idx', 'waitTime_to_start', 'endNode_idx', 'workTime_to_end', 'start_time', 'end_time']

            if not event['is_job']:
                start_time = 0 if start_time_str is None else calc_forward_gap(time_base, parse(start_time_str), 'min', True)

                if end_time_str is None:
                    end_time = sum([node[-1] for node in queue.elems])
                else:
                    msg = 'end_time is prior to current time'
                    end_time = calc_forward_gap(time_base, parse(end_time_str), 'min', False, msg)

                startNode_idx = queue.get_node_by_accumulated_to(start_time).get_index()
                endNode_idx = queue.get_node_by_accumulated_to(end_time).get_index()

                workerNodes_idx = list(range(startNode_idx, endNode_idx+1))

                nodes = [node for node in queue]

                waitTime_to_start = pipe(
                    sum([x.get_data()[-1] for x in nodes[:startNode_idx]]),
                    lambda x: start_time - x
                )

                workTime_to_end = pipe(
                    sum([x.get_data()[-1] for x in nodes[:endNode_idx]]),
                    lambda x: end_time - x
                )

                eventList = [workerNodes_idx, startNode_idx, waitTime_to_start, endNode_idx, workTime_to_end, start_time, end_time]
                eventDict = dict(zip(names, eventList))

                register_event(resource_db=resource_db, resource_id=event['resource_id'], is_job=event['is_job'], time_switch=event['time_switch'], **eventDict)

            else:
                if end_time_str is None:
                    msg = f'There exits a job={event["job_id"]} with unlimited end time.'
                    raise ValueError(310, msg)
                else:
                    msg = 'end_time is prior to current time'
                    end_time = calc_forward_gap(start=time_base, end=parse(end_time_str), granularity='min', reset_end=False, message=msg)

                if start_time_str is None:
                    start_time = 0
                else:
                    start_time = calc_forward_gap(time_base, parse(start_time_str), 'min', True)

                workerNodes = [x for x in queue if x.get_data()[0] != 0]
                startNode_idx = queue.get_node_by_accumulated_to(start_time).get_index()
                endNode_idx = queue.get_node_by_accumulated_to(end_time).get_index()

                nodes = [node for node in queue]

                if start_time == 0:
                    waitTime_to_start = None
                    # if start with unavailable periods, skip to the first available period
                    while queue.get_node(startNode_idx).get_data()[0] == 0:
                        startNode_idx += 1

                    workTime_to_end = pipe(
                        sum([x.get_data()[-1] for x in nodes[:endNode_idx]]),
                        lambda x: end_time - x
                    )

                    # calculate break time and working time for performance
                    breaktime = sum([x.get_data()[-1] for x in nodes[:endNode_idx] if x.get_data()[0] == 0])

                    # find dependencies
                    event_dependency_list = None
                else:
                    # TODO: 自動擴展可用時間if insufficient working days and hours
                    queue.unify_available_state_between(start_time, end_time, new_state=1)

                    waitTime_to_start = None
                    workTime_to_end = queue.get_size(endNode_idx)
                    startNode_idx += 1

                    # find dependencies
                    event_dependency_list = thread_root_series(event['job_id'], job_seq)

                workerNodes_idx = list()
                workerNodes = [_ for _ in queue if _.get_data()[0] != 0]

                for node in workerNodes:
                    if node.get_index() in list(range(startNode_idx, endNode_idx+1)):
                        workerNodes_idx.append(node.get_index())

                eventList = [workerNodes_idx, startNode_idx, waitTime_to_start, endNode_idx, workTime_to_end, start_time, end_time]
                eventDict = dict(zip(names, eventList))
                register_resource(job_id      = event['job_id'],
                                  job_db      = job_db,
                                  resource_id = event['resource_id'],
                                  resource_db = resource_db,
                                  **eventDict)
                if start_time == 0:
                    performance_dict[event['resource_id']][event['job_id']] = 1 - breaktime/end_time
                else:
                    performance_dict[event['resource_id']][event['job_id']] = 1.0

                # If dependencies are not empty, dispatching dependencies & removing dependencies from job_seq
                if event_dependency_list:
                    root_bank = compose_root_features(root_seris=event_dependency_list,
                                                      leaf=event['job_id'],
                                                      leaf_bank=job_db)

                    # update: 2021/2/20
                    is_positive_start = array([_['head'] for _ in root_bank.values()]) > 0
                    if False in is_positive_start:
                        raise DispatchingException(310, f"Dependenies of event job {event['job_id']} can't start at negative start_time.")

                    dependencies_dict = dict()
                    for root in root_bank:
                        traits = dict()

                        traits['waiting'] = root_bank[root]['head']
                        traits['due'] = root_bank[root]['tail']
                        traits['est_time'] = job_db[root]['est_time']
                        traits['is_multi'] = True if job_db[root]['secondary_resource'] else False
                        traits['primary'] = {*job_db[root]['primary_resource']}.pop()
                        traits['secondary'] = (*job_db[root]['secondary_resource'],)
                        traits['candidate_resources'] = {**job_db[root]['primary_resource'], **job_db[root]['secondary_resource']}

                        dependencies_dict[root] = traits

                    for jobID in dependencies_list:
                        dep_res = select_best_resource(job_id=jobID,
                                                       resource_db=resource_db,
                                                       forward_search=False, # backward for dependencies
                                                       **dependencies_dict)

                        for dep in dep_res:
                            performance_dict[update['resource_id']][jobID] = dep['loading_ratio']
                            remove_key(dep, ['selectedNodes_indices', 'loading_ratio'])
                            register_resource(job_id=jobID, job_db=job_db, resource_db=resource_db, **dep)
                            del job_seq[jobID]

        except DispatchingException as e:
            logger.error(e)
            error_handle.append(str(e))
            sys.exit(e)

        except Exception as e:
            logger.debug(e)
            error_handle.append(str(e))

        except ValueError as e:
            logger.debug(e)
            error_handle.append(str(e))


def dependency_check(job_db:dict, dependencies_list:Sequence) -> bool:
    '''To make sure if all of jobs in the dependencies_list have done yet.

    Args:
        job_db:
        dependencies_list:

    Returns:
        A boolean value indicating if all dependencies are done or not.
    '''
    for dJID in dependencies_list:
        if not job_db[dJID]['is_finished']:
            return False
    return True


def select_best_resource(resource_db:dict, job_id:str, est_time:Number, waiting:Number, is_multi:bool, candidate_resources:dict, primary:str, secondary:tuple, due=None, forward_search=True):
    from itertools import accumulate, compress, chain
    from operator import add
    from numpy import array
    from copy import deepcopy

    cube = {x: list() for x in candidate_resources}
    candidates = tuple(chain(*candidate_resources.values()))

    # ===== FIND ALL CANDIDATE RESOURCE ZONES =====
    for rcid in candidates:
        queue = resource_db[rcid]['queue']
        allowOverlapped = resource_db[rcid]['allow_overlapped']
        capacity = resource_db[rcid]['capacity']

        # update: 2021/2/17
        try:
            if resource_db[rcid]['in_between']:
                availables = queue.find_all_availables(size=est_time, is_stacked=is_multi, offset=waiting, boundary=due, allowOverlapped=allowOverlapped, capacity=capacity)
            else:
                availables = queue.find_first_available(size=est_time, is_stacked=is_multi, offset=waiting, boundary=due, allowOverlapped=allowOverlapped, capacity=capacity)
        except:
            continue

        for available in availables:
            selectedNodes_indices = available.get_indices()
            workerNodes = [x for x in available if x.get_data()[0] != 0] # 不考慮breaktime
            workerNodes_sizeList = [x.get_data()[-1] for x in workerNodes]
            workerNodes_accumList = array(list(accumulate(workerNodes_sizeList, add)))
            workerNodes_indices = [x.get_index() for x in workerNodes] # original indices of the resource queue

            # ===== START NODE =====
            startpoint = 0
            startNode_idx = available.get_indices()[startpoint]
            est_start = sum([elem[-1] for elem in list(queue.elems)[:startNode_idx]])

            if est_start < waiting:
                waitTime_to_start = waiting - est_start
                est_start = waiting
            else:
                waitTime_to_start = None

            # ===== END NODE =====
            if waitTime_to_start:
                workers_qualified = workerNodes_accumList >= (est_time + waitTime_to_start)
            else:
                workers_qualified = workerNodes_accumList >= est_time

            # endpoint = [i for i, x in enumerate(workers_qualified) if x][0] # sol 1 -> slower
            endpoint = list(compress(range(len(workers_qualified)), workers_qualified))[0] # sol 2 -> faster
            endNode_idx = workerNodes_indices[endpoint]

            middle_time = sum([x.get_data()[-1] for x in workerNodes[:endpoint]])
            if waitTime_to_start:
                workTime_to_end = (est_time + waitTime_to_start) - middle_time
            else:
                workTime_to_end = est_time - middle_time

            est_end = sum([elem[-1] for elem in list(queue.elems)[:endNode_idx]]) + workTime_to_end

            # append to candidates list
            workerNodes_indices = workerNodes_indices[startpoint: endpoint+1]

            # new way to update cube[k]
            for k in cube.keys():
                if rcid in candidate_resources[k]:
                    cube[k].append((rcid, workerNodes_indices, selectedNodes_indices,
                                    startNode_idx, waitTime_to_start,
                                    endNode_idx, workTime_to_end,
                                    est_start, est_end, 1.0))

    # ===== SORT ALL CANDIDATE RESOURCE ZONES =====
    if forward_search:
        msg = f"job '{job_id}' can't be satisfied with insufficient available time."
        eCode = 350
    else:
        nextJobIdx = int(job_id.replace(JOB_PREFIX, '')) + 1
        nextJob_id = JOB_PREFIX + str(nextJobIdx)
        msg = f"job '{job_id}' can't be arranged in insufficient available time between current time and {nextJob_id}"
        eCode = 310

    for k in cube.keys():
        if not cube[k]:
            raise DispatchingException(eCode, msg)
        else:
            cube[k] = sorted(cube[k], key=lambda x: x[-2], reverse=False) # sort by est_end

    # ===== DETERMINE FOREWARD OR BACKWARD =====
    cube = cube if forward_search else {k: v[::-1] for k, v in cube.items()}

    # ===== FIND BEST MULTI-RESOURCE COMBINATION =====
    outcome = list()
    cols = ['resource_id', 'workerNodes_idx', 'selectedNodes_indices', 'startNode_idx', 'waitTime_to_start', 'endNode_idx', 'workTime_to_end', 'start_time', 'end_time', 'loading_ratio']
    if not is_multi:
        vals = next(iter(cube.values()))[0]
        single = dict(zip(cols, vals))
        outcome.append(single)
        return outcome
    else:
        try:
            multi = match_primary_secondary(primary_type=primary, secondary_type=secondary, cube=cube, resource_db=resource_db, est_time=est_time, forward_search=forward_search)
        except:
            raise DispatchingException(310, f"All primary resources of job '{job_id}' can't find matched secondary resources.")

        for vals in multi.values():
            single = dict(zip(cols, vals))
            outcome.append(single)
        return outcome


#TODO: move into utilities
def register_node(nodes_queue:LinkedList, node_idx:int, job_id:str):
    '''Arrange node in the LinkedList object.

    Args:
        nodes_queue:
        node_idx:

    Returns:
        None
    '''
    from copy import deepcopy

    # node data: [is_available, on_job, job_qty, time_length]
    node = nodes_queue.get_node(node_idx)
    new_data = deepcopy(node.get_data())
    new_data[1] = 1
    new_data[2] += 1
    nodes_queue.modify_node_job_state(index=node_idx, new_state=new_data[1])
    nodes_queue.modify_node_job_qty(index=node_idx, is_addition=True)
    nodes_queue.get_node(node_idx).set_job(job_id)
    nodes_queue.get_node(node_idx).update()


# register resource (dispatching)
def register_resource(job_db:dict, job_id:str, resource_db:dict, resource_id:str, workerNodes_idx:list, startNode_idx:int, waitTime_to_start:Number, endNode_idx:int, workTime_to_end:Number, start_time:Number, end_time:Number):
    '''Dispatching jobs to resource nodes.

    Args:
        job_db:
        job_id:
        resource_db:
        resource_id:
        workerNodes_idx:
        startNode_idx:
        waitTime_to_start:
        endNode_idx:
        workTime_to_end:
        start_time:
        end_time:

    Returns:
        None
    '''

    from copy import deepcopy

    queue = resource_db[resource_id]['queue']

    # CASE: only 1 segment
    if startNode_idx == endNode_idx:
        thisNode_idx = startNode_idx
        if waitTime_to_start is None:
            if workTime_to_end != queue.get_node(thisNode_idx).get_data()[-1]:
                # partially occupy former part
                queue.divide_node(thisNode_idx, workTime_to_end)

        else:
            if workTime_to_end == queue.get_node(thisNode_idx).get_data()[-1]:
                # partially occupy latter part
                queue.divide_node(thisNode_idx, waitTime_to_start)
                thisNode_idx += 1

            else:
                # partition twice and occupy the middle part
                queue.divide_node(thisNode_idx, workTime_to_end)
                queue.divide_node(thisNode_idx, waitTime_to_start)
                thisNode_idx += 1

        register_node(nodes_queue=resource_db[resource_id]['queue'], node_idx=thisNode_idx, job_id=job_id)

    else:
        # middle nodes
        for thisNode_idx in workerNodes_idx[1: -1]:
            register_node(nodes_queue=resource_db[resource_id]['queue'], node_idx=thisNode_idx, job_id=job_id)

        # ending node
        if queue.get_node(endNode_idx).get_data()[-1] == workTime_to_end:
            register_node(nodes_queue=resource_db[resource_id]['queue'], node_idx=endNode_idx, job_id=job_id)

        else:
            queue.divide_node(endNode_idx, workTime_to_end)
            register_node(nodes_queue=resource_db[resource_id]['queue'], node_idx=endNode_idx, job_id=job_id)

        # starter worker
        if waitTime_to_start is None:
            register_node(nodes_queue=resource_db[resource_id]['queue'], node_idx=startNode_idx, job_id=job_id)

        else:
            queue.divide_node(startNode_idx, waitTime_to_start)
            startNode_idx += 1
            register_node(nodes_queue=resource_db[resource_id]['queue'], node_idx=startNode_idx, job_id=job_id)

    job_db[job_id]['start_time'] = start_time
    job_db[job_id]['end_time'] = end_time
    job_db[job_id]['is_finished'] = True


def match_primary_secondary(primary_type:str, secondary_type:tuple, cube:dict, resource_db:dict, est_time:Number, forward_search:bool):
    '''match primary with appropriate secondary resources'''

    # re-sort cube by start time
    for k in cube.keys():
        cube[k] = sorted(cube[k], key=lambda x: x[7], reverse=False) # re-sort by est_start

    # ['resource_id', 'workerNodes_idx', 'selectedNodes_indices', 'startNode_idx', 'waitTime_to_start', 'endNode_idx', 'workTime_to_end', 'start_time', 'end_time', loading_ratio]
    for major in cube[primary_type]:
        lowerbound = major[7] # start_time
        upperbound = major[8] # end_time

        my_multi = dict()
        my_multi[primary_type] = major # 登錄master resource可用區間
        next_major = False

        # find minors' time zones
        for secondary in secondary_type:

            # 跳過已登錄的 minor resource type
            # if secondary in my_multi:
            #     continue

            for minor in cube[secondary]:
                resource_id = minor[0]
                start_time = minor[7]
                end_time = minor[8]
                selectedNodes_indices = minor[2]

                minor_queue = resource_db[resource_id]['queue']
                final_end = minor_queue.accumulated_to_node(selectedNodes_indices[-1])

                # forward: start time > lowerbound -> break
                # backward: final end < upperbound -> break
                if forward_search:
                    if start_time > lowerbound:
                        next_major = True
                        break
                else:
                    if final_end < upperbound:
                        next_major = True
                        break

                # new start node for minor
                if start_time > lowerbound:
                    continue

                elif start_time < lowerbound:
                    startNode_idx = minor_queue.get_node_by_accumulated_to(lowerbound).get_index()
                    waitTime_to_start = lowerbound - minor_queue.accumulated_to_node(startNode_idx-1)
                else:
                    startNode_idx = minor[3]
                    waitTime_to_start = minor[4]

                # new end node for minor
                if final_end < upperbound:
                    continue

                if end_time == upperbound:
                    endNode_idx = minor[5]
                    workTime_to_end = minor[6]
                else:
                    endNode_idx = minor_queue.get_node_by_accumulated_to(upperbound).get_index()
                    workTime_to_end = upperbound - minor_queue.accumulated_to_node(endNode_idx-1)

                # if breaktime and on duty nodes between new start time and end time
                if minor_queue.get_node(endNode_idx).get_data()[0] == 0:
                    # 1. workTime_to_end is Node size: nothing

                    # 2. workTime_to_end is part of Node size: divide node & workTime_to_end = new Node size
                    # TODO: flexible abs_tol for minute/hour/...
                    if not math.isclose(workTime_to_end, minor_queue.get_size(endNode_idx), abs_tol=1.0):
                        minor_queue.divide_node(endNode_idx, workTime_to_end)
                        workTime_to_end = minor_queue.get_size(endNode_idx)

                if minor_queue.get_node(startNode_idx).get_data()[0] == 0:
                    # 1. waitTime_to_start is None: nothing

                    # 2. waitTime_to_start is not None: divide node & startNode_idx + 1 & waitTime_to_start = None
                    if waitTime_to_start:
                        minor_queue.divide_node(startNode_idx, waitTime_to_start)
                        startNode_idx += 1
                        waitTime_to_start = None

                new_availability = 1
                on_duty = False
                for i in range(startNode_idx, endNode_idx+1):
                    condition = minor_queue.get_node(i).get_data()
                    if condition[1] == 1:
                        on_duty = True
                        break

                    if minor_queue.get_node(i).get_data()[0] == 0:
                        # force break time to be available time
                        minor_queue.modify_node_available_state(i, new_availability)


                if on_duty:
                    break

                # 登錄secondary resource的匹配區間
                workerNodes_idx = list(range(startNode_idx, endNode_idx+1))
                loading_ratio = est_time/(upperbound-lowerbound)
                my_multi[secondary] = (resource_id, workerNodes_idx, selectedNodes_indices,
                                       startNode_idx, waitTime_to_start,
                                       endNode_idx, workTime_to_end,
                                       lowerbound, upperbound, loading_ratio)

                break

            if next_major:
                break

        if len(my_multi) < len(cube):
            continue
        else:
            return my_multi
    if len(my_multi) < len(cube):
        raise DispatchingException(310, f"All primary resources of job '{job_id}' can't find matched secondary resources.")




# from networkx import DiGraph
# def gen_dependency_net(job_seq:list) -> DiGraph:
#     DG = DiGraph()
#     # linking dependencies
#     for thisJob in job_seq:
#         dependencies = thisJob[4]

#         if not dependencies:
#             continue

#         dependencies_num = len(dependencies)
#         thisJob_repeated = [thisJob[0]]*dependencies_num
#         DG.add_edges_from(zip(dependencies, thisJob_repeated))
#     return DG


def check_node_available(resource_db:dict, resource_id:dict, nodes_indices:Sequence) -> bool:
    '''Check the availability of the node.

    Args:
        resource_db:
        resource_id:
        nodes_indices:

    Returns:
        A boolean value indicating whether the node is available or not.

    '''
    queue = resource_db[resource_id]['queue']
    allowOverlapped = resource_db[resource_id]['allow_overlapped']
    # update: 2021/2/20
    maxJobs = resource_db[resource_id]['capacity']

    for node_idx in nodes_indices:
        fullity = queue.get_node(node_idx).get_data()[2] # node data: [is_available, on_job, job_qty, time_length]

        # check fullity > capacity OUTSIDE THE FUNCTION
        # update: 2021/2/20
        if fullity == resource_db[resource_id]['capacity']:
            return False

        if fullity != 0 and not allowOverlapped:
            return False

    return True