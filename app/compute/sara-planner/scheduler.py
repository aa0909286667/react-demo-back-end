#============== function: executor function ==============
def execute(db:dict, event_db:dict, ver:str, seq_method:str, start_time:str):
    '''Scheduler function using first-in-first-out algorithm to arrange job schedule

    Args:
        db: data with JSON format.
        event_db: data with JSON format.
        ver: version of df format.
        seq_method: 'order_date', 'order_due', or 'job_due'
        dispatching:
        start_time: The beginning time for scheduling launch. Preferable supported format 'yyyy-mm-dd HH:MM:SS'.
                    The default start_time is system current time.

    Return:
        A dict data same as input df but filled in schedule arrangement for resources and assigned resource for jobs
    '''
    from datetime import datetime
    t0 = datetime.now()

    import logging
    from logging.config import dictConfig
    from config import VARS, LOG_CONFIG
    from global_vars import RESULT, EVALUATION_TABLE

    import workflow
    import preprocessing as prep
    import sequencing as seque
    import dispatching as disp
    import postprocessing as postp

    dictConfig(LOG_CONFIG)
    logger = logging.getLogger('myLogger')

    ver = ver.upper()

    order_db = 'order_db'
    product_db = 'product_db'
    process_db = 'process_db'
    job_db = 'job_db'
    resource_db = 'resource_db'
    workingdays_db = 'workingdays_db'
    workinghours_db = 'workinghours_db'

    try:
        from dateutil.parser import parse
        t0 = parse(start_time) if isinstance(start_time, str) else t0
    except Exception as e:
        logger(e)

    from termcolor import colored # delete it when on production
    print(colored('===== ENTER PREPROCESSING =====', 'green'))
    t1 = datetime.now()
    processed_db = prep.run(db, ver, **VARS['db_keys'])
    print(f'TOTAL TIME in PREPROCESSING: {datetime.now()-t1}')
    print(colored('===== EXIT FROM PREPROCESSING =====', 'green'))

    print(colored('===== ENTER SEQUENCING =====', 'green'))
    t2 = datetime.now()
    my_sequence = seque.run(sort_by=seq_method, time_base=t0, **processed_db)
    print(f'TOTAL TIME in SEQUENCING: {datetime.now()-t2}')
    print(colored('===== EXIT FROM SEQUENCING =====', 'green'))

    # Before Dispatching and Postprocessing, initialise performance dictionary
    for k in processed_db[resource_db].keys():
        EVALUATION_TABLE[k] = dict()

    print(colored('===== ENTER DISPATCHING =====', 'green'))
    t3 = datetime.now()
    disp.run(time_base=t0,
             event_db=event_db,
             job_seq=my_sequence['job_seq'],
             job_db=processed_db[job_db],
             resource_db=processed_db[resource_db],
             workingdays_db=processed_db[workingdays_db],
             workinghours_db=processed_db[workinghours_db])
    print(f'TOTAL TIME in DISPATCHING: {datetime.now()-t3}')
    print(colored('===== EXIT FROM DISPATCHING =====', 'green'))

    print(colored('===== ENTER POSTPROCESSING =====', 'green'))
    t4 = datetime.now()
    # update: 2021/2/17
    schedule_result = postp.run(time_base=t0,
                                resource_db=processed_db[resource_db],
                                job_db=processed_db[job_db],
                                job_seq=my_sequence['job_seq'])
    print(f'TOTAL TIME in POSTPROCESSING: {datetime.now()-t4}')
    print(colored('===== EXIT FROM POSTPROCESSING =====', 'green'))

    RESULT['schedule'] = schedule_result
    return RESULT
