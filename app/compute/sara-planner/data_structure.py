'''This module defines basic data structures of required classes.'''

from numbers import Number

# import logging
# from logging.config import dictConfig
# from config import *
# from error_handler import *

# dictConfig(LOG_CONFIG)
# logger = logging.getLogger('baseLogger')


class Node():
    """A class to represent a node.

    Attributes:
        data (list): The data value stored in the node object.
        index (int): The index of the node object to a series of node object.
        next_node (Node): The next node object in a series of node object.
        prev_node (Node): The previous node object in a series of node object.
        jobs (str/set/list/tuple): The job ID that the node object works for.

    Methods:
        get_next():
            Return the next node.
        set_next(next_node):
            To set the next node of this node.
        get_prev():
            Return the previous node.
        set_prev(prev_node):
            To set the previous node this node.
        get_data():
            Return data of the node.
        set_data(data):
            Assign data to the node.
        get_index():
            Return the index of the node.
        set_index(index):
            To set the index of the node.
        get_job():
            Return set of working jobs on the node.
        set_job(jobs):
            To record working jobs with the node.
        get_log():
            Return times of edition on the node.
        set_log(new_log):
            To overwrite the log of the node with new log.
        update():
            To update times of edition on the node.
        copy():
            Return a duplicated node.
    """

    def __init__(self, data, index=None, next_node=None, prev_node=None, job=None):
        '''Constructs all the necessary attributes for the node object.

        Args:
            data (list): The data value stored in the node object.
            index (int): The index of the node object to a series of node object.
            next_node (Node): The next node object in a series of node object.
            prev_node (Node): The previous node object in a series of node object.
            jobs (str/set/list/tuple): The job ID that the node object works for.
        '''
        self.data = data
        self.index = index
        self.next = next_node
        self.previous = prev_node
        self.job = set()
        self.log = 0

        # update: 2021/2/23
        if job:
            self.set_job(job)

    def __repr__(self):
        return str(self.data)

    def get_next(self):
        return self.next

    def set_next(self, next_node):
        self.next = next_node

    def get_prev(self):
        return self.previous

    def set_prev(self, prev_node):
        self.previous = prev_node

    def get_data(self):
        return self.data

    def set_data(self, data):
        self.data = data

    def get_index(self):
        return self.index

    def set_index(self, index:int):
        self.index = index

    def get_job(self):
        return self.job

    def set_job(self, job):
        if isinstance(job, str):
            self.get_job().add(job)
        else:
            self.get_job().update(job)

    def get_log(self):
        return self.log

    def set_log(self, new_log):
        self.log = new_log

    def update(self):
        self.log += 1

    def copy(self):
        from copy import deepcopy

        content = dict()
        content['data'] = deepcopy(self.get_data())
        content['job'] = deepcopy(self.get_job())
        content['index'] = self.get_index()
        content['next_node'] = self.get_next()
        content['prev_node'] = self.get_prev()
        node = Node(**content)
        log = self.get_log()
        node.set_log(log)
        return node


class LinkedList():

    def __init__(self, nodes=None):
        from collections import deque

        self.head = None
        self.elems = deque()

        if isinstance(nodes, LinkedList):
            nodes.reset_index()
            self = nodes
        elif isinstance(nodes, list):
            node = Node(data=nodes.pop(0), index=0)
            self.head = node
            self.elems.append(node.get_data())
            for idx, elem in enumerate(nodes, start=1):
                node.set_next(Node(data=elem, index=idx))
                node.get_next().set_prev(node)
                node = node.get_next()
                self.elems.append(node.get_data())


    def __iter__(self):
        node = self.head
        while node is not None:
            yield node
            node = node.get_next()


    def __repr__(self):
        return str(self.elems)


    def _to_node(self, node):
        if not isinstance(node, Node):
            node = Node(node)
        return node


    def reverse_index(self, target_node_index:int): # node_index ---> target_node_index
        '''only negative to positive'''
        if target_node_index < 0: # node_index ---> target_node_index
            node = self.get_last_node()
            length = len(self.get_data())
            while node is None:
                reverse = node.get_index() - length
                if reverse == target_node_index:
                    return node.get_index()
                node = node.get_prev()

            if node is None:
                raise IndexError('List index out of range')

        return target_node_index


    def reset_index(self, offset:'int'=0):
        '''reset indices with new start index as offset'''
        indices = list(range(offset, len(self.elems)+offset))
        for node in self:
            node.set_index(indices.pop(0))


    def subset(self, start:int, to:int, index_reset=True):
        '''excludes right bound'''
        if start == to:
            raise ValueError("Can't slice an empty list. 'to' must be greater than 'start'.")

        new_list = list(self.elems)[start: to]
        new_list = LinkedList(new_list)
        if index_reset:
            new_list.reset_index()
        else:
            new_list.reset_index(offset=start)
        return new_list


    def reset_head(self, new_head_index=0):
        if not self.head:
            raise DispatchingException(210, "List is empty")

        new_head_index = self.reverse_index(new_head_index)

        if new_head_index > self.get_last_node().get_index():
            raise IndexError('Index out of range')

        node = self.head
        while node.get_index() != new_head_index:
            node = node.get_next()

        if node.get_prev():
            node.get_prev().set_next(None)
            node.set_prev(None)
            self.head = node
            sub = self.subset(start=new_head_index, to=None)
            self.elems = sub.elems
            self.reset_index()
        return


    def get_indices(self):
        li = []
        for node in self:
            li.append(node.get_index())
        return li


    def get_length(self):
        count = 0
        node = self.head

        while node is not None:
            count += 1
            node = node.get_next()
        return count


    def get_last_node(self):
        for node in self:
            pass
        return node


    def get_node(self, index:int):
        index = self.reverse_index(index)

        if index > self.get_last_node().get_index():
            raise IndexError('Index out of range')

        node = self.head
        while node.get_index() != index:
            node = node.get_next()
        return node


    def add_first(self, node):
        self.reset_index(offset=1)

        node = self._to_node(node)

        node.set_next(self.head)
        self.head.set_prev(node)
        self.head = node
        self.elems.appendleft(node.get_data())
        self.reset_index()


    def add_last(self, node):
        node = self._to_node(node)
        if not self.head:
            self.head = node
            self.elems = deque([node.get_data()])
            return

        prev = self.get_last_node()
        prev.set_next(node)
        node.set_prev(prev)
        node.set_index(prev.get_index()+1)
        self.elems.append(node.get_data())


    def add_after(self, target_node_index:int, new_node):
        if not self.head:
            raise DispatchingException(210, "List is empty")

        target_node_index = self.reverse_index(target_node_index)

        if target_node_index > self.get_last_node().get_index():
            raise IndexError('List index out of range')

        new_node = self._to_node(new_node)

        if target_node_index == self.get_last_node().get_index():
            return self.add_last(new_node)

        for idx, node in enumerate(self):
            if node.get_index() == target_node_index:
                new_node.set_next(node.get_next())
                node.next.set_prev(new_node)

                node.set_next(new_node)
                new_node.set_prev(node)

                self.elems.insert(idx+1, new_node.get_data())
                self.reset_index()
                return


    def add_before(self, target_node_index:int, new_node):
        if not self.head:
            raise DispatchingException(210, "List is empty")

        target_node_index = self.reverse_index(target_node_index)

        if target_node_index > self.get_last_node().get_index():
            raise IndexError('Index out of range')

        if self.head.get_index() == target_node_index:
            return self.add_first(new_node)

        prev_node = self.head
        new_node = self._to_node(new_node)
        for idx, node in enumerate(self):
            if node.get_index() == target_node_index:
                prev_node.set_next(new_node)
                new_node.set_prev(prev_node)

                new_node.set_next(node)
                node.set_prev(new_node)

                self.elems.insert(idx, new_node.get_data())
                self.reset_index()
                return

            prev_node = node


    def remove_node(self, target_node_index:int):
        if not self.head:
            raise DispatchingException(210, "List is empty")

        if target_node_index < 0:
            target_node_index = self.reverse_index(target_node_index)

        if target_node_index > self.get_last_node().get_index():
            raise IndexError('Index out of range')

        if self.get_length() == 1:
            self = LinkedList()
            return

        if self.head.get_index() == target_node_index:

            self.head = self.head.get_next()
            self.head.set_prev(None)

            self.elems.popleft()
            self.reset_index()
            return

        prev_node = self.head # self.head -> None
        for node in self:
            if node.get_index() == target_node_index:
                prev_node.set_next(node.get_next())

                if node.get_next() is not None:
                    node.get_next().set_prev(prev_node)

                del self.elems[node.get_index()]
                self.reset_index()
                return

            prev_node = node


    # TODO: to be deprecated
    def replace_node(self, index:int, new_node):
        if index < 0:
            index = self.reverse_index(index)

        if index > self.get_last_node().get_index():
            raise IndexError('Index out of range')

        if not isinstance(new_node, Node):
            new_node = self._to_node(new_node)

        node = self.head
        while node.get_index() != index:
            node = node.get_next()

        node.set_data(new_node.get_data())
        self.elems[index] = new_node.get_data()
        return


    # specific to structure [is_available, on_job, job_qty, time_length]
    def get_size(self, index:int):
        return self.elems[index][-1]


    # TODO:
    # specific to structure [is_available, on_job, job_qty, time_length]
    def modify_node_available_state(self, index, new_state):
        from copy import deepcopy
        node = self.get_node(index)
        new_data = deepcopy(node.get_data())
        new_data[0] = new_state
        node.set_data(new_data)
        self.elems[index] = new_data
        return


    # TODO:
    # specific to structure [is_available, on_job, job_qty, time_length]
    def modify_node_job_state(self, index, new_state):
        from copy import deepcopy
        node = self.get_node(index)
        new_data = deepcopy(node.get_data())
        new_data[1] = new_state
        node.set_data(new_data)
        self.elems[index] = new_data
        return


    # TODO:
    # specific to structure [is_available, on_job, job_qty, time_length]
    def modify_node_job_qty(self, index, is_addition:'bool'=True):
        from copy import deepcopy
        node = self.get_node(index)
        new_data = deepcopy(node.get_data())
        if is_addition:
            new_data[2] += 1
        else:
            new_data[2] -= 1

        node.set_data(new_data)
        self.elems[index] = new_data
        return


    # specific to structure [is_available, on_job, job_qty, time_length]
    def _overlapping(self, size:Number, offset:Number, jump_in:bool, allowOverlapped:bool, capacity:int, boundary=None) -> list:
        availables = []

        #===== start node index =====
        if jump_in:
            list_start_idx = self.get_node_by_accumulated_to(offset).get_index()
            wait = offset - self.accumulated_to_node(list_start_idx-1)
        else:
            flag_node = self.get_node(self.get_length() - 1)
            list_start_idx = flag_node.get_index()
            while flag_node.get_data()[0] == 1: # if node is unavailable, break
                list_start_idx = flag_node.get_index()
                flag_node = flag_node.get_prev()

            wait = 0
            if self.accumulated_to_node(list_start_idx-1) < offset:
                list_start_idx = self.get_node_by_accumulated_to(offset).get_index()
                wait = offset - self.accumulated_to_node(list_start_idx-1)

        if wait == self.get_size(list_start_idx):
            list_start_idx += 1
            wait = 0

        #===== end node index =====
        if boundary:
            list_end_idx = self.get_node_by_accumulated_to(boundary).get_index() + 1
        else:
            list_end_idx = None
            boundary = self.accumulated_to_node(self.get_length()-1)

        if self.accumulated_to_node(list_start_idx) > boundary:
            pre_size = self.accumulated_to_node(list_start_idx)
            list_end_idx = self.get_node_by_accumulated_to(pre_size + size).get_index() + 1

        #===== collection =====
        for node in self.subset(list_start_idx, list_end_idx, index_reset=False):
            has_block = False
            nData = node.get_data()

            if node.get_index() > list_start_idx:
                wait = 0

            # 起頭必須為可用的區間
            if nData[0] == 1:
                base = self.accumulated_to_node(node.get_index()-1)
                freeList = self.subset(node.get_index(), list_end_idx, index_reset=False) # do not reset indices

                available_size = 0
                startNode_idx = freeList.get_indices()[0]
                endNode_idx = None
                for freeNode in freeList:
                    fnData = freeNode.get_data()

                    if allowOverlapped:
                        # 可overlapped情況下，Node的job數超過限制
                        if fnData[2] >= capacity:
                            has_block = True
                            break
                    else:
                        # 不可overlapped情況下，中間卡其他job
                        if fnData[1] == 1:
                            has_block = True
                            break

                    # 資源不開工
                    if fnData[0] == 0:
                        continue

                    available_size += fnData[-1]

                if available_size >= (size + wait):
                    if (base + size) > boundary:
                        break
                    endNode_idx = freeNode.get_index()
                    endNode_idx = endNode_idx if has_block else (endNode_idx + 1)
                    availables.append(self.subset(startNode_idx, endNode_idx, False))

        return availables


    # specific to structure [is_available, on_job, job_qty, time_length]
    def _nonoverlapping(self, size:Number, offset:Number, jump_in:bool, allowOverlapped:bool, capacity:int, boundary=None) -> list:
        availables = []

        #===== start node index =====
        if jump_in:
            nextStartNode_idx = self.get_node_by_accumulated_to(offset).get_index()
            wait = offset - self.accumulated_to_node(nextStartNode_idx-1)
        else:
            starter_idx_forward = self.get_node_by_accumulated_to(offset).get_index()

            flag_node = self.get_node(self.get_length() - 1)
            starter_idx_backward = flag_node.get_index()
            while flag_node.get_data()[0] == 1: # if node is unavailable, break
                starter_idx_backward = flag_node.get_index()
                flag_node = flag_node.get_prev()

            if starter_idx_backward > starter_idx_forward:
                nextStartNode_idx = starter_idx_backward
                wait = 0
            else:
                nextStartNode_idx = starter_idx_forward
                wait = offset - self.accumulated_to_node(nextStartNode_idx - 1)

        if wait == self.get_size(nextStartNode_idx):
            nextStartNode_idx += 1
            wait = 0

        #===== end ndoe index =====
        if boundary:
            if boundary <= self.accumulated_to_node(nextStartNode_idx-1):
                boundary = self.accumulated_to_node(nextStartNode_idx-1) + size

            seriesEndNode_idx = self.get_node_by_accumulated_to(boundary).get_index() + 1

        else:
            seriesEndNode_idx = None
            boundary = self.accumulated_to_node(self.get_length()-1)

        upperbound = seriesEndNode_idx if seriesEndNode_idx else self.get_length()

        #===== collection =====
        while nextStartNode_idx < upperbound:
            node = self.get_node(nextStartNode_idx)
            has_block = False
            nData = node.get_data()

            # 起頭必須為可用的區間
            if nData[0] == 1 and wait < nData[-1]:
                base = self.accumulated_to_node(node.get_index()-1)
                # TODO: when nextStartNode_idx jump to the last node, nextStartNode_idx == seriesEndNode_idx GET ERROR
                freeList = self.subset(node.get_index(), seriesEndNode_idx, index_reset=False) # do not reset indices

                available_size = 0
                startNode_idx= freeList.get_indices()[0]
                endNode_idx = None
                for freeNode in freeList:
                    fnData = freeNode.get_data()

                    if allowOverlapped:
                        # 可overlapped情況下，Node的job數超過限制
                        if fnData[2] >= capacity:
                            has_block = True
                            break
                    else:
                        # 不可overlapped情況下，中間卡其他job
                        if fnData[1] == 1:
                            has_block = True
                            break

                    # 資源不開工
                    if fnData[0] == 0:
                        continue

                    available_size += fnData[-1]
                    nextStartNode_idx = freeNode.get_index() + 1

                if available_size >= (size + wait):
                    if (base + size) > boundary:
                        break
                    endNode_idx = freeNode.get_index()
                    endNode_idx = endNode_idx if has_block else (endNode_idx + 1)
                    availables.append(self.subset(startNode_idx, endNode_idx, False))

                if has_block:
                    nextStartNode_idx = freeNode.get_index() + 1
                    wait = 0

            if not has_block: # has_block is False
                nextStartNode_idx += 1
                wait = 0

        return availables


    def search_engine(self, is_stacked:bool, size:Number, offset:Number, jump_in:bool, allowOverlapped:bool, capacity:int, boundary=None):
        ''' Select resource search engine according to arguments.

        is_stacked: if repeatedly extend all available resources.
        '''
        def select_engine(is_stacked):
            nonlocal size, offset, jump_in, allowOverlapped, capacity, boundary

            if is_stacked:
                return self._overlapping(size, offset, jump_in, allowOverlapped, capacity, boundary)
            else:
                return self._nonoverlapping(size, offset, jump_in, allowOverlapped, capacity, boundary)

        avails = select_engine(is_stacked=is_stacked)
        while not avails:
            next_node_idx = self.get_node_by_accumulated_to(boundary).get_index() + 1
            boundary = boundary + self.get_size(next_node_idx)
            avails = select_engine(is_stacked=is_stacked)

        return avails


    def find_all_availables(self, is_stacked:bool, size:Number, offset:Number, allowOverlapped:bool, capacity:int, boundary=None) -> list:
        jump_in = True # cut in resource queue to find earliest avaialble resoures
        return self.search_engine(is_stacked, size, offset, jump_in, allowOverlapped, capacity, boundary)


    def find_first_available(self, is_stacked:bool, size:Number, offset:Number, allowOverlapped:bool, capacity:int, boundary=None) -> list:
        jump_in = False # NOT cut in resource queue, wait in resource queue to get first available resource instead
        return self.search_engine(is_stacked, size, offset, jump_in, allowOverlapped, capacity, boundary)


    def is_satisfied(self, target_node_index:int, data_index:'int'=0, condition=None):
        if target_node_index < 0:
            target_node_index = self.reverse_index(target_node_index)

        if target_node_index > self.get_last_node().get_index():
            raise IndexError('Index out of range')

        node = self.get_node(target_node_index)


        if node.get_data()[data_index] == condition:
            return True
        else:
            return False


    def unify_available_state_between(self, lowerbound:Number, upperbound:Number, new_state:int):
        left_node = self.get_node_by_accumulated_to(lowerbound)
        right_node = self.get_node_by_accumulated_to(upperbound)

        left_offset = lowerbound - self.accumulated_to_node(left_node.get_index()-1)
        right_offset = upperbound - self.accumulated_to_node(right_node.get_index()-1)

        if right_offset != self.get_size(right_node.get_index()):
            self.divide_node(right_node.get_index(), right_offset)

        if left_offset != self.get_size(left_node.get_index()):
            self.divide_node(left_node.get_index(), left_offset)

        left_node = left_node.get_next()

        for idx in range(left_node.get_index(), right_node.get_index()+1):
            self.modify_node_available_state(idx, new_state)

        return


    # update: 2021/2/23
    # specific to structure [is_available, on_job, job_qty, time_length]
    def divide_node(self, target_node_index:int, data:list):
        from copy import deepcopy

        if not self.head:
            raise DispatchingException(210, "List is empty")

        target_node_index = self.reverse_index(target_node_index)

        if target_node_index > self.get_last_node().get_index():
            raise IndexError('Index out of range')

        if self.get_length() == 1:
            left_node = self.head.copy()
            left_part = left_node.get_data()
            left_part[-1] = data
            left_node.set_data(left_part)

            right_node = self.head.copy()
            right_part = right_node.get_data()
            right_part[-1] = right_part[-1] - data
            right_node.set_data(right_part)

            self.add_first(left_node)
            self.add_last(right_node)
            self.remove_node(target_node_index+1)
            return


        if self.head.get_index() == target_node_index:
            left_node = self.head.copy()
            left_part = left_node.get_data()
            left_part[-1] = data
            left_node.set_data(left_part)

            right_node = self.head.copy()
            right_part = right_node.get_data()
            right_part[-1] = right_part[-1] - data
            right_node.set_data(right_part)

            self.remove_node(self.head.get_index())

            self.add_first(right_node)
            self.add_first(left_node)
            return

        prev_node = None
        for node in self:
            if node.get_index() == target_node_index:
                left_node = node.copy()
                left_part = left_node.get_data()
                left_part[-1] = data
                left_node.set_data(left_part)

                right_node = node.copy()
                right_part = right_node.get_data()
                right_part[-1] = right_part[-1] - data
                right_node.set_data(right_part)

                flag = prev_node.get_index()

                self.remove_node(node.get_index())
                self.add_after(flag, right_node)
                self.add_after(flag, left_node)
                return

            prev_node = node


    # specific to structure [is_available, on_job, job_qty, time_length]
    def get_node_by_accumulated_to(self, value:int) -> Node:
        _value = 0
        node = self.head
        if node is None:
            raise DispatchingException(210, 'List is empty')

        while node is not None:
            _value += node.get_data()[-1]
            if _value >= value:
                break
            node = node.get_next()

        return node


    # specific to structure [is_available, on_job, job_qty, time_length]
    def accumulated_to_node(self, target_node_index:int) -> Number:
        _value = 0
        node = self.head
        if node is None:
            raise DispatchingException(210, 'List is empty')

        while node.get_index() <= target_node_index:
            _value += node.get_data()[-1]
            if node.get_index() == target_node_index:
                break
            else:
                node = node.get_next()

        return _value


    # specific to structure [is_available, on_job, job_qty, time_length]
    def merge_nodes(self, start_index:int, forward:'bool'=True):
        from copy import deepcopy

        if forward:
            left_node = self.get_node(start_index)
            right_node = self.get_node(start_index+1)

        else:
            left_node = self.get_node(start_index-1)
            right_node = self.get_node(start_index)

        left_available = left_node.get_data()[0]
        right_available = right_node.get_data()[0]

        left_job = left_node.get_data()[1]
        right_job = right_node.get_data()[1]

        left_qty = left_node.get_data()[2]
        right_qty = right_node.get_data()[2]

        if left_available != right_available:
            raise Exception("Can't merge nodes with different availability status")
        elif left_job != right_job:
            raise Exception("Can't merge nodes with different job status")
        elif left_qty != right_qty:
            raise Exception("Can't merge nodes with different job quantity")


        if left_node.get_job() or right_node.get_job():
            if left_job.get_job() != right_node.get_job():
                raise Exception("Can't merge nodes with different job")

        new_data = deepcopy(left_node.get_data())
        new_data[-1] += deepcopy(right_node.get_data()[-1])
        self.replace_node(left_node.get_index(), new_data)
        # left_node.set_data(new_data)
        # self.elems[left_node.get_index()] = new_data
        self.remove_node(right_node.get_index())
        return
