def initialize():
    global RESULT, EVALUATION_TABLE, JOB_PREFIX

    RESULT = dict()
    RESULT['warning'] = list()
    RESULT['error'] = list()

    EVALUATION_TABLE = dict()

    JOB_PREFIX = 'JID'
