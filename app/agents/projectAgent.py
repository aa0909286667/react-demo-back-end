from sqlalchemy.orm.attributes import flag_modified    
from app.base.models import SQLProjectModel, SQLProductModel
from app.api.utils import get_current_time_string


from app import db as sql_db
from sqlalchemy import or_, and_, func
"""
web project-agent
in responsible for real time status
"""

class ProjectAgent(object):

    def __init__(self,pid,sql_p=None):
        if sql_p is None:
            self.sql_p = SQLProjectModel.query.filter(
                or_(
                SQLProjectModel.index == pid, 
                SQLProjectModel.project_name == pid
                )).first()
        else:
            self.sql_p = sql_p

    def get_general_info(self):
        info = dict(
            order_id = self.sql_p.order_id,
            order_date = self.sql_p.order_date,
            project_name = self.sql_p.project_name,
            project_type = self.sql_p.project_type,
            due = self.sql_p.due,
            customer_id = self.sql_p.customer_id,
            status = self.sql_p.status
        )
        return info
    
    def end(self,prdid):
        if prdid == self.sql_p._product[-1]:
            self.sql_p.status = "complete"
            sql_db.session.commit()
            
    def update_plan_end_time(self,model_set=None):
        
        sql_product = self.get_end_product(model_set)
        if sql_product is None:
            self.sql_p.status = "hold"
        else:
            self.sql_p.plan_end_time = sql_product.plan_end_time
            if sql_product.plan_end_time is None:
                self.sql_p.status = "hold"
            else:
                self.sql_p.status = "released"
                self.check_delay()

        
        self.sql_p.updated_on = get_current_time_string()
        
        sql_db.session.commit()
    def update_missing_data(self):
        sql_product = self.get_end_product()
        if sql_product is None:
            return
        else:
            self.sql_p.order_date = sql_product.order_date
            self.sql_p.due = sql_product.due
            self.sql_p.order_id = sql_product.order_id

    def get_end_product(self,model_set=None):
        if model_set != None:
            SQLProductModel = model_set[0]
        else:
            from app.base.models import SQLProductModel
        if len(self.sql_p._product) < 1:
            return None 
        product_id =  self.sql_p._product[-1]
        sql_product = SQLProductModel.query.filter(SQLProductModel.index == product_id).first()
        return sql_product

    def check_delay(self):
        if self.sql_p.due is not None and self.sql_p.plan_end_time > self.sql_p.due:
            self.sql_p.abnormal_status["delay"] = True
            flag_modified(self.sql_p, "abnormal_status")
            sql_db.session.commit()
    
    def update_buffer_time(self):
        from app.agents.productAgent import ProductAgent
        for prdid in self.sql_p._product:
            product_agent = ProductAgent(prdid)
            product_agent.update_buffer_time(self.sql_p.due)
            
   
        
