import os
from flask import jsonify, current_app, abort
from app.api.utils import save_json_to_file
from app.api.utils import get_current_time_string

from app.local_config import CUSTOMER_NAME, CUSTOMER_MES_API, CUSTOMER_ERP_INPR_ORDER_API
import requests
import json
import time
from app.api.schedulerAPI import Schedule
from app.datagent import run

def call_MES_scheduling_api(timestamp):
    
    print("==== call MES api ====")
    response = requests.get(CUSTOMER_MES_API)
    data_db = json.loads(response.text)
    filename = timestamp + ".json"
    path = os.path.join('app','schedule_result',filename)
    save_json_to_file(data_db, filename="",path=path)
    print("==== MES api result get ====")
    return path, data_db


def call_ERP_inprogress_order_api(timestamp):
    url = CUSTOMER_ERP_INPR_ORDER_API 
    response = requests.get(url)
    data_db = json.loads(response.text)
    keys = ["mo_nbr", "mo_nbr_step", "mo_lot_nbr", "wip_qty", "item_no", 
        "ord_nbr", "mo_release_date", "mo_required_date", "shp_date_class", "plan_shp_date"]
    res = []
    for row in data_db:
        item = {key:val for key, val in zip(keys,row)}
        res.append(item)
    
    filename = "ERP_" + timestamp + ".json"
    path = os.path.join('app','schedule_result',filename)
    save_json_to_file(res, filename="",path=path)
    
    print("==== ERP api result get ====")
    return path, res 

def GetScheduleResult():
    # GetScheduleResult
    # ---- celery task 1: ----  
    # 重選scheduleDB + 新增空 scheduleResult scheduleDB
    # datagent run switcher
    # 更新scheduleDB
    # 刪除db，複製processingDB 至 現在db
    # 刪除processingDB
    timestamp = get_current_time_string() 
    info = dict(
        time = timestamp,
        schedule_status = "mes_schedule",
        gantt_status = "NotImplemented",
        status = "update scheduling"
    )
    sql_s = Schedule.post(info)

    erp_path, erp_db =call_ERP_inprogress_order_api(timestamp)
    mes_path, data_db = call_MES_scheduling_api(timestamp)
    #erp_path = "app/schedule_result/ERP_2021-05-06 11:37.json"
    #mes_path = "app/schedule_result/2021-05-06 11:37.json"
    print("==== call datagent ====")
    run.switcher(mode="pipeline",path=[erp_path,mes_path],customer_name=CUSTOMER_NAME)
    
    from app.agents.scheduleAgent import delete_db, copy_to_db, delete_p_db
    delete_db()
    copy_to_db()
    delete_p_db() 
    
    update_info = dict(
        schedule_result = dict(ERP=erp_db, MES=data_db),
        time = timestamp,
        status =  "released"
    )
    Schedule.put(sid=sql_s.id,info=update_info)
    return sql_s.id
    


def ReselectScheduleResult(sid):
    # ---- celery task 1: ----  
    # 重選scheduleDB +  編輯ScheduleResult scheduleDB
    # datagent run switcher
    # 更新scheduleDB
    from app.api.schedulerAPI import Schedule
    from app.datagent import run
    
    raise NotImplementedError
    # info = dict(
    #     status = "update scheduling",
    #     selected = True
    # )
    # sql_s = Schedule.put(sid, info)
    # print("==== call datagent ====")
    # run.switcher(mode="MES",path=sql_s.time,customer_name=CUSTOMER_NAME)
    # update_info = {
    #     "status": "released"
    # }
    # Schedule.put(sid,update_info)


def GetMESUpdate():
    # GetMESUpdate
    # ---- celery task 1: ----  
    # datagent run mes_update_wip
    # 新增 MESResult scheduleDB 
    from app.api.schedulerAPI import Schedule
    from app.datagent import run
    
    print("==== call datagent for update ====")
    data = run.mes_update_wip(customer_name=CUSTOMER_NAME)
    info = dict(
        time = get_current_time_string(),
        schedule_result = data,
        schedule_status = "mes_update",
        gantt_status = "NotImplemented",
        status="released"
    )
    Schedule.post(info=info)
