from flask_restful import Resource, request
from app.base.models import SQLBottleneckSetting

from app import db as sql_db

class BottleneckSetting(Resource):
    """
    ignore_setting: never, ignoreOnce, alwaysIgnore
    """
    @staticmethod
    def post(info=None):
        if info is None: 
            info = request.json
        sql_b = SQLBottleneckSetting(**info)
        sql_db.session.add(sql_b)
        sql_db.session.commit()
    
    @staticmethod
    def get(job_name):
        sql_b = SQLBottleneckSetting.query.filter(SQLBottleneckSetting.job_name  == job_name).first()
        if sql_b is None:
            sql_b = SQLBottleneckSetting.query.filter(SQLBottleneckSetting.job_name  == 'all').first()
        return sql_b
    
    @staticmethod
    def put(jlb_name):
        info = request.json 
        sql_b = SQLBottleneckSetting.query.filter(SQLBottleneckSetting.job_name == jlb_name).first()
        if sql_b is None:
            BottleneckSetting.post(info)
        else:
            for key, value in info.items():
                setattr(sql_b, key, value)

            sql_db.session.commit()
    
    @staticmethod
    def delete():
        pass