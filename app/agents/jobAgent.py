from datetime import datetime, timedelta
from flask_restful import Resource, request
from sqlalchemy import desc
from sqlalchemy import or_, and_
from sqlalchemy.orm.attributes import flag_modified

from app.base.models import SQLJobModel, SQLJobLib, SQLProductModel, SQLErrorSetting, SQLNotificationModel
from app.api.utils import get_current_time_string, convert_datetime_to_str, convert_str_to_datetime, get_current_time
from app.agents.dataAgent import pause_following_jobs, resume_following_jobs
from app.agents.dataAgent import find_operation_delay_threshold, find_bottleneck_threshold, find_working_hours_mismatach_threshold
from app.agents.resourceAgent import ResourceAgent
print("import jobAgent")
import logging
logging.getLogger().setLevel(logging.INFO)

from app import db as sql_db

class GetJob(Resource):
    @staticmethod
    def get_job_error_table():
        SQLErrorEntities = SQLErrorSetting.query.filter(SQLErrorSetting.error_type=='工作').all()
        #error_json = serialize_db(SQLErrorEntities)
        sol = {}

        for sql_e in SQLErrorEntities:
            if sql_e.error_group not in sol:
                sol[sql_e.error_group] = {}
            sol[sql_e.error_group][sql_e.index] = sql_e.error_code
        return sol

    @staticmethod
    def get(jid):

        ja = JobAgent(jid)

        sol = ja.get_job_info()
        sol.update(ja.get_time_info())
        sol.update(ja.get_history())

        ongoing_error = []
        for sql_n in SQLNotificationModel.query.filter(SQLNotificationModel.jid==jid).all():
            if sql_n.status == 'report_sent' or sql_n.status == 'report_received':
                ongoing_error.append(sql_n.event_type)
        sol["ongoing_error"] = ongoing_error

        return [sol,GetJob.get_job_error_table()]

    @staticmethod
    def link_products_by_sequence(sql_j):
        sql_product = SQLProductModel.query.filter(SQLProductModel.index == sql_j.product_id).first()
        if str(sql_j.job_sequence) not in sql_product._jobID_list:
            sql_product._jobID_list[str(sql_j.job_sequence)] = []
        sql_product._jobID_list[str(sql_j.job_sequence)] = [sql_j.index]
        flag_modified(sql_product, "_jobID_list")
        return sql_product

    @staticmethod
    def post(info=None):

        from app.agents.jobLib import JobLibItem
        jlb_keys = ["full_name","jlb_name","process_name","job_name", "sourcing","_material","jlb_rule"]

        if info == None:
            info = request.json
        print(info)
        index = "JID" + str(SQLJobModel.query.count())

        if len(info["job_name"]) ==0 and len(info["jlb_name"]) > 0:
            info["job_name"] = info["jlb_name"]
        if len(info["jlb_name"]) == 0  and len(info["job_name"]) > 0:
            info["jlb_name"] = info["job_name"]
        info["full_name"] = info["process_name"] + '-'  + info["jlb_name"] + '-'  + info['job_name']
        if 'sourcing' in info:
            info["full_name"]  = info["full_name"] + '-' + info['sourcing']
        info["index"] = index
        trim_info = {key:info[key] for key in info if (key not in jlb_keys or key =="job_name")}

        sql_j = SQLJobModel(**trim_info)
        jla_info = {key: info[key] for key in jlb_keys if key in info }
        sql_j.jlb = JobLibItem.check_if_exist(info["full_name"],jla_info)
        sql_product = GetJob.link_products_by_sequence(sql_j)

        sql_j.product_name = sql_product.product_name
        sql_j.project_name = sql_product.project_name
        sql_j.project_id = sql_product.project_id
        sql_j.product = sql_product
        sql_db.session.add(sql_j)
        sql_db.session.commit()





class JobAgent(object):
    def __init__(self, jid, sql_j=None):
        if sql_j is None:
            self.sql_j = SQLJobModel.query.filter(SQLJobModel.index == jid).first()
        else:
            self.sql_j = sql_j
        # self.sql_j = session.query(SQLJobModel).filter(SQLJobModel.index==jid).first()


    """
    methods for real time reports:
        start
        end
        report_abnormal
        abnormal_resovle
    """

    def pass_on(self):
        info = {
            "project_name": self.sql_j.project_name,
            "project_id": self.sql_j.project_id,
            "abnormal_status": self.sql_j.abnormal_status,
            "lock": self.sql_j.lock
        }
        return info

    def start(self, time=None,mode="real_time"):
        self.sql_j.real_start_time = time if time else get_current_time_string()
        if self.sql_j.status != "finished":
            self.sql_j.status = "running"


        # print("1")

        self.update_history(event_time=self.sql_j.real_start_time,
                            event_type="工作開始",
                            event_detail=None)


        # print("3")
        if mode == "real_time":
            if self.sql_j.product != None:
                if self.sql_j.product.current_loc != self.sql_j.jlb.process_name:
                    self.sql_j.product.current_loc = self.sql_j.jlb.process_name

            self.abnormal_sequence()
            sql_db.session.commit()





    def pause(self, info):
        event_time = info["time"] if "time" in info else get_current_time_string()
        self.sql_j.good_qty += info["good_qty"]
        self.sql_j.bad_qty += info["bad_qty"]

        detail = "該次良品:{} 不良品:{} 剩餘:{}".format(
            info["good_qty"], info["bad_qty"], self.sql_j.qty - self.sql_j.good_qty
        )
        self.update_history(event_time=event_time,
                            event_type="工作暫停",
                            event_detail=detail)

        sql_db.session.commit()

    def get_project_type(self):
        from app.agents.projectAgent import ProjectAgent
        pa = ProjectAgent(self.sql_j.project_id)
        return pa.sql_p.project_type

    def end(self, info,mode="real_time"):

        self.sql_j.real_end_time = info["time"] if "time" in info else get_current_time_string()
        self.sql_j.status = "finished"
        self.sql_j.good_qty += info["good_qty"]
        self.sql_j.bad_qty += info["bad_qty"]



        detail = "該次良品:{} 不良品:{} 剩餘:{}".format(
            info["good_qty"], info["bad_qty"], self.sql_j.qty - self.sql_j.good_qty
        )
        self.update_history(event_time=self.sql_j.real_end_time,
                            event_type="工作結束",
                            event_detail=detail)

        sql_db.session.commit()

        SQLJobEntities = self.get_next_job()
        if len(SQLJobEntities) == 0:
            self.end_product()
        elif SQLJobEntities != 0:
            for next_sql_j in SQLJobEntities:
                if next_sql_j.status == "scheduled":
                    next_sql_j.status = "ready"
                    self.update_product_loc(next_sql_j.jlb.process_name)
        if mode == "real_time":
            self.abnormal_working_hours_mismatch()
        sql_db.session.commit()

    def reset(self):
        self.sql_j.plan_start_time = None
        self.sql_j.plan_end_time = None
        self.sql_j.status = "remove"
        self.sql_j.resource = {}

        self.sql_j.abnormalID = None
        self.sql_j.abnormal_status = {"operation_delay": 0,
                                      "working_hours_mismatch": False,
                                      "abnormal_sequence": False,
                                      "abnormal_report": 0}
        self.sql_j.plan_working_time = None
        self.sql_j.working_time_by_day = {}
        self.sql_j.bottleneck = False
        self.sql_j.waiting_time = None
        self.sql_j.lock = False
        self.update_history(event_time=get_current_time_string(),
                            event_type="工作拆批",
                            event_detail=None)

        sql_db.session.commit()

    def end_product(self):
        from app.agents.productAgent import ProductAgent
        pa = ProductAgent(self.sql_j.product_id)
        pa.end()


    def update_product_loc(self,process_name):
        self.sql_j.product.current_loc = process_name


    def get_product_first_job(self, model_set=None):
        from app.agents.productAgent import ProductAgent
        if self.sql_j.product == None:
            return None
        pa = ProductAgent(self.sql_j.product_id,self.sql_j.product)
        return pa.get_first_ready_job(model_set)


    def get_product_loc(self):
        return self.sql_j.product.current_loc

    def get_product_lot(self):
        return self.sql_j.product.lot_nbr

    def lock_job(self):
        self.sql_j.lock = True
        sql_db.session.commit()

    def call_resources(self):
        pass

    def report_abnormal(self, info):
        info["rid"] = str(self.sql_j.resource)
        info["start_time"] = get_current_time_string()
        info["type"] = "schedule:job"
        info["status"] = "report_sent"
        self.sql_j.status = "pause"

        # create event and notification
        from app.api.modelAPI import Notification
        self.sql_j.abnormalID = Notification.post(info)
        self.sql_j.abnormal_status['abnormal_report'] += 1
        flag_modified(self.sql_j, "abnormal_status")
        # update history
        self.update_history(event_time=info["start_time"],
                            event_type=info["event_type"],
                            event_detail="異常已回報")

        pause_following_jobs(self.sql_j)

        sql_db.session.commit()

    def abnormal_resolve(self, info):
        info["end_time"] = get_current_time_string()
        self.sql_j.status = "resume"
        resume_following_jobs(self.sql_j)

        from app.api.modelAPI import Notification
        Notification.put(self.sql_j.abnormalID, info)
        self.update_history(event_time=info["end_time"],
                            event_type=info["event_type"],
                            event_detail="異常排除已回報")
        sql_db.session.commit()
    """
    methods for update schedule
        update_plan_working_time
    """

    def update_plan_working_time(self, plan_start_time, plan_end_time,model_set=None):
        self.sql_j.plan_start_time = plan_start_time
        self.sql_j.plan_end_time = plan_end_time

        if self.sql_j.job_sequence == 1:
           self.sql_j.status = "ready"
           self.update_product_loc(self.sql_j.jlb.process_name)
        first_job = self.get_product_first_job(model_set)
        if first_job is None:
            return
        elif self.sql_j.job_sequence == first_job.job_sequence:
            self.sql_j.status = "ready"
            self.update_product_loc(self.sql_j.jlb.process_name)
        else:
            self.sql_j.status = "scheduled"

    def update_assigned_resource(self, resource):
        self.sql_j.resource = resource


    """
    calculation methods


    """

    def calculate_time_spent_total(self):
        result = 0
        if self.sql_j.working_time_by_day == None:
            return None

        for date in self.sql_j.working_time_by_day:
            result += sum(self.sql_j.working_time_by_day[date], timedelta()).total_seconds()
        return result//60

    @staticmethod
    def calculate_plan_working_hours_v2(sql_j, time1, time2, sql_r=None, r_start_time=None, r_end_time=None):
        #print("===={} calculate_plan_working_hours===".format(sql_j.index))
        if time1 == None or time2 == None:
            return None

        if r_start_time == None:

            rid = list(sql_j.resource.keys())[0]
            if sql_r is not None and sql_r.index != rid:
                return None

            if sql_r is not None and sql_r.index == rid:
                ra = ResourceAgent(rid, sql_r)
            else:
                ra = ResourceAgent(rid)
            working_days = ra.get_working_days()
            working_hours, break_time = ra.get_working_hours()
            r_start_time, r_end_time = working_hours[0].split('-')

            break_time_list = [b.split('-') for b in sorted(break_time)]

        work_time_by_day = {}

        start_time = time1
        end_time = time2
        end_flag = False
        infinite_count = 0
        while True:
            infinite_count +=1
            if infinite_count > 100:
                break
                logging.error("ENTER INFINITE LOOP")
            d = start_time[0:10]

            start_day = d
            r_start = "{} {}".format(d, r_start_time)
            r_end = "{} {}".format(d, r_end_time)


            if start_day not in working_days:
                print("{}'s working day({}) out of range.\n plan_start:{}, plan_end:{}".format(
                    sql_j.index, start_day, time1, time2
                ))

                break

            if start_time < r_start:
                print("{}'s working hours({}) out of range. \n plan_start:{}, plan_end:{}\n shift_start:{} shift_end:{}".format(
                    sql_j.index, start_day, time1, time2, r_start, r_end
                ))

                break
            work_time_by_day[d] = []
            break_time_of_day = []
            for time_range in break_time_list:
                b_start = "{} {}".format(d, time_range[0])
                b_end = "{} {}".format(d, time_range[1])

                if end_time < b_start:

                    diff = convert_str_to_datetime(end_time) - \
                        convert_str_to_datetime(start_time)
                    work_time_by_day[d].append(diff)
                    end_flag = True
                    break
                if b_start < end_time < b_end:
                    print("{}'s working hours({}) is during breaktime. \n plan_start:{}, plan_end:{} \n break_time_start: {}, break_time_end:{}".format(
                    sql_j.index, start_day, time1, time2, b_start, b_end))
                    break
                elif end_time > b_end and start_time < b_start:
                    diff = convert_str_to_datetime(b_start) - \
                        convert_str_to_datetime(start_time)
                    work_time_by_day[d].append(diff)
                    start_time = b_end

            if end_flag is True:
                break
            if end_flag is False and end_time < r_end:
                diff = convert_str_to_datetime(end_time) - \
                    convert_str_to_datetime(start_time)
                work_time_by_day[d].append(diff)
                break

            elif end_time > r_end:
                diff = convert_str_to_datetime(r_end) - \
                    convert_str_to_datetime(start_time)
                work_time_by_day[d].append(diff)
                next_d = working_days[working_days.index(start_day) + 1][0:10]
                start_time  = "{} {}".format(next_d,r_start_time)

        result = 0
        for date in work_time_by_day:
            result += sum(work_time_by_day[date], timedelta()).total_seconds()

        sql_j.plan_working_time = result // 60

        for date in work_time_by_day:
            for i in range(len(work_time_by_day[date])):
                work_time_by_day[date][i] = work_time_by_day[date][i].total_seconds()

        sql_j.working_time_by_day = work_time_by_day
        # sql_db.session.commit()

        return work_time_by_day

    def calculate_waiting_time(self,mode="real_time"):
        if self.sql_j.job_sequence == 1:
            self.sql_j.waiting_time = 0.0
            return 0.0
        first_job = self.get_product_first_job()
        if self.sql_j.job_sequence == first_job.job_sequence:
            self.sql_j.waiting_time = 0.0
            return 0.0

        prev_sql_j = self.get_prev_job()
        if prev_sql_j is None:
            logging.warning("prev_sql_j not found: \n JID:{} current job_sequence:{} \
                            prev JID:{} prev job_sequence:{}".\
                            format(self.sql_j.job_sequence, self.sql_j.index, first_job.index,first_job.job_sequence
                            ))
            return
        elif prev_sql_j.plan_end_time == None:
            logging.warning("prev_sql_j plan end time not found \n\
                   current_seq:{} current_jid:{} prev_seq:{}".format(self.sql_j.job_sequence, self.sql_j.index, prev_sql_j))

            return

        diff = convert_str_to_datetime(self.sql_j.plan_start_time)\
            - convert_str_to_datetime(prev_sql_j.plan_end_time)

        waiting_time = (diff.total_seconds() // 3600) if diff != timedelta(0) else 0.0

        if waiting_time < 0:
            logging.warning("waiting time less than 0, {} {} {}".format(prev_sql_j.index, prev_sql_j.plan_end_time, self.sql_j.plan_start_time))

        self.sql_j.waiting_time = waiting_time
        if mode == "real_time":
            sql_db.session.commit()

        return waiting_time

    def update_buffer_time(self, due):
        t1 = convert_str_to_datetime(due)
        t2 = convert_str_to_datetime(self.sql_j.plan_end_time)

        buffer_time = (t1 - t2).total_seconds() / 3600
        self.sql_j.buffer_time = buffer_time

    def check_bottleneck(self, mode="real_time"):
        thresholds = find_bottleneck_threshold(self.sql_j.jlb.jlb_name)

        if "ignore" in thresholds[2] or self.sql_j.waiting_time is None:
            return
        if self.sql_j.waiting_time > thresholds[0]:
            self.sql_j.bottleneck = True
        if mode == "real_time":
            sql_db.session.commit()


    def abnormal_operation_delay(self):
        if self.sql_j.abnormal_status["operation_delay"] > 0 and self.sql_j.real_start_time != None:
            return self.sql_j.abnormal_status["operation_delay"]

        if self.sql_j.plan_start_time == None:
            return 0

        if self.sql_j.real_start_time == None:
            start_time = get_current_time()

        else:
            start_time = convert_str_to_datetime(self.sql_j.real_start_time)

        plan_start_time = convert_str_to_datetime(self.sql_j.plan_start_time)

        #if self.sql_j.abnormal_status["operation_delay"] == 0:
        thresholds = find_operation_delay_threshold(self.sql_j)

        if (start_time - plan_start_time) > timedelta(minutes=thresholds):
            self.sql_j.abnormal_status["operation_delay"] = (
                start_time - plan_start_time).total_seconds() / 60
            flag_modified(self.sql_j, "abnormal_status")
        return self.sql_j.abnormal_status["operation_delay"]

    def abnormal_working_hours_mismatch(self,mode="real_time"):
        plan_start, plan_end = self.get_plan_working_time()
        real_start, real_end = self.get_real_working_time()

        plan_time = convert_str_to_datetime(plan_end) - convert_str_to_datetime(plan_start)
        real_time = convert_str_to_datetime(real_end) - convert_str_to_datetime(real_start)

        thresholds = find_working_hours_mismatach_threshold(self.sql_j)

        if (real_time - plan_time) > timedelta(minutes=thresholds):
            self.sql_j.abnormal_status['working_hours_mismatch'] = True
            flag_modified(self.sql_j, "abnormal_status")
            return True
        if mode == "real_time":
            sql_db.session.commit()

    def abnormal_sequence(self,mode="real_time"):
        prev_sql_j = self.get_prev_job()

        if prev_sql_j == None:
            return

        if (prev_sql_j.real_end_time == None and self.sql_j.real_start_time != None) \
                or (self.sql_j.real_start_time < prev_sql_j.real_end_time):
            self.sql_j.abnormal_status['abnormal_sequence'] = True
            prev_sql_j.status = "finished"
            flag_modified(self.sql_j, "abnormal_status")
            return True
        sql_db.session.commit()


    def split_job():
        pass

    """
    helper methods
        update history
    """

    def get_plan_working_time(self):
        return self.sql_j.plan_start_time, self.sql_j.plan_end_time

    def get_real_working_time(self):
        return self.sql_j.real_start_time, self.sql_j.real_end_time

    def update_history(self, event_time, event_type, event_detail):
        history = {
            "event_time": event_time,
            "event_type": event_type,
            "event_detail": event_detail
        }
        self.sql_j.history.append(history)

    def get_prev_job(self):
        if self.sql_j.job_sequence == 1:
            return None

        prev_sql_j = SQLJobModel.query.filter(
            and_(
                SQLJobModel.product_id == self.sql_j.product_id,
                SQLJobModel.job_sequence == self.sql_j.job_sequence - 1
            )).order_by(desc(SQLJobModel.plan_end_time)).first()

        return prev_sql_j

    def get_next_job(self):
        InlineJob = SQLJobModel.query.filter(
            and_(
                SQLJobModel.product_id == self.sql_j.product_id,
                SQLJobModel.job_sequence == self.sql_j.job_sequence
            )).order_by(desc(SQLJobModel.plan_start_time)).all()
        for sql_j in InlineJob:
            if sql_j.status != "finished":
                return 0

        SQLJobEntities = SQLJobModel.query.filter(
            and_(
                SQLJobModel.product_id == self.sql_j.product_id,
                SQLJobModel.job_sequence == self.sql_j.job_sequence + 1
            )).order_by(desc(SQLJobModel.plan_start_time)).all()

        return SQLJobEntities

    def get_candidate_resource(self):
        candidate_resources = self.sql_j.primary_resource
        candidate_resources.update(self.sql_j.secondary_resource)

        return candidate_resources

    def get_job_info(self):
        info = {
            "index": self.sql_j.index,
            "job_name": self.sql_j.job_name,
            "status": self.sql_j.status,
            "project_name": self.sql_j.project_name,
            "project_id": self.sql_j.project_id,
            "product_id": self.sql_j.product_id,
            "product_name": self.sql_j.product_name,
            "lot_nbr": self.sql_j.product.lot_nbr,
            "resource": self.sql_j.resource,
            "qty": self.sql_j.qty,
            "process_name": self.sql_j.jlb.process_name,
            "jlb_name": self.sql_j.jlb.jlb_name
        }
        return info

    def get_time_info(self):
        info = {
            "job_sequence": self.sql_j.job_sequence,
            "plan_start_time": self.sql_j.plan_start_time,
            "plan_end_time": self.sql_j.plan_end_time,
            "real_start_time": self.sql_j.real_start_time,
            "real_end_time": self.sql_j.real_end_time,
            "detail": self.sql_j.detail,
            "translated_detail": self.sql_j.translated_detail,
        }

        return info

    def get_history(self):
        info = {
            "history": self.sql_j.history
        }
        return info

    def get_abnormal_history(self):
        table = []
        for event in self.sql_j.history:
            if event["event_detail"] == '異常已回報':
                table.append(event)

        return table

    def if_abnormal(self):
        if self.abnormal_operation_delay() > 0:
            return True
        if self.sql_j.abnormal_status["working_hours_mismatch"] == True:
            return True
        if self.sql_j.abnormal_status["abnormal_sequence"] == True:
            return True
        if self.sql_j.abnormal_status["abnormal_report"] > 0:
            return True
        else:
            return False
