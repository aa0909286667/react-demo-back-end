from sqlalchemy import desc
from sqlalchemy import or_, and_

from app.base.models import SQLProjectModel, SQLProductModel, SQLJobModel, SQLProcessModel, SQLSchedulerSetting, SQLMaterialModel, SQLJobLib
from app.base.models import SQLWorkingDays, SQLWorkingHours, SQLResourceModel, SQLScheduleDB, SQLNotificationModel, SQLErrorSetting 
from app.base.processingModels import p_SQLJobModel, p_SQLResourceModel, p_SQLProductModel, p_SQLProjectModel 
from app.base.processingModels import p_SQLMaterialModel, p_SQLProcessModel, p_SQLJobLib, p_SQLWorkingDays,p_SQLWorkingHours 



from app import db as sql_db

def delete_db():
    print("==== delete db ====")
    sql_db.session.query(SQLJobModel).delete()
    sql_db.session.query(SQLResourceModel).delete()
    sql_db.session.commit()
    sql_db.session.query(SQLProductModel).delete()
    sql_db.session.query(SQLProjectModel).delete()
    # sql_db.session.query(SQLScheduleDB).delete()
    sql_db.session.query(SQLNotificationModel).delete()
    # 完全不保留盤點資料
    sql_db.session.query(SQLMaterialModel).delete()
    sql_db.session.query(SQLProcessModel).delete()
    sql_db.session.query(SQLJobLib).delete()
    sql_db.session.query(SQLWorkingDays).delete()
    sql_db.session.query(SQLWorkingHours).delete()
    sql_db.session.commit()

def append_data(model,data):
    for row in data:
        new = model(**data[row])
        sql_db.session.add(new)
    sql_db.session.commit()

def copy_to_db():
    print("==== copy to db ====")
    from app.api.utils import serialize_db
    append_data(SQLWorkingHours,serialize_db(p_SQLWorkingHours.query.all()))
    append_data(SQLWorkingDays,serialize_db(p_SQLWorkingDays.query.all()))
    append_data(SQLMaterialModel,serialize_db(p_SQLMaterialModel.query.all()))
    append_data(SQLProcessModel,serialize_db(p_SQLProcessModel.query.all()))
    append_data(SQLJobLib,serialize_db(p_SQLJobLib.query.all()))
    append_data(SQLProjectModel,serialize_db(p_SQLProjectModel.query.all()))
    append_data(SQLProductModel,serialize_db(p_SQLProductModel.query.all()))
    append_data(SQLResourceModel,serialize_db(p_SQLResourceModel.query.all()))
    append_data(SQLJobModel,serialize_db(p_SQLJobModel.query.all()))
    
def delete_p_db():
    print("==== delete p db ====")
    sql_db.session.query(p_SQLJobModel).delete()
    sql_db.session.query(p_SQLResourceModel).delete()
    sql_db.session.commit()
    sql_db.session.query(p_SQLProductModel).delete()
    sql_db.session.query(p_SQLProjectModel).delete()
    # 完全不保留盤點資料
    sql_db.session.query(p_SQLMaterialModel).delete()
    sql_db.session.query(p_SQLProcessModel).delete()
    sql_db.session.query(p_SQLJobLib).delete()
    sql_db.session.query(p_SQLWorkingDays).delete()
    sql_db.session.query(p_SQLWorkingHours).delete()
    sql_db.session.commit() 

def get_summary_detail(products):
    res = [{
            "product_full_name": sql_product.product_full_name,
            "order_date": sql_product.order_date,
            "due": sql_product.due,
            "plan_end_time": sql_product.plan_end_time
            } for sql_product in products ]
    return res

def get_schedule_summary():
    """ 
    紀錄該次排程工單, 排程工單數 
    逾時工單，逾時工單數
    瓶頸製程，瓶頸製程數
    """
    # 排程工單
    scheduled_products = SQLProductModel.query.filter(SQLProductModel.status=="scheduled").all()    
    scheduled_products_info = get_summary_detail(scheduled_products)
    # 逾時工單
    delayed_products  = SQLProductModel.query.filter(SQLProductModel.plan_end_time > SQLProductModel.due).all()
    delayed_products_info = get_summary_detail(delayed_products)

    # 瓶頸製程
    bottlenecks = sql_db.session.query(SQLJobModel.bottleneck, SQLJobLib.job_name).filter(
        and_( SQLJobModel.jlb_id == SQLJobLib.id,
              SQLJobModel.bottleneck==True)).distinct()
    bottlenecks_info = [ (bottleneck.jlb_name, bottleneck.job_name) for bottleneck in bottlenecks]

    return {
        "schedule_products_info":scheduled_products_info,
        "delay_products_info": delayed_products_info,
        "bottlenecks_info":bottlenecks_info}
        

