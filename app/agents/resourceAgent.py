from sqlalchemy.orm.attributes import flag_modified    
from app.base.models import SQLResourceModel, SQLWorkingHours, SQLWorkingDays
from app.base.models import SQLJobModel, SQLJobLib
from app import db as sql_db
from sqlalchemy import or_, and_
from datetime import datetime, timedelta
from config import Config

from app.api.utils import get_current_time_string, convert_datetime_to_str, convert_str_to_datetime, get_current_time
from app.agents.dataAgent import find_lock_time

"""
web resource-agent:
    in responsible of real time status, shifts, and assigned jobs
"""
class ResourceAgent(object):
    def __init__(self,rid,sql_r=None):
        if sql_r is None:
            self.sql_r = SQLResourceModel.query.filter(
                or_(SQLResourceModel.index==rid,
                    SQLResourceModel.resource_name==rid)).first()
        else:
            self.sql_r = sql_r

    def start(self):
        self.sql_r.status = "running"
    
    def stop(self):
        self.sql_r.status = "standBy"

    def report_abnormal(self,info):
        info["start_time"] = get_current_time_string()
        info["type"] = "schedule:resource"
        info["status"] = "report_sent"
        self.sql_r.status = "pause"        
        
        # create evenet and notification
        import requests
        import json

        self.sql_r.abnormalID = json.loads(requests.post(Config.SARA_PORT + '/api/get_notification',json=info).text)

        self.sql_r.abnormal_status['abnormal_report'] +=1
        
        # update history
        self.update_history(
            event_time=info["start_time"],
            event_type=info["event_type"],
            event_detail="異常已回報"
        )

        sql_db.session.commit()

    def abnormal_resolve(self,info):
        info["end_time"] = get_current_time_string()
        
        from app.api.modelAPI import Notification
        Notification.put(self.sql_r.abnormalID,info)
        self.update_history(event_time=info["end_time"],
                            event_type=info["event_type"],
                            event_detail="異常排除已回報")
    
    def update_history(self,event_time,event_type, event_detail):
        
        history = {
            "event_time": event_time,
            "event_type": event_type, 
            "event_detail": event_detail
        }
        self.sql_r.history.append(history)
        sql_db.session.commit()

    def calculate_working_hours(self):
        pass

    def update_assigned_jobs(self, jobs_tuple=None,jid_list=None):

        if jid_list is None:
            jid_list = [j[0] for j in jobs_tuple]

        if type(jid_list) == str:
            self.sql_r.jid_list.append(jid_list)
        else:
            self.sql_r.jid_list = jid_list
        if len(jid_list) > 0:
            self.sql_r.status = "scheduled"
        else:
            self.sql_r.status = "standBy"
        
        

        # self.update_lock_time_range()
        # self.update_job_lock_status(jobs_tuple)

    
    def update_job_lock_status(self,jobs_tuple):
        # jobs_tuple:
        # [ [jid,plan_start, plan_end], [jid2, plan_start, plan_end] ... ]
        from app.agents.jobAgent import JobAgent

        for j in jobs_tuple:
            if self.sql_r.index == "RCID0":
                print(j[2], self.sql_r.lock_time_end)
            if j[2] < self.sql_r.lock_time_end:
                ja = JobAgent(j[0])
                ja.lock_job()

    def get_calendar_name(self):
        return self.sql_r.workingDays.calendar_name

        # wdid = self.sql_r.working_day
        # sql_wd = SQLWorkingDays.query.filter(SQLWorkingDays.index == wdid).first()
        # return sql_wd.calendar_name        

    def get_hours_name(self):
        return self.sql_r.workingHours.hours_name

        # whid = self.sql_r.working_hours
        # sql_wh = SQLWorkingHours.query.filter(SQLWorkingHours.index == whid).first()
        # return sql_wh.hours_name
                
    def get_working_days(self):
        # wdid = self.sql_r.working_day
        # sql_wd = SQLWorkingDays.query.filter(SQLWorkingDays.index == wdid).first()
        # working_days = sql_wd.working_days
        return self.sql_r.workingDays.working_days

    def get_working_hours(self):
        # whid = self.sql_r.working_hours
        # sql_wh = SQLWorkingHours.query.filter(SQLWorkingHours.index == whid).first()
        # working_hours = sql_wh.working_hours
        # break_time = sql_wh.break_time
        return self.sql_r.workingHours.working_hours, self.sql_r.workingHours.break_time

    def get_resource_info(self):
        info = {
            "resource_name": self.sql_r.resource_name,
            "rid": self.sql_r.index,
            "resource_type": self.sql_r.resource_type,
            "job_name": self.sql_r.job_name,
            "workingdays": self.get_calendar_name(),
            "workinghours": self.get_hours_name(),
            "status": self.sql_r.status,
            "history": self.sql_r.history
        }
        return info

    def get_setting_info(self):
        info = {
            "allow_overlapped_capacity": self.sql_r.capacity,
            "lock_time": self.sql_r.lock_time,
            "day_off": self.sql_r.day_off
        }
        return info

    def update_lock_time(self,lock_time):
        self.sql_r.lock_time = lock_time
        sql_db.session.commit()

    def update_lock_time_range(self):
        # latest update time = start_time
        # latest update time + lock_time = end_time
        # lock time 時間內的工作不重排
        start_time = get_current_time()

        end_time = start_time + timedelta(hours=self.sql_r.lock_time)
        self.sql_r.lock_time_start = convert_datetime_to_str(start_time)
        self.sql_r.lock_time_end = convert_datetime_to_str(end_time)
        
        sql_db.session.commit()

    def get_process(self):
        from app.base.models import SQLProcessModel

        process = set()
        for job in self.sql_r.job_name:
            prc = SQLProcessModel.query.filter(SQLProcessModel._job.any(job)).first()
            if prc:
                process.add(prc.process_name)
            
        return list(process)

    def get_job_table(self):
        
        SQLJobEntities = sql_db.session.query(SQLJobModel, SQLJobLib).join(SQLJobLib).filter(
            SQLJobModel.index.in_(self.sql_r.jid_list)
        ).order_by(
            SQLJobModel.real_start_time, 
            SQLJobModel.plan_start_time).all()
        
        
        return SQLJobEntities

    def calculate_plan_working_hours_by_day(self):
        from datetime import datetime, timedelta
        from app.agents.jobAgent import JobAgent
        day = {}
        SQLJobEntities = SQLJobModel.query.with_entities(
            SQLJobModel.index, SQLJobModel.working_time_by_day,\
        ).filter(
            SQLJobModel.index.in_(self.sql_r.jid_list)
        ).all()
        
        

        for sql_j in SQLJobEntities:
            for date in sql_j.working_time_by_day:
                if date not in day:
                    day[date] = sql_j.working_time_by_day[date]
                else:
                   day[date].extend(sql_j.working_time_by_day[date]) 

        working_days = self.get_working_days()
        working_hours, _ = self.get_working_hours()
        s1, s2 = working_hours[0].split('-')
        tdelta = convert_str_to_datetime(s2, mode='hour') - convert_str_to_datetime(s1, mode='hour')

        sol = {}
        for date in day:
            if date in working_days:
                # 今天實際工時
                result = timedelta(seconds=sum(day[date]))
                loading = round(float(result/tdelta),2)
                sol[date] = [result.total_seconds()/60,loading]
        return sol

    def add_day_on(self, day_on):
        for index in day_on:
            
            self.sql_r.day_on[index] = day_on[index] 
        
        flag_modified(self.sql_r, "day_on")
        sql_db.session.commit()
 
    def add_day_off(self, day_off):
        """
        {
            index: 
                "event_time": [start_time(%Y-%m-%d %H:%M:%S), end_time(%Y-%m-%d %H:%M:%S)],
                "event_type": event_type #請假, 維修, 借出 etc
            
        }
        """
        #dt_format = "%a %m-%d-%Y"
         
        for index in day_off:
            
            self.sql_r.day_off[index] = day_off[index] 
        
        flag_modified(self.sql_r, "day_off")
        sql_db.session.commit()
    
    def delete_day_off(self,day_off):
        for index in day_off:
            self.sql_r.day_off.pop(index, None)
        
        flag_modified(self.sql_r, "day_off")
        sql_db.session.commit()
    
    def delete_day_on(self,day_on):
        for index in day_on:
            
            self.sql_r.day_on.pop(index,None)
        
        flag_modified(self.sql_r, "day_on")
        sql_db.session.commit()

    
    
    def update_resource_type(self):
        if self.sql_r.resource_type is not None:
            return
        if self.sql_r.operator_skill < 9:
            self.sql_r.resource_type = "operator"
        elif self.sql_r.operator_skill == 9 and len(self.sql_r.job_name) > 1:
            self.sql_r.resource_type = "operator"
        else:
            self.sql_r.resource_type = "machine"  
    
    def add_new_ability(self, job_name, process_name=None):
        if self.sql_r.job_name == None:
            tmp = set()
        else:
            tmp = set(self.sql_r.job_name)

        if type(job_name) == str:
            job_name = job_name.split(',')
            for j in job_name:
                tmp.add(j)
        else:
            for j in job_name:
                tmp.add(j)
        self.sql_r.job_name = list(tmp)

        flag_modified(self.sql_r, "job_name")
        sql_db.session.commit()
        

        
