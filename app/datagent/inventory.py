import pandas as pd
import json
import time

from app.base.processingModels import p_SQLProcessModel, p_SQLResourceModel, p_SQLJobLib 
from app.base.processingModels import p_SQLWorkingDays, p_SQLWorkingHours, p_SQLMaterialModel
from app.datagent.utils import trim
from app import db as sql_db

def create_model_index(prefix, df, model):
    count = model.query.count()
    
    df["index"] = [prefix + str(i + count) for i in range(df.shape[0])]
    return df

def create_index(prefix, df):
    df["index"] = prefix + df.index.astype(str)
    return df

def push_json_to_sql(json_db,model):
    for _id in json_db:
        if "product_name" in json_db[_id] and json_db[_id]["product_name"] == "RDT4202-1D0":
            print("====DEBUG ====")
            print(json_db[_id])
            print()
            print()
            print()
            
        obj = model(**json_db[_id])
        sql_db.session.add(obj)
    sql_db.session.commit()
    return len(json_db)
    
def make_process_inventory(df,keys):
    print("====make_process_inventory====")
    start_time = time.time()
    if "job_name" not in df.columns and "jlb_name" in df.columns:
        df["job_name"] = df["jlb_name"]
        
    df = trim(df,keys,subset=None)
    
    df = df.rename(columns=dict(job_name='_job'))
    df = df.groupby(['process_name']).agg(lambda x: x.tolist()).reset_index()
    df = create_model_index("PRCID", df, p_SQLProcessModel)
    count = push_json_to_sql(json.loads(df.to_json(orient="index")), p_SQLProcessModel)    
    print("--- {} seconds {} length---".format( time.time() - start_time, count))
    return df

def make_calendar_inventory(df,keys):

    print("==== make calendar inventory ==== ") 
    start_time = time.time()
    df = trim(df,keys,subset=None)
    df = df.groupby(['calendar_name']).agg(lambda x: x.tolist()).reset_index()
    df = create_model_index("WDID", df, p_SQLWorkingDays)
    count = push_json_to_sql(json.loads(df.to_json(orient="index")), p_SQLWorkingDays)    
    print("--- {} seconds {} length---".format( time.time() - start_time, count))
    # if isinstance(info["working_days"], dict):
    #     tmp =  [ c["start"] for c in info["working_days"]]
    #     info["working_days"] = tmp
    
    return df

def make_shift_inventory(df,keys):
    print("====make_shift_inventory====")
    start_time = time.time()
    df = trim(df,keys,subset=None)
    df["working_hours"] =df.working_hours.apply(lambda x: x.split(','))
    df["break_time"] =df.break_time.apply(lambda x: x.split(',') if pd.isna(x)!=True else [])
    df = create_model_index("WHID", df, p_SQLWorkingHours)
    count = push_json_to_sql(json.loads(df.to_json(orient="index")),p_SQLWorkingHours)
    print("--- {} seconds {} length---".format( time.time() - start_time, count))
    return df

def make_material_inventory(df,keys):
    print("====make_material_inventory====")
    start_time = time.time()
    df = trim(df,keys,subset="material_name")
    df["available"] = df["inventory"]
    count = push_json_to_sql(json.loads(df.to_json(orient="index")), p_SQLMaterialModel)
    print("--- {} seconds {} length---".format( time.time() - start_time, count))
    return df


def make_jlb_inventory(df,keys):
    print("====make_jlb_inventory====")
    from app.agents.jobLib import JobLibAgent
    start_time = time.time()
    
    
    if "job_name" not in df.columns and "jlb_name" in df.columns:
        df["job_name"] = df["jlb_name"]
    if "jlb_name" not in df.columns and "job_name" in df.columns:
        df["jlb_name"] = df["job_name"]

    df = trim(df,keys,subset=None)

    df["full_name"] = df["process_name"] + '-'  + df["jlb_name"] + '-' + df['job_name'] 
    if "sourcing" in df.columns:
        df["full_name"] = df["full_name"] + '-' + df['sourcing']
    
    df = trim(df,keys,subset="full_name")
    df = create_model_index("JLBID", df, p_SQLJobLib) 
    result = json.loads(df.to_json(orient="index"))
    for _id in result:
        info = result[_id]
        
        index = "JLB" + str(p_SQLJobLib.query.count())
        if "job_name" not in info:
            info["job_name"] = info["jlb_name"]
        sql_jlb = p_SQLJobLib(**info)
        sql_jlb.index = index

        sql_db.session.add(sql_jlb)
        sql_db.session.commit()
        agent = JobLibAgent(sql_jlb.index, sql_jlb)
        agent.find_primary_second_resource(model_set=[p_SQLResourceModel,p_SQLWorkingDays,p_SQLWorkingHours])
        

        
    print("--- {} seconds {} length---".format( time.time() - start_time, len(result)))
    return df


def create_resource_workingtime_relation(info):
    if "calendar_name" in info:
        sql_wd = p_SQLWorkingDays.query.filter(p_SQLWorkingDays.calendar_name == info["calendar_name"]).first()
        info["working_day"] = sql_wd.index
        info.pop("calendar_name")
    else:
        sql_wd = p_SQLWorkingDays.query.filter(p_SQLWorkingDays.index == "WDID0").first()
    if "hours_name" in info:
        sql_wh = p_SQLWorkingHours.query.filter(p_SQLWorkingHours.hours_name == info["hours_name"]).first()
        info["working_hours"] = sql_wh.index
        info.pop("hours_name")
    else:
        sql_wh = p_SQLWorkingHours.query.filter(p_SQLWorkingHours.hours_name == "全天").first()
    if sql_wd == None or sql_wd == None:
        print("fail create resource working time relation")
    return info, sql_wd, sql_wh

def make_resource_inventory(df,keys):
    print("====make_resource_inventory====")
    start_time = time.time()
    df = trim(df,keys,subset=None)
    # resource_id 與 resource_name 是否一對一?
    # 處理virtual resource
    if 'resource_type' not in df.columns:
        df['resource_type']='machine'
    
    if "resource_id" not in df.columns:
        df = create_model_index("RCID", df, p_SQLResourceModel) 
    else:
        df = df.rename(columns=dict(resource_id='index'))

    if 'resource_name' not in df.columns:
        df['resource_name'] = df['index']

    df.loc[(df['resource_name'] == "nan") | (df['resource_name'] == "None") ,"resource_type"] = "virtual"
    df.loc[(df['index'] == "nan") | (df['index'] == "None") ,"index"] = df['job_name'].astype(str)+"-未註冊機台"
    df.loc[(df['resource_name'] == "nan") | (df['resource_name'] == "None") ,"resource_name"] = df['job_name'].astype(str)+"-未註冊機台"
    df["index"].fillna(df['job_name']+"-未註冊機台",inplace=True)
    # 處理多製程能力
    df["job_name"] =df.job_name.apply(lambda x: x.split(','))
    
    # push to db
    result = json.loads(df.to_json(orient="index"))
    new_resources = []
    for _id in result:
        try:
            if result[_id]["index"] in new_resources:
                sql_r = p_SQLResourceModel.query.filter(p_SQLResourceModel.index == result[_id]["index"]).first()
                sql_r.job_name.extend(result[_id]["job_name"])
            else:
                new_resources.append(result[_id]["index"])
                info, sql_wd, sql_wh = create_resource_workingtime_relation(result[_id]) 
                sql_r = p_SQLResourceModel(**info)
                sql_r.workingDays = sql_wd
                sql_r.workingHours = sql_wh
                sql_db.session.add(sql_r)
            
        except Exception as e:
            print(e)
            print("some weird bug")
            print(_id)
            print(result[_id])
        sql_db.session.commit()

    print("--- {} seconds {} length---".format( time.time() - start_time, len(result)))
    return df

def make_oursourcing_inventory(df,keys):
    print("====make_oursourcing_inventory====")
    start_time = time.time()
    df = trim(df,keys,subset=None)
    df["job_name"] =df.job_name.apply(lambda x: x.split(','))
    df["resource_type"] = "vendor"
    df["allow_overlapped"] = True
    df = create_model_index("RCID", df, p_SQLResourceModel) 

    # push to db
    result = json.loads(df.to_json(orient="index"))
    new_resources = []
    for _id in result:
        try:
            new_resources.append(result[_id]["index"])
            info, sql_wd, sql_wh = create_resource_workingtime_relation(result[_id]) 
            sql_r = p_SQLResourceModel(**info)
            sql_r.workingDays = sql_wd
            sql_r.workingHours = sql_wh
            sql_db.session.add(sql_r)
        except Exception as e:
            print(e)
            print("some weird bug")
            print(result[_id])
            
    sql_db.session.commit()
    print("--- {} seconds {} length---".format( time.time() - start_time, len(result)))
    return df

