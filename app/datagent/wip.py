import pandas as pd
import json
import time
from sqlalchemy import desc
from sqlalchemy import or_, and_

from app.datagent.inventory import create_model_index, push_json_to_sql
from app.datagent.utils import trim, translate, json_to_df, get_subset, _clean
from app.base.processingModels import p_SQLProjectModel, p_SQLProductModel, p_SQLJobModel, p_SQLJobLib, p_SQLResourceModel

from app import db as sql_db
from sqlalchemy.orm.attributes import flag_modified    
from app.local_config import CUSTOMER_MES_RESULT_API, CUSTOMER_ERP_INPR_ORDER_API,CUSTOMER_ERP_INPR_ORDER_DETAIL_API
import logging
logging.getLogger().setLevel(logging.INFO)


def create_wip(df,ver_config, mode="custom"):
    project_config = ver_config['project'][:]
    product_config = ver_config['product'][:] 
    
    if trim(df,ver_config['project'],subset="project_name") is None:
        project_config.remove('order_date')
        project_config.remove('due')
    else:
        print(project_config)
    # if "order_id" in df:
    #     df["project_full_name"] = df["order_id"] + '_' + df["project_name"]
    # else:
    #     df["project_full_name"] = df["project_name"]
    
    # print(df)
    uniq_projects = create_projects(trim(df,project_config,subset="project_name"))
    df['project_id'] = df.project_name.map(uniq_projects.set_index('project_name')['index'])
    
    if "lot_nbr" not in df.columns:
        df['lot_nbr'] = '001'
    df["product_full_name"] = df["project_name"] + '_' + df["product_name"] + '_' + df['lot_nbr']
    
    uniq_products = create_products(trim(df,product_config,subset="product_full_name"))
    df['product_id'] = df.product_full_name.map(uniq_products.set_index('product_full_name')['index'])
    
    if mode != "pipeline":
        df = create_jobs(df,ver_config["job"],ver_config["jlb"])
    else:
        return uniq_projects, uniq_products

def create_projects(uniq_projects):

    print("====create project ====")
    start_time = time.time()
    if len(uniq_projects) == 0:
        print("df is empty!")
    uniq_projects = create_model_index("PID", uniq_projects, p_SQLProjectModel )
    count = push_json_to_sql(json.loads(uniq_projects.to_json(orient="index")), p_SQLProjectModel)
    assert count == len(uniq_projects)

    print("--- {} seconds {} length---".format( time.time() - start_time, count))
    return uniq_projects

def create_products(uniq_products):
    print("====create product =====")
    start_time = time.time()
    uniq_products = create_model_index("PRDID", uniq_products,p_SQLProductModel )
    
    count = push_json_to_sql(json.loads(uniq_products.to_json(orient="index")), p_SQLProductModel)
    
    assert count == len(uniq_products)
    link_projects(uniq_products.groupby(['project_id']).agg(lambda x: x.tolist()))
    print("--- {} seconds {} length---".format( time.time() - start_time, count))
    return uniq_products

def check_if_exist(full_name, info):
    from app.agents.jobLib import JobLibAgent
    sql_jlb = p_SQLJobLib.query.filter( p_SQLJobLib.full_name == full_name ).first()
    if sql_jlb:
        return sql_jlb
    else:
        index = "JLB" + str(p_SQLJobLib.query.count())
        if "job_name" not in info:
            info["job_name"] = info["jlb_name"]
        sql_jlb = p_SQLJobLib(**info)
        sql_jlb.index = index

        sql_db.session.add(sql_jlb)
        sql_db.session.commit()
        
        jlb_agent = JobLibAgent(sql_jlb.full_name, sql_jlb)
        jlb_agent.find_primary_second_resource(model_set=[p_SQLResourceModel,p_SQLWorkingDays,p_SQLWorkingHours])
        
def create_jobs(df,keys, jlb_keys):
    from app.agents.jobLib import JobLibItem

    print("==== create job =====")
    start_time = time.time()
    print(df.columns)

    if "job_name" not in df.columns and "jlb_name" in df.columns:
        df["job_name"] = df["jlb_name"]
    if "jlb_name" not in df.columns and "job_name" in df.columns:
        df["jlb_name"] = df["job_name"]
    df["full_name"] = df["process_name"] + '-'  + df["jlb_name"] + '-'  + df['job_name']
    if 'sourcing' in df.columns:
        df["full_name"]  = df["full_name"] + '-' + df['sourcing']
    if "index" not in df.columns:
        df = create_model_index("JID", df, p_SQLJobModel)

    uniq_jobs = trim(df,keys,subset="index")
    result = json.loads(uniq_jobs.to_json(orient="index"))
    new_indice = []
    session = sql_db.session
    failed_product = {}
    for _id in result:
        if result[_id]['product_name'] == "RDT4202-1D0":
            print("==== create time DEBUG====")
            print(result[_id])
            print("\n\n\n")

        info = result[_id]
        info.pop('lot_nbr',None)
        trim_info = {key:info[key] for key in info if (key not in jlb_keys or key =="job_name")}

        if result[_id]["index"] in new_indice:
            print("dupliate job", result[_id]["index"] )
        else:
            sql_product = p_SQLProductModel.query.filter(p_SQLProductModel.index == info["product_id"]).first()
            if sql_product == None:
                if result[_id]["product_name"] not in failed_product:
                    failed_product[result[_id]["product_name"]] = []
                failed_product[result[_id]["product_name"]].append(trim_info)
                continue
            sql_j = p_SQLJobModel(**trim_info)
            jla_info = {key: info[key] for key in jlb_keys if key in info }
            sql_j.jlb = check_if_exist(info["full_name"],jla_info)
            
            sql_j.product = sql_product
            new_indice.append(result[_id]["index"])
            session.add(sql_j)
    session.commit()
    if len(failed_product) > 0:
        logging.error("{} products not found:{}".format(len(failed_product),list(failed_product.keys())))

    if "job_sequence" not in uniq_jobs and "route_seq" in uniq_jobs:
        print("==== link by route ====")
        link_products_by_route(uniq_jobs)
    elif "job_sequence" in uniq_jobs:
        print("==== link by sequence ====")
        link_products_by_sequence(uniq_jobs)
    elif "plan_start_time" in df:
        link_products_by_time(df)
    else:
        "link failed"
        sys.exit(1)

    print("--- {} seconds {} length---".format( time.time() - start_time, len(result)))

    return df.rename(columns=dict(index='job_id'))

def link_projects(df):
    from app.agents.projectAgent import ProjectAgent
    result = json.loads(df.to_json(orient="index"))
    print("link_projects")
    for _id in result:
        sql_p = p_SQLProjectModel.query.filter(p_SQLProjectModel.index == _id).first()
        pa = ProjectAgent(sql_p.index, sql_p)
        _product = pa.sql_p._product
        _product.extend(result[_id]["index"])
        pa.sql_p._product = _product
        flag_modified(pa.sql_p, "_product")
    sql_db.session.commit()

def link_products_by_sequence(df):
    from app.agents.productAgent import ProductAgent
    df = df.groupby(['product_id']).agg(lambda x: x.tolist())
    result = json.loads(df.to_json(orient="index"))
    for _id in result:
        sql_product = p_SQLProductModel.query.filter(p_SQLProductModel.index == _id).first()
        product_agent = ProductAgent(sql_product.index, sql_product)
        jid_list = product_agent.sql_product._jobID_list
        for seq, jid in zip(result[_id]["job_sequence"],result[_id]["index"]):
            if str(int(seq)) not in jid_list:
                jid_list[str(int(seq))] = []
            jid_list[str(int(seq))].append(jid)
        product_agent.sql_product._jobID_list = jid_list
        flag_modified(product_agent.sql_product,"_jobID_list")

    sql_db.session.commit()

def link_products_by_route(df):
    from app.agents.productAgent import ProductAgent
    df = df.groupby(['product_id']).agg(lambda x: x.tolist())
    result = json.loads(df.to_json(orient="index"))
    parameter = {}
    for _id in result:

        sql_product = p_SQLProductModel.query.filter(p_SQLProductModel.index == _id).first()
        product_agent = ProductAgent(_id)
        jid_list = product_agent.sql_product._jobID_list
        count = 1
        for r, jid in zip(result[_id]["route_seq"],result[_id]["index"]):

            if str(count) not in jid_list:

                jid_list[str(count)] = []
            jid_list[str(count)].append(jid)
            parameter[jid] = count
            count +=1

        product_agent.sql_product._jobID_list = jid_list
        flag_modified(product_agent.sql_product,"_jobID_list")

    SQLJobEntities = p_SQLJobModel.query.all()
    for sql_j in SQLJobEntities:
        sql_j.job_sequence = parameter[sql_j.index]

    sql_db.session.commit()


def link_products_by_time(df):
    from app.agents.productAgent import ProductAgent
    from app.agents.jobAgent import JobAgent

    df = df.sort_values(['product_name', 'real_start_time','plan_start_time'])
    df = df.groupby(['product_id']).agg(lambda x: x.tolist())
    result = json.loads(df.to_json(orient="index"))
    for _id in result:
        seq = 1
        jid_list = {}
        for r_t, t, jid in zip(result[_id]["real_start_time"],result[_id]["plan_start_time"],result[_id]["index"]):

            if r_t != None and t == None and str(seq) not in jid_list:
                jid_list[str(seq)] = [jid]
            elif r_t != None and t == None and str(seq) in jid_list:
                jid_list[str(seq)].append(jid)

            elif t is not None:
                seq = len(jid_list) + 1
                jid_list[str(seq)] = [jid]
            sql_j = p_SQLJobModel.query.filter(p_SQLJobModel.index == jid).first()
            ja = JobAgent(jid, sql_j)
            ja.sql_j.job_sequence = seq
        product_agent = ProductAgent(_id)
        product_agent.sql_product._jobID_list = jid_list
    sql_db.session.commit()

def get_MES_update_result():
    import requests

    # # 撈出所有完整料號
    # SQLProductEntities = SQLProduct.query.all()
    # data_db = []
    # # call API
    # for sql_product in SQLProductEntities:
    #     project_name, product_name, lot_nbr = sql_product.product_full_name.split('_')
    #     response = requests.get(CUSTOMER_MES_RESULT_API + '{}/{}/{}'.format(project_name,product_name,lot_nbr))
    #     data_db.append(json.loads(response.text))
    # 撈出所有完整料號
    print('====get_MES_update_result====')
    start_time = time.time()
    failed_project = []
    data_db = []
    SQLProdjectEntities = p_SQLProjectModel.query.all()
    print("====total project count:{} ====".format(len(SQLProdjectEntities)))
    count = 0
    for sql_p in SQLProdjectEntities:
        print(count,end="\r")
        project_name = sql_p.project_name
        #project_name, product_name, lot_nbr = sql_product.product_full_name.split('_')
        url = CUSTOMER_MES_RESULT_API + '/{}'.format(project_name)
        response = requests.get(url)
        if response.status_code != 200:
            failed_project.append(project_name)
        # project =
        # for j in project:
        #     product_full_name = "{}_{}_{}".format(j['mo_nbr'],j['item_nbr'],j['mo_lot_nbr'])
        data_db.extend(json.loads(response.text))
    if len(failed_project) > 0:
        print("failed project:{}".format(failed_project))
    return data_db

def get_ERP_inprogress_order():
    import requests
    url = CUSTOMER_ERP_INPR_ORDER_API 
    response = requests.get(url)
    docs = json.loads(response.text)
        
    erp_df = pd.DataFrame.from_dict(docs)
    erp_df.columns =['project_name', 'mo_nbr_step', 'lot_nrb','UNK','product_name']
    return erp_df

def update_product_erp(erp_df,uniq_products):
    print("==== update_product_erp ==== ")
    found = 0
    not_found = []
    for i in range(len(erp_df)):
        mo_nbr_step = erp_df.iloc[i,1]
        k = uniq_products[(uniq_products["project_name"] == erp_df.iloc[i,0]) & (uniq_products["product_name"] == erp_df.iloc[i,4]) & (uniq_products["lot_nbr"] == erp_df.iloc[i,2])]
        if len(k) == 0:
            not_found.append((erp_df.iloc[i,0],erp_df.iloc[i,4],erp_df.iloc[i,2]))
        else:
            found +=1
            info = get_ERP_inprogress_order_detail(erp_df.iloc[i,0],mo_nbr_step, erp_df.iloc[i,2])
            sql_product = p_SQLProductModel.query.filter(
                p_SQLProductModel.product_full_name == erp_df.iloc[i,0] + "_" + erp_df.iloc[i,4] + "_" + erp_df.iloc[i,2]
                ).first()
            for key, value in info.items():
                setattr(sql_product, key, value)
    sql_db.session.commit()
    
def get_ERP_inprogress_order_detail(project_name,mo_nbr_step, lot_nbr):
    def brute_convertTime(t1):
        from datetime import datetime
        from app.api.utils import convert_datetime_to_str
        string1 = datetime.strptime(t1[5:],'%d %b %Y %H:%M:%S %Z')
        return convert_datetime_to_str(string1)
    
    import requests
    url = CUSTOMER_ERP_INPR_ORDER_DETAIL_API + "/{}/{}/{}".format(project_name,mo_nbr_step,lot_nbr)
    response = requests.get(url)
    
    tmp = json.loads(response.text)
    if len(tmp) < 1:
        return {}
    tmp = tmp[0]
    info = dict(
        order_id=tmp[0],
        mo_nbr_step=tmp[2],
        order_date=brute_convertTime(tmp[8]),
        due=brute_convertTime(tmp[9]))
    return info

def job_MES_query(product_full_name, route_seq, job_name):
    result_tuple = sql_db.session.query(p_SQLJobModel,p_SQLProductModel).join(p_SQLProductModel).filter(
        and_(
            p_SQLProductModel.product_full_name == product_full_name,
            p_SQLProductModel.index == p_SQLJobModel.product_id,
            p_SQLJobModel.route_seq == route_seq,
            p_SQLJobModel.job_name == job_name)
        ).first()

    return result_tuple

def job_MES_update(df):
    from app.agents.jobAgent import JobAgent
    print('====distribute_jobs_time====')
    print(df)
    start_time = time.time()
    result = json.loads(df.to_json(orient="index"))

    for _id in result:
        
        product_full_name = "{}_{}_{}".format(result[_id]['project_name'],result[_id]['product_name'],result[_id]['lot_nbr'])
        job_name = result[_id]['process_name'][0:3] + '-' + result[_id]['job_name']
        result_tuple = job_MES_query(product_full_name, result[_id]["route_seq"],job_name)
        if result_tuple is None:
            continue
        print(_id,end="\r")
        sql_j = result_tuple[0]
        ja = JobAgent(sql_j.index, sql_j)
        
        if result[_id]["real_start_time"] != None and (sql_j.status != "running" or sql_j.status != "finished"):
            ja.start(time=result[_id]["real_start_time"],mode="group")
        if result[_id]["real_end_time"] != None and sql_j.status != "finished":
            info = {
                "time": result[_id]["real_end_time"],
                "good_qty": 0,
                "bad_qty": 0
            }
            ja.end(info,mode="group")
    sql_db.session.commit()


def distribute_jobs_time(job_time):
    from app.agents.jobAgent import JobAgent
    print('====distribute_jobs_time====')
    start_time = time.time()
    result = json.loads(job_time.to_json(orient="index"))
    s = sql_db.session

    for _id in result:
        

        sql_j = p_SQLJobModel.query.filter(p_SQLJobModel.index==result[_id]["index"]).first()
        ja = JobAgent(result[_id]["index"],sql_j)

        if ja.sql_j is None:
            print("job not found in job_db but appear in input data")
            print(result[_id])
            continue
        if result[_id]["real_start_time"] != None:
            ja.start(time=result[_id]["real_start_time"],mode="group")
        else:
            ja.update_plan_working_time(result[_id]["plan_start_time"],result[_id]["plan_end_time"],model_set=[p_SQLJobModel])

    s.commit()

    print("--- {} seconds {} length---".format( time.time() - start_time, len(result)))



def distribute_job_resource(job_assigned):
    from app.agents.jobAgent import JobAgent
    from app.agents.resourceAgent import ResourceAgent
    print('====distribute_job_resource====')
    start_time = time.time()
    job_assigned = job_assigned.groupby(['index']).agg(lambda x: x.tolist()).reset_index()

    result = json.loads(job_assigned.to_json(orient="index"))
    for _id in result:
        sql_j = p_SQLJobModel.query.filter(p_SQLJobModel.index ==result[_id]["index"]).first()
        ja = JobAgent(result[_id]["index"], sql_j)
        if ja.sql_j is None:
            # TODO: LOG
            continue
        #print(result[_id])
        resources = result[_id]["resource_id"]

        rid_list = {}
        for r_name in resources:
            if r_name == None:
                r_name = result[_id]["job_name"][0] + "-未註冊機台"
            sql_r = p_SQLResourceModel.query.filter(
                or_(p_SQLResourceModel.index==r_name,
                    p_SQLResourceModel.resource_name==r_name)).first()

            #sql_r = p_SQLResourceModel.query.filter(p_SQLResourceModel.resource_name == r_name).first()
            ra = ResourceAgent(rid=r_name,sql_r=sql_r)
            if ra.sql_r is None:
                print(r_name)
                print(result[_id])
            else:

                rid_list[ra.sql_r.index] = r_name

        ja.update_assigned_resource(rid_list)
    print("--- {} seconds {} length---".format( time.time() - start_time, len(result)))

def distribute_resources(job_assigned):
    from app.agents.resourceAgent import ResourceAgent
    print('====distribute_resources====')
    start_time = time.time()
    print("===== unassigned resource ====")
    tmp = job_assigned[pd.isna(job_assigned.resource_id)].copy()
    tmp["resource_id"] = tmp["job_name"] + '-未註冊機台'
    job_assigned = job_assigned.groupby(['resource_id']).agg(lambda x: x.tolist()).reset_index()
    df = pd.concat([job_assigned, tmp])

    #result = json.loads(df.to_json(orient="index"))
    for i in range(len(df)):
        resource_id = df.iloc[i,df.columns.get_loc("resource_id") ]

        sql_r = p_SQLResourceModel.query.filter(p_SQLResourceModel.index == resource_id).first()
        ra = ResourceAgent(rid=resource_id,sql_r=sql_r)
        jid_list = df.iloc[i,df.columns.get_loc("index") ]
        ra.update_assigned_jobs(jid_list=jid_list)
    sql_db.session.commit()
    print("--- {} seconds {} length---".format( time.time() - start_time, len(df)))

def update_products(uniq_products):
    print('====update_products====')
    start_time = time.time()
    from app.agents.productAgent import ProductAgent
    for i in range(len(uniq_products)):
        full_name = uniq_products.iloc[i,0] + "_" + uniq_products.iloc[i,1] + "_" + uniq_products.iloc[i,2]
        sql_product = p_SQLProductModel.query.filter(
            or_(p_SQLProductModel.product_full_name == full_name,
                p_SQLProductModel.index == full_name, 
            )).first()
        if sql_product is None:
            print("full_name:{}".format(full_name))
            print(uniq_products.iloc[i,:])
            
        
        product_agent = ProductAgent( sql_product.index ,sql_product)
        try:
            product_agent.update_plan_end_time(model_set=[p_SQLJobModel])
            product_agent.update_product_current_loc(model_set=[p_SQLJobModel])
        except Exception as e:
            print(e)
            
            
        
    print("--- {} seconds {} length---".format( time.time() - start_time, len(uniq_products)))


def update_projects(uniq_projects):
    print('====update_projects====')
    start_time = time.time()
    from app.agents.projectAgent import ProjectAgent
    for i in range(len(uniq_projects)):
        sql_p = p_SQLProjectModel.query.filter(
                or_(
                p_SQLProjectModel.index == uniq_projects.iloc[i,0], 
                p_SQLProjectModel.project_name == uniq_projects.iloc[i,0]
                )).first()
        pa = ProjectAgent(uniq_projects.iloc[i,0], sql_p)
        try:
            pa.update_plan_end_time(model_set=[p_SQLProductModel])
            
        except Exception as e:
            print(e)
            
    print("--- {} seconds {} length---".format( time.time() - start_time, len(uniq_projects)))

def update_wip(mes_df, erp_df):
    distribute_jobs_time(
        trim(
            mes_df,
            keys= ['plan_start_time', 'plan_end_time','real_start_time','real_end_time', 'index'],
            subset='index'))
    distribute_job_resource(
        trim(mes_df,keys= ['job_name','resource_id','index'],
            subset='index'))
    distribute_resources(
        trim(mes_df,keys= ['job_name','resource_id','index'],
            subset='index'))

    update_products(
        trim(erp_df,keys= [ 'project_name','product_name', 'lot_nbr'],subset=None))
    
    #erp_df = _clean(get_ERP_inprogress_order())
    #update_product_erp(erp_df, trim(df,keys= [ 'project_name','product_name', 'lot_nbr'],subset=None))

    update_projects(
        trim(erp_df,keys= ['project_name'],subset=None))
