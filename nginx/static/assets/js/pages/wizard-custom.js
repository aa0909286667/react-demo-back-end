'use strict';
$(document).ready(function() {
    setTimeout(function() {
        // [ Smart Wizard ] start
        $('#smartwizard').smartWizard({
            theme: 'default',
            transitionEffect: 'fade',
            autoAdjustHeight: false,
            useURLhash: false,
            showStepURLhash: false
        });
    }, 700);
    $("#theme_selector").on("change", function() {
        $('#smartwizard').smartWizard("theme", $(this).val());
        return true;
    });
    $('#smartwizard .sw-toolbar').appendTo($('#smartwizard .sw-container'));
    $("#theme_selector").change();

    // [ smart Vartical-left wizard ] start
    setTimeout(function() {
        $('#smartwizard-left').smartWizard({
            selected: 2, // Initial selected step, 0 = first step
            theme: 'dark',
            justified: true,
            transitionEffect: 'fade',
            showStepURLhash: true,
            autoAdjustHeight: false,
            useURLhash: false,
            anchorSettings: {
                anchorClickable: true, // Enable/Disable anchor navigation
                enableAllAnchors: true, // Activates all anchors clickable all times
                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
            },
            lang: { // Language variables for button
                next: 'Next >',
                previous: '< Previous'
            },
            keyboardSettings: {
                keyNavigation: true, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                keyLeft: [37], // Left key code
                keyRight: [39] // Right key code
            },
            toolbarSettings: {
                toolbarPosition: 'bottom', // none, top, bottom, both
                toolbarButtonPosition: 'right', // left, right, center
                showNextButton: false, // show/hide a Next button
                showPreviousButton: false, // show/hide a Previous button
                toolbarExtraButtons: [] // Extra buttons to show on toolbar, array of jQuery input/buttons elements
            },
        });
        $('#smartwizard-left .sw-toolbar').appendTo($('#smartwizard-left .sw-container'));
    }, 100);

    //  [ smart Vartical-right wizard ] start
    setTimeout(function() {
        $('#smartwizard-right').smartWizard({
            theme: 'default',
            transitionEffect: 'fade',
            showStepURLhash: true,
            autoAdjustHeight: false,
            useURLhash: false,
            showStepURLhash: false
        });
        $('#smartwizard-right .sw-toolbar').appendTo($('#smartwizard-right .sw-container'));
    }, 700);
});
