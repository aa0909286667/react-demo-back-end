// import { Splitter, Gantt, ResourceHistogram, ProjectModel } from '/static/assets/plugins/gantt-pro/build/gantt.module.js';
import { Toolbar, Toast, DateHelper, Splitter, Gantt, Scheduler, ProjectModel } from '/static/assets/plugins/gantt-pro/build/gantt.module.js';

// import shared from '/static/assets/plugins/gantt-pro/home/_shared/shared.module.js';
/* eslint-disable no-unused-vars */

class GanttToolbar extends Toolbar {
    // Factoryable type name
    static get type() {
        return 'gantttoolbar';
    }

    static get $name() {
        return 'GanttToolbar';
    }

    // Called when toolbar is added to the Gantt panel
    set parent(parent) {
        super.parent = parent;

        const me = this;

        me.gantt = parent;

        parent.project.on({
            load    : 'updateStartDateField',
            refresh : 'updateStartDateField',
            thisObj : me
        });

        me.styleNode = document.createElement('style');
        document.head.appendChild(me.styleNode);
    }

    get parent() {
        return super.parent;
    }

    static get defaultConfig() {
        return {
            items : [
                {
                    type  : 'buttonGroup',
                    items : [
                        // {
                        //     ref      : 'editTaskButton',
                        //     icon     : 'b-fa b-fa-pen',
                        //     text     : 'Edit',
                        //     tooltip  : 'Edit selected task',
                        //     onAction : 'up.onEditTaskClick'
                        // },
                        {
                            ref   : 'undoRedo',
                            type  : 'undoredo',
                            items : {
                                transactionsCombo : null
                            }
                        }
                    ]
                },
                {
                    type  : 'buttonGroup',
                    items : [
                        {
                            ref      : 'zoomInButton',
                            icon     : 'b-fa b-fa-search-plus',
                            tooltip  : 'Zoom in',
                            onAction : 'up.onZoomInClick'
                        },
                        {
                            ref      : 'zoomOutButton',
                            icon     : 'b-fa b-fa-search-minus',
                            tooltip  : 'Zoom out',
                            onAction : 'up.onZoomOutClick'
                        },
                        {
                            ref      : 'zoomToFitButton',
                            icon     : 'b-fa b-fa-compress-arrows-alt',
                            tooltip  : 'Zoom to fit',
                            onAction : 'up.onZoomToFitClick'
                        },
                        {
                            ref      : 'previousButton',
                            icon     : 'b-fa b-fa-angle-left',
                            tooltip  : 'Previous time span',
                            onAction : 'up.onShiftPreviousClick'
                        },
                        {
                            ref      : 'nextButton',
                            icon     : 'b-fa b-fa-angle-right',
                            tooltip  : 'Next time span',
                            onAction : 'up.onShiftNextClick'
                        }
                    ]
                },
                // {
                //     type  : 'buttonGroup',
                //     items : [
                //         {
                //             type       : 'button',
                //             ref        : 'featuresButton',
                //             icon       : 'b-fa b-fa-tasks',
                //             text       : 'Features',
                //             tooltip    : 'Toggle features',
                //             toggleable : true,
                //             menu       : {
                //                 onItem       : 'up.onFeaturesClick',
                //                 onBeforeShow : 'up.onFeaturesShow',
                //                 // "checked" is set to a boolean value to display a checkbox for menu items. No matter if it is true or false.
                //                 // The real value is set dynamically depending on the "disabled" config of the feature it is bound to.
                //                 items        : [
                //                     {
                //                         text    : 'Draw dependencies',
                //                         feature : 'dependencies',
                //                         checked : false
                //                     },
                //                     {
                //                         text    : 'Task labels',
                //                         feature : 'labels',
                //                         checked : false
                //                     },
                //                     {
                //                         text    : 'Project lines',
                //                         feature : 'projectLines',
                //                         checked : false
                //                     },
                //                     {
                //                         text    : 'Highlight non-working time',
                //                         feature : 'nonWorkingTime',
                //                         checked : false
                //                     },
                //                     {
                //                         text    : 'Enable cell editing',
                //                         feature : 'cellEdit',
                //                         checked : false
                //                     },
                //                     {
                //                         text    : 'Show baselines',
                //                         feature : 'baselines',
                //                         checked : false
                //                     }
                //                 ]
                //             }
                //         }
                //     ]
                // },
                // {
                //     type      : 'datefield',
                //     ref       : 'startDateField',
                //     label     : 'Project start',
                //     // required  : true, (done on load)
                //     flex      : '1 2 17em',
                //     listeners : {
                //         change : 'up.onStartDateChange'
                //     }
                // },
                {
                    type                 : 'textfield',
                    ref                  : 'filterByName',
                    cls                  : 'filter-by-name',
                    flex                 : '1 1 12.5em',
                    // Label used for material, hidden in other themes
                    label                : 'Find tasks by name',
                    // Placeholder for others
                    placeholder          : 'Find tasks by name',
                    clearable            : true,
                    keyStrokeChangeDelay : 100,
                    triggers             : {
                        filter : {
                            align : 'end',
                            cls   : 'b-fa b-fa-filter'
                        }
                    },
                    onChange : 'up.onFilterChange'
                }
            ]
        };
    }


    updateStartDateField() {
        const { startDateField } = this.widgetMap;

        // startDateField.value = this.gantt.project.startDate;

        // This handler is called on project.load/propagationComplete, so now we have the
        // initial start date. Prior to this time, the empty (default) value would be
        // flagged as invalid.
        // startDateField.required = true;
        this.gantt.zoomToFit({
            leftMargin  : 50,
            rightMargin : 50
        }); 
    
    }

    // region controller methods

    onEditTaskClick() {
        const { gantt } = this;

        if (gantt.selectedRecord) {
            gantt.editTask(gantt.selectedRecord);
        }
        else {
            Toast.show(this.L('First select the task you want to edit'));
        }
    }

    onZoomInClick() {
        this.gantt.zoomIn();
    }

    onZoomOutClick() {
        this.gantt.zoomOut();
    }

    onZoomToFitClick() {
        this.gantt.zoomToFit({
            leftMargin  : 50,
            rightMargin : 50
        });
    }

    onShiftPreviousClick() {
        this.gantt.shiftPrevious();
    }

    onShiftNextClick() {
        this.gantt.shiftNext();
    }

    onStartDateChange({ value, oldValue }) {
        if (!oldValue) { // ignore initial set
            return;
        }

        this.gantt.startDate = DateHelper.add(value, -1, 'week');

        this.gantt.project.setStartDate(value);
    }

    onFilterChange({ value }) {
        if (value === '') {
            this.gantt.taskStore.clearFilters();
        }
        else {
            value = value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

            this.gantt.taskStore.filter({
                filters : task => task.name && task.name.match(new RegExp(value, 'i')),
                replace : true
            });
        }
    }

    // onFeaturesClick({ source : item }) {
    //     const { gantt } = this;

    //     if (item.feature) {
    //         const feature = gantt.features[item.feature];
    //         feature.disabled = !feature.disabled;
    //     }
    //     else if (item.subGrid) {
    //         const subGrid = gantt.subGrids[item.subGrid];
    //         subGrid.collapsed = !subGrid.collapsed;
    //     }
    // }

    // onFeaturesShow({ source : menu }) {
    //     const { gantt } = this;

    //     menu.items.map(item => {
    //         const { feature } = item;

    //         if (feature) {
    //             // a feature might be not presented in the gantt
    //             // (the code is shared between "advanced" and "php" demos which use a bit different set of features)
    //             if (gantt.features[feature]) {
    //                 item.checked = !gantt.features[feature].disabled;
    //             }
    //             // hide not existing features
    //             else {
    //                 item.hide();
    //             }
    //         }
    //         else {
    //             item.checked = gantt.subGrids[item.subGrid].collapsed;
    //         }
    //     });
    // }

    // endregion
};

// Register this widget type with its Factory
GanttToolbar.initClass();


// Project that will be shared by Gantt & SchedulerPro (Try use only Scheduler here)
const project = window.project = new ProjectModel({

    startDate : new Date(),
    endDate   : new Date(),

    // General calendar is used by default
    calendar : 'general',

    // transport : {
    //    load : {
    //        url : '/static/assets/plugins/gantt-pro/home/_datasets/m002.json'
    //    },

    transport : {
        load : {
            // url : 'http://192.168.99.101:80/api/load_gantt',
            url: 'http://' + document.domain + '/api/load_gantt',
            method : 'GET'
        },
        sync : {
            // url : 'http://192.168.99.101:80/api/sync_gantt',
            url : 'http://' + document.domain + '/api/sync_gantt',
        }
    },
    autoLoad : true,
    autoSync : true,
    autoSyncTimeout : 10000
});

const gantt = window.project = new Gantt({
    project,

    appendTo                : 'container-gantt',
    flex                    : 1,
    features : {
        baselines  : true,
        indicators : true,
        filter         : true,
        timeRanges     : {
            showCurrentTimeLine : true
        },
        labels : {
            left : {
                field  : 'name',
                editor : {
                    type : 'textfield'
                }
            }
        }
    },

    tbar : {
        type : 'gantttoolbar'
    },

    viewPreset  : 'weekAndDayLetter',
    columnLines : true,

    columns : [
        // { type : 'sequence', minWidth : 50, width : 50, text : '', align : 'right', resizable : false },
        { type : 'name', text: '工單排程', width : 280 },
        { type : 'resourceassignment', text : '排定資源', width : 160 }
    ],

    startDate : '2020-12-10T08:30:00',
    rowHeight : 50,
    barMargin : 15,


    listeners : {
        beforeCellEditStart : ({ editorContext }) => editorContext.column.field !== 'percentDone' || editorContext.record.isLeaf
    }
});

new Splitter({
    appendTo : 'container-gantt'
});

// const histogram = window.histogram = new ResourceHistogram({
//     appendTo    : 'container-gantt',
//     project,
//     hideHeaders : true,
//     partner     : gantt,
//     rowHeight   : 50,
//     showBarTip  : true,
//     columns     : [
//         { field : 'name',
//           text  : '資源負荷', 
//           width : 400  }
//     ]
// });

const scheduler = new Scheduler({
    project,

    appendTo            : 'container-gantt',
    minHeight           : '20em',
    flex                : 1,
    partner             : gantt,
    rowHeight           : 50,
    barMargin           : 15,
    eventColor          : 'gantt-green',

    allowOverlap      : true,

    columns : [
        // { type : 'sequence', minWidth : 50, width : 50, text : '', align : 'right', resizable : false },

        {
            type           : 'resourceInfo',
            field          : 'name',
            text           : '資源排程',
            showEventCount : true,
            // showRole       : true,
            showImage      : false,
            width          : 280
        },
        {
            type  : 'resourceCalendar',
            text  : '工作時段',
            width : 160
        }
    ]
});
