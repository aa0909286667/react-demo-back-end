"use strict";

var _bryntum$gantt = bryntum.gantt,
    Splitter = _bryntum$gantt.Splitter,
    WidgetHelper = _bryntum$gantt.WidgetHelper,
    Gantt = _bryntum$gantt.Gantt,
    SchedulerPro = _bryntum$gantt.SchedulerPro,
    ProjectModel = _bryntum$gantt.ProjectModel;
/* eslint-disable no-unused-vars */
// Project that will be shared by Gantt & SchedulerPro

var project = window.project = new ProjectModel({
  startDate: '2019-01-16',
  endDate: '2019-02-13',
  // General calendar is used by default
  calendar: 'general',
  transport: {
    load: {
      url: '../_datasets/launch-saas.json'
    }
  },
  autoLoad: true
});
var gantt = new Gantt({
  project: project,
  resourceImageFolderPath: '../_shared/images/users/',
  appendTo: 'container',
  flex: 1,
  subGridConfigs: {
    locked: {
      flex: 5
    },
    normal: {
      flex: 8
    }
  },
  features: {
    labels: {
      left: {
        field: 'name',
        editor: {
          type: 'textfield'
        }
      }
    }
  },
  viewPreset: 'weekAndDayLetter',
  columnLines: true,
  columns: [{
    type: 'sequence',
    minWidth: 50,
    width: 50,
    text: '',
    align: 'right',
    resizable: false
  }, {
    type: 'name',
    width: 280
  }, {
    type: 'percent',
    text: '% Completed',
    field: 'percentDone',
    showValue: false,
    width: 160
  }, {
    type: 'resourceassignment',
    text: 'Assigned Resources',
    showAvatars: true,
    width: 160
  }],
  startDate: '2019-01-11',
  listeners: {
    beforeCellEditStart: function beforeCellEditStart(_ref) {
      var editorContext = _ref.editorContext;
      return editorContext.column.field !== 'percentDone' || editorContext.record.isLeaf;
    }
  }
});
new Splitter({
  appendTo: 'container'
});
var scheduler = new SchedulerPro({
  project: project,
  appendTo: 'container',
  minHeight: '20em',
  flex: 1,
  partner: gantt,
  rowHeight: 45,
  eventColor: 'gantt-green',
  useInitialAnimation: false,
  resourceImagePath: '../_shared/images/users/',
  features: {
    dependencies: true,
    percentBar: true
  },
  columns: [{
    type: 'resourceInfo',
    field: 'name',
    text: 'Resource',
    showEventCount: false,
    width: 330
  }, {
    text: 'Assigned tasks',
    field: 'events.length',
    width: 160,
    editor: false,
    align: 'right',
    renderer: function renderer(_ref2) {
      var value = _ref2.value;
      return "".concat(value, " task").concat(value !== 1 ? 's' : '');
    }
  }, {
    text: 'Assigned work days',
    width: 160,
    editor: false,
    align: 'right',
    renderer: function renderer(_ref3) {
      var record = _ref3.record;
      return record.events.map(function (task) {
        return task.duration;
      }).reduce(function (total, current) {
        return total + current;
      }, 0) + ' days';
    }
  }]
});
WidgetHelper.append([{
  type: 'button',
  ref: 'zoomInButton',
  cls: 'b-tool',
  icon: 'b-icon-search-plus',
  tooltip: 'Zoom in',
  onAction: function onAction() {
    return gantt.zoomIn();
  }
}, {
  type: 'button',
  ref: 'zoomOutButton',
  cls: 'b-tool',
  icon: 'b-icon-search-minus',
  tooltip: 'Zoom out',
  onAction: function onAction() {
    return gantt.zoomOut();
  }
}], {
  insertFirst: document.getElementById('tools') || document.body
});