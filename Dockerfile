FROM python:3.8.6

ENV PYTHONUNBUFFERED TRUE

RUN apt-get update && apt-get install -y \
	unixodbc \
	unixodbc-dev \
	tdsodbc \
	tzdata

# RUN ln -sf /usr/share/zoneinfo/Asia/Taipei /etc/localtime
# RUN echo "Asia/Taipei" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata
# ENV TZ=Asia/Taipei
# RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get install -y freetds-common freetds-bin freetds-dev
ADD odbcinst.ini /etc/
RUN pip install -U pip

ENV FLASK_APP run.py

COPY run.py requirements.txt config.py .env ./
COPY app app
COPY migrations migrations

RUN pip install -r requirements.txt
COPY ./entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
